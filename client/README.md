# Eltrot: Client

## Project setup

First, run
```
npm install
```
Use
```
npm run serve
```
to automatically recompile and reload on save, and
```
npm run build
```
to make a production build, which can then be found in the `dist/` directory.

Use
```
npm run format
```
to automatically format all files using `prettier`.

## Developing

The web app requires the backend server running at `localhost:8000`, as specified in the `.env` file.
