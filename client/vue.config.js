module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  },
  chainWebpack: config => {
    config.module
      .rule("i18n")
      .resourceQuery(/blockType=i18n/)
      .type('javascript/auto')
      .use("i18n")
      .loader("@intlify/vue-i18n-loader")
      .end();
  },
  pwa: {
    name: 'Eltrot',
    themeColor: '#008940',
    msTileColor: '#008940',
    appleMobileWebAppCapable: 'yes',
    workboxOptions: {
      skipWaiting: true
    }
  }
}
