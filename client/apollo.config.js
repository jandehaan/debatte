module.exports = {
  client: {
    service: {
      name: "server",
      url: "http://localhost:8000/graphql",
    },
    includes: ["src/**/*.vue", "src/**/*.js"],
  },
};
