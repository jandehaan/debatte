import gql from "graphql-tag";

export const answerFragment = gql`
  fragment Answer on Answer {
    id
    answer
    author {
      id
      name
    }
  }
`;

export const debatesAboutFragment = gql`
  fragment DebatesAbout on DebatesAboutList {
    id
    list {
      id
      question
      reviewRequired {
        id
        reviewRequired
      }
    }
  }
`;

export const debatesOverviewFragment = gql`
  fragment DebatesOverview on TopLevelDebatesList {
    id
    list {
      id
      question
      details
      shortDetails
      state
      lastChange
      concernedArgument {
        heading
      }
      answers {
        id
        list {
          ...Answer
        }
      }
      subDebates {
        id
        list {
          id
          question
          shortDetails
          lastChange
          hasSubDebates
        }
      }
    }
  }

  ${answerFragment}
`;
