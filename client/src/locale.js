export function getLocale() {
  var locale = "EN_US";
  for (var lang of navigator.languages) {
    if (lang.startsWith("de")) {
      locale = "DE_DE";
      break;
    } else if (lang.startsWith("en")) {
      locale = "EN_US";
      break;
    }
  }
  return locale;
}
