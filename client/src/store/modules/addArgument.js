import Vue from "vue";

export default {
  namespaced: true,
  state: {
    supports: {},
  },
  mutations: {
    setAnswers(state, { answers }) {
      state.supports = {};
      answers.map((answer) => Vue.set(state.supports, answer.id, false));
    },
    setSupport(state, { answerId, supports }) {
      state.supports[answerId] = supports;
    },
  },
  getters: {
    supportedAnswersIds(state) {
      let supported = [];
      for (let key in state.supports) {
        if (state.supports[key]) {
          supported.push(key);
        }
      }
      return supported;
    },
  },
};
