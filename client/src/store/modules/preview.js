import Vue from "vue";
import gql from "graphql-tag";

export default {
  namespaced: true,
  state: {
    debateTree: null,
    flattened: null,
  },
  mutations: {
    setBaseTree(state, { debates }) {
      state.debateTree = debates.filter((debate) => !debate.concernedArgument);
      state.flattened = flatten(state.debateTree);
    },
    deepenTree(state, { debate }) {
      state.flattened[debate.id].subDebates = debate.subDebates;
      state.flattened = {
        ...state.flattened,
        ...flatten(debate.subDebates.list),
      };
    },
  },
};

function flatten(debateTree) {
  let flattened = {};
  debateTree.forEach((debate) => {
    flattened[debate.id] = debate;
    if (debate.subDebates) {
      flattened = { ...flattened, ...flatten(debate.subDebates.list) };
    }
  });
  return flattened;
}
