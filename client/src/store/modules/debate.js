import Vue from "vue";
import { ArgumentWeight } from "@/argumentWeight";

export default {
  namespaced: true,
  state: {
    viewedDebate: null,
    argumentWeights: {},
    totalAnswerWeights: {},
    ballotAnswerOrder: [],
    manuallyOrdered: false,
  },
  getters: {
    sortedAnswers: (state) => {
      if (!state.viewedDebate || state.viewedDebate.err) {
        return;
      }

      let sorted = state.ballotAnswerOrder.map(
        (answerId) =>
          state.viewedDebate.answers.list.filter(
            (answer) => answer.id == answerId
          )[0]
      );
      return sorted;
    },
    isBeforeInBallotAnswerOrder: (state) => (id1, id2) => {
      let i1 = state.ballotAnswerOrder.indexOf(id1);
      let i2 = state.ballotAnswerOrder.indexOf(id2);
      return i1 < i2;
    },
  },
  mutations: {
    reset(state) {
      state.viewedDebate = null;
      state.argumentWeights = {};
      state.totalAnswerWeights = {};
      state.ballotAnswerOrder = [];
      state.manuallyOrdered = false;
    },
    setViewedDebate(state, { viewedDebate }) {
      state.viewedDebate = viewedDebate;
      if (state.viewedDebate) {
        state.viewedDebate.arguments.list.map((argument) => {
          if (!state.argumentWeights[argument.id]) {
            Vue.set(
              state.argumentWeights,
              argument.id,
              new ArgumentWeight(0, 0)
            );
          }
        });
        state.viewedDebate.answers.list.map((answer) => {
          if (!state.totalAnswerWeights[answer.id]) {
            Vue.set(
              state.totalAnswerWeights,
              answer.id,
              new ArgumentWeight(0, 0)
            );
          }
        });
      }
    },
    setVoteCast(state, { voteCast }) {
      state.viewedDebate.voteCast = voteCast;
    },
    changeWeight(state, { argumentId, weight }) {
      if (state.viewedDebate.err) {
        return;
      }

      let difference = weight.subtract(state.argumentWeights[argumentId]);
      Vue.set(state.argumentWeights, argumentId, weight);
      state.viewedDebate.arguments.list
        .filter((argument) => argument.id == argumentId)
        .map((argument) => {
          argument.supportedAnswers.list.map((answer) => {
            let old = state.totalAnswerWeights[answer.id];
            Vue.set(state.totalAnswerWeights, answer.id, old.add(difference));
          });
        });
    },
    setBallotAnswerOrderByWeights(state) {
      if (!state.viewedDebate || state.viewedDebate.err) {
        return;
      }

      let copy = state.viewedDebate.answers.list.slice();
      state.ballotAnswerOrder = copy
        .sort((answerA, answerB) => {
          let a = state.totalAnswerWeights[answerA.id];
          let b = state.totalAnswerWeights[answerB.id];
          return a.compare(b);
        })
        .map((answer) => answer.id)
        .reverse();
    },
    setBallotAnswerOrderByBallot(state, { ballot }) {
      state.manuallyOrdered = true;
      let answerRanking = ballot.answerRanking.slice();
      answerRanking.sort((rankedA, rankedB) =>
        rankedA.rank > rankedB.rank ? 1 : rankedA.rank < rankedB.rank ? -1 : 0
      );
      state.ballotAnswerOrder = answerRanking.map(
        (rankedAnswer) => rankedAnswer.answer.id
      );
    },
    moveBeforeInBallotAnswerOrder(state, { toMove, before }) {
      if (!state.ballotAnswerOrder) {
        return;
      }

      let newBallotAnswerOrder = state.ballotAnswerOrder.slice();
      let indexToRemove = newBallotAnswerOrder.indexOf(toMove);
      newBallotAnswerOrder.splice(indexToRemove, 1);
      let indexToInsert = newBallotAnswerOrder.indexOf(before);
      newBallotAnswerOrder.splice(indexToInsert, 0, toMove);
      state.ballotAnswerOrder = newBallotAnswerOrder;
    },
    moveAfterInBallotAnswerOrder(state, { toMove, after }) {
      if (!state.ballotAnswerOrder) {
        return;
      }

      if (toMove === after) {
        return;
      }

      let newBallotAnswerOrder = state.ballotAnswerOrder.slice();
      let indexToRemove = newBallotAnswerOrder.indexOf(toMove);
      newBallotAnswerOrder.splice(indexToRemove, 1);
      let indexToInsert = newBallotAnswerOrder.indexOf(after) + 1;
      newBallotAnswerOrder.splice(indexToInsert, 0, toMove);
      state.ballotAnswerOrder = newBallotAnswerOrder;
    },
    setManuallyOrdered(state, { manuallyOrdered }) {
      state.manuallyOrdered = manuallyOrdered;
    },
    moveUpInBallotAnswerOrder(state, { toMove }) {
      if (!state.ballotAnswerOrder) {
        return;
      }

      let i = state.ballotAnswerOrder.indexOf(toMove);
      if (i == 0) {
        return;
      }
      let newBallotAnswerOrder = state.ballotAnswerOrder.slice();
      newBallotAnswerOrder[i] = state.ballotAnswerOrder[i - 1];
      newBallotAnswerOrder[i - 1] = state.ballotAnswerOrder[i];
      state.ballotAnswerOrder = newBallotAnswerOrder;
    },
    moveDownInBallotAnswerOrder(state, { toMove }) {
      if (!state.ballotAnswerOrder) {
        return;
      }

      let i = state.ballotAnswerOrder.indexOf(toMove);
      if (i == state.ballotAnswerOrder.length - 1) {
        return;
      }
      let newBallotAnswerOrder = state.ballotAnswerOrder.slice();
      newBallotAnswerOrder[i] = state.ballotAnswerOrder[i + 1];
      newBallotAnswerOrder[i + 1] = state.ballotAnswerOrder[i];
      state.ballotAnswerOrder = newBallotAnswerOrder;
    },
  },
  actions: {
    changeViewedDebate(context, { viewedDebate }) {
      context.commit("setViewedDebate", { viewedDebate });

      viewedDebate.arguments.list.map((argument) => {
        if (argument.weight) {
          let weight = ArgumentWeight.fromGraphQLArgumentWeight(
            argument.weight
          );
          context.commit("changeWeight", { argumentId: argument.id, weight });
        }
      });

      if (viewedDebate.preparedVote.preparedBallot) {
        context.commit("setBallotAnswerOrderByBallot", {
          ballot: viewedDebate.preparedVote.preparedBallot.ballot,
        });
      } else {
        context.commit("setBallotAnswerOrderByWeights");
      }
    },
    changeWeightAndReorder(context, args) {
      context.commit("changeWeight", args);
      if (!context.state.manuallyOrdered) {
        context.commit("setBallotAnswerOrderByWeights");
      }
    },
  },
};
