import Vue from "vue";
import Vuex from "vuex";
import debate from "@/store/modules/debate";
import preview from "@/store/modules/preview";
import addArgument from "@/store/modules/addArgument";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== "production",
  modules: {
    debate,
    preview,
    addArgument,
  },
});
