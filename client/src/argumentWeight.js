import i18n from "./i18n";

export class ArgumentWeight {
  primaryWeight;
  secondaryWeight;

  constructor(primaryWeight, secondaryWeight) {
    this.primaryWeight = primaryWeight;
    this.secondaryWeight = secondaryWeight;
  }

  static fromGraphQLArgumentWeight(graphQLArgumentWeight) {
    if (graphQLArgumentWeight.infinite) {
      return new ArgumentWeight(1, 0);
    } else {
      return new ArgumentWeight(0, graphQLArgumentWeight.regularWeight);
    }
  }

  add(other) {
    return new ArgumentWeight(
      this.primaryWeight + other.primaryWeight,
      this.secondaryWeight + other.secondaryWeight
    );
  }

  subtract(other) {
    return new ArgumentWeight(
      this.primaryWeight - other.primaryWeight,
      this.secondaryWeight - other.secondaryWeight
    );
  }

  compare(other) {
    if (this.primaryWeight < other.primaryWeight) {
      return -1;
    } else if (this.primaryWeight > other.primaryWeight) {
      return 1;
    } else {
      if (this.secondaryWeight < other.secondaryWeight) {
        return -1;
      } else if (this.secondaryWeight > other.secondaryWeight) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  toString() {
    let primaryWeight =
      this.primaryWeight != 0
        ? (this.primaryWeight != 1 ? this.primaryWeight : "") + "∞"
        : "";
    let secondaryWeight =
      this.secondaryWeight != 0 || this.primaryWeight == ""
        ? this.secondaryWeight
        : "";
    let separator = primaryWeight != "" && secondaryWeight != "" ? " + " : "";
    let pointsStr =
      this.primaryWeight == 0 && this.secondaryWeight == 1
        ? i18n.t("point")
        : i18n.t("points");
    return primaryWeight + separator + secondaryWeight + " " + pointsStr;
  }

  get primaryWeight() {
    return this.primaryWeight;
  }

  get secondaryWeight() {
    return this.secondaryWeight;
  }
}
