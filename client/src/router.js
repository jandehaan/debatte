import Vue from "vue";
import Router from "vue-router";
import CreateAccount from "@/views/CreateAccount.vue";
import DebatesOverview from "./views/DebatesOverview.vue";
import Login from "./views/Login.vue";
import StartDebate from "@/views/StartDebate.vue";
import StartDebateAbout from "@/views/StartDebateAbout.vue";
import NotFound from "@/views/NotFound.vue";
import ViewDebate from "@/views/ViewDebate.vue";
import AddArgument from "@/views/AddArgument.vue";
import MainView from "@/views/MainView.vue";
import LoginLink from "@/views/LoginLink.vue";
import VerifyEmailLink from "@/views/VerifyEmailLink.vue";
import MyProfile from "@/views/MyProfile.vue";
import ResetPassword from "@/views/ResetPassword.vue";
import Quickstart from "@/views/Quickstart.vue";
import QuickstartPublic from "@/views/QuickstartPublic.vue";
import Legal from "@/views/Legal.vue";
import LoginTemp from "@/views/LoginTemp.vue";
import { SelectedButton } from "@/super-components/NavigationBar.vue";
import { graphQLClient } from "@/main";
import { localAuthTokenQuery, isAuthTokenValidQuery } from "@/queries";
import gql from "graphql-tag";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: MainView,
      children: [
        {
          path: "/",
          component: DebatesOverview,
          meta: {
            selectedButton: SelectedButton.Participate,
          },
        },
        {
          path: "/start-debate",
          component: StartDebate,
          meta: {
            selectedButton: SelectedButton.StartADebate,
          },
        },
        {
          path: "/start-debate-about",
          component: StartDebateAbout,
          meta: {
            selectedButton: SelectedButton.StartADebate,
          },
        },
        {
          path: "/debate/:debateId",
          name: "debate",
          component: ViewDebate,
          meta: {
            selectedButton: SelectedButton.Participate,
          },
        },
        {
          path: "/add-argument/:debateId",
          component: AddArgument,
          meta: {
            selectedButton: SelectedButton.Participate,
          },
        },
        {
          path: "/profile",
          component: MyProfile,
          meta: {
            selectedButton: SelectedButton.MyProfile,
          },
        },
        {
          path: "/legal",
          component: Legal,
          meta: {
            selectedButton: SelectedButton.MyProfile,
          },
        },
      ],
    },
    {
      path: "/register",
      component: CreateAccount,
    },
    {
      path: "/login/:token",
      component: LoginLink,
    },
    {
      path: "/verify-email/:token",
      component: VerifyEmailLink,
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/reset-password/:token?",
      component: ResetPassword,
    },
    {
      path: "/quickstart/:token",
      component: Quickstart,
    },
    {
      path: "/quickstart/public/:token",
      component: QuickstartPublic,
    },
    {
      path: "/try",
      component: LoginTemp,
    },
    {
      path: "*",
      name: "404",
      component: NotFound,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (
    to.path.startsWith("/login") ||
    to.path == "/register" ||
    to.path.startsWith("/verify-email") ||
    to.path.startsWith("/reset-password") ||
    to.path.startsWith("/quickstart") ||
    to.path.startsWith("/try")
  ) {
    next();
  } else {
    graphQLClient
      .query({
        query: localAuthTokenQuery,
      })
      .then((result) => {
        if (result.data == null) {
          next("/login");
        } else {
          let token = result.data.localAuthToken;
          graphQLClient
            .query({
              query: isAuthTokenValidQuery,
              variables: { token: token },
            })
            .then((result) => {
              if (result.data.isAuthTokenValid.ok) {
                next();
              } else {
                next("/login");
              }
            });
        }
      });
  }
});

export default router;
