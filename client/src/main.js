import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { onError } from "apollo-link-error";
import { ApolloLink } from "apollo-link";
import Vue from "vue";
import VueApollo from "vue-apollo";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import "@/assets/main.css";
import gql from "graphql-tag";
import { localAuthTokenQuery, setLocalAuthTokenMutation } from "@/queries";
import store from "@/store";
import i18n from "./i18n";

Vue.use(VueApollo);

Vue.config.productionTip = false;

export const schema = gql`
  extend type Query {
    localAuthToken: String
  }

  extend type Mutation {
    setLocalAuthToken(token: String!): String!
  }
`;

export const LOCAL_AUTH_TOKEN_KEY = "localAuthToken";

const resolvers = {
  Mutation: {
    setLocalAuthToken: (_, { token }, { cache }) => {
      cache.writeQuery({
        query: localAuthTokenQuery,
        data: { localAuthToken: token },
      });
      localStorage.setItem(LOCAL_AUTH_TOKEN_KEY, token);
      return token;
    },
  },
};

const cache = new InMemoryCache();
let token = localStorage.getItem(LOCAL_AUTH_TOKEN_KEY);
if (token != null) {
  cache.writeQuery({
    query: localAuthTokenQuery,
    data: { localAuthToken: token },
  });
}

let headers = [];
if (process.env.VUE_APP_USE_BASIC_AUTH === "true") {
  headers = {
    Authorization: "Basic c3RhZ2luZzpNTGtmNVVpeFRWbkI3NVlUTEZnV2lTd3I=",
  };
}

const apolloClient = new ApolloClient({
  link: ApolloLink.from([
    new HttpLink({
      uri: process.env.VUE_APP_API_SERVER + "/graphql",
      headers: headers,
    }),
  ]),
  cache: cache,
  schema,
  resolvers,
});

export const graphQLClient = apolloClient;

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

Vue.directive("focus", {
  inserted: function (el) {
    el.focus();
  },
});

new Vue({
  router,
  apolloProvider,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
