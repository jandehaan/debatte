import gql from "graphql-tag";

export const localAuthTokenQuery = gql`
  query LocalAuthToken {
    localAuthToken @client
  }
`;

export const setLocalAuthTokenMutation = gql`
  mutation SetLocalAuthToken($token: String!) {
    setLocalAuthToken(token: $token) @client
  }
`;

export const isAuthTokenValidQuery = gql`
  query IsAuthTokenValid($token: String!) {
    isAuthTokenValid(token: $token) {
      ok
      err
    }
  }
`;
