const purgecss = require("@fullhuman/postcss-purgecss")({
  content: ["./src/**/*.html", "./src/**/*.vue", "./public/**/*.html"],
  whitelist: ["flip-list-move"],
  whitelistPatterns: [/fade-.*/, /slide-.*/],
  defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || [],
});

module.exports = {
  plugins: [
    require("tailwindcss")("tailwind.config.js"),
    require("autoprefixer")(),
    ...(process.env.NODE_ENV === "production" ? [purgecss] : []),
  ],
};
