#!/bin/sh

inkscape -e android-chrome-192x192.png logo.dist.svg --export-width 192
inkscape -e android-chrome-512x512.png logo.dist.svg --export-width 512
inkscape -e apple-touch-icon-60x60.png logo.dist.svg --export-width 60
inkscape -e apple-touch-icon-76x76.png logo.dist.svg --export-width 76
inkscape -e apple-touch-icon-120x120.png logo.dist.svg --export-width 120
inkscape -e apple-touch-icon-152x152.png logo.dist.svg --export-width 152
inkscape -e apple-touch-icon-180x180.png logo.dist.svg --export-width 180
inkscape -e apple-touch-icon.png logo.dist.svg --export-width 180
inkscape -e favicon-16x16.png logo.dist.svg --export-width 16
inkscape -e favicon-32x32.png logo.dist.svg --export-width 32
inkscape -e msapplication-icon-144x144.png logo.dist.svg --export-width 144
inkscape -e mstile-150x150.png logo.dist.svg --export-width 150
cp logo.dist.svg safari-pinned-tab.svg
convert -background transparent favicon-32x32.png favicon.ico
