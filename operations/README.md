# Eltrot: Automated Deployment

If you just want to deploy the application and not change anything in the deployment process, the best option will be to just use the Gitlab continuous deployment job as described in the top-level README.

Otherwise, it is more practical to run the deployment on your local machine, as follows:
- Install Ansible (these are the commands for Ubuntu, for other systems see the [Ansible installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)).
```sh
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```
- Install the following python modules
(`sudo python -m pip install ...` –
using `python -m pip` instead of just `pip` is important to install packages for the correct version of Python):
    - `hcloud`
- Get the private SSH key that allows access to the virtual machine(s) the server, database, web server and mail server (should) run on and pass its path as `YOUR_KEY_FILE`. Get the password for the encrypted secrets in the ansible files and save it in a file whose path you pass as `YOUR_PASSWORD_FILE`. If you want to deploy to production, add the argument `--extra-vars 'env="production"'` to all commands, otherwise, you will deploy to staging (this affects the domains that are used). `provision.yml` only has to be run when there are no virtual machines yet, otherwise running it is unnecessary.
```sh
ansible-playbook provision.yml --private-key YOUR_KEY_FILE --vault-password-file YOUR_PASSWORD_FILE --extra-vars 'env="production"'
ansible-playbook prepare.yml --private-key YOUR_KEY_FILE --vault-password-file YOUR_PASSWORD_FILE --extra-vars 'env="production"'
ansible-playbook site.yml --private-key YOUR_KEY_FILE --vault-password-file YOUR_PASSWORD_FILE --extra-vars 'env="production"'
```
