# Eltrot: Server

The executable `eltrot` simply calls a single function in the library with the same name.
The part depending on Diesel for database access is split into a separate crate in an attempt to reduce compilation times.

## Developing

- Make sure that Postgres is installed – on Ubuntu, that means running
```sh
sudo apt install postgresql
```
- Make sure that OpenSSL (libraries for development) is installed – on Ubuntu, that means running 
```sh
sudo apt install libssl-dev
```
- The embedded LaTeX engine has a lot of additional dependencies: 
```sh
sudo apt install libfontconfig1-dev libgraphite2-dev libharfbuzz-dev libicu-dev zlib1g-dev
```
- Install the Diesel CLI: 
```sh
cargo install diesel_cli --no-default-features --features postgres.
```
- Create a database user `debatte_user`

> Run `sudo -u postgres psql`,
> then enter the command `create user debatte_user createdb password 'devpwd';`,
> type `\q` to exit.

- Run
```sh
diesel setup
```

### Debugging

> If you want to debug the executable using VS Code,
> make sure that `lldb` is installed
> (on Ubuntu: `sudo apt install lldb`).

### Tests

- To test sending emails, you need a local SMTP server listening on port 2525,
e.g. [FakeSMTP](http://nilhcem.com/FakeSMTP/download.html) (includes a GUI).
- To run the integration tests for the `db::connection` (i.e. using a real database),
run `diesel setup` **in the `tests` directory**.
Diesel will setup a second database for running the integration tests.

### Deployment

- The directory `res` contains files that are required while the program is running and must be copied along with the executable.