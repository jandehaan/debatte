use askama_escape::Escaper;
use std::collections::HashMap;
use std::fmt::{self, Write};

pub struct Tex;

impl Escaper for Tex {
    fn write_escaped<W>(&self, mut fmt: W, string: &str) -> fmt::Result
    where
        W: Write,
    {
        let escaped = escape_for_latex(string);
        fmt.write_str(&escaped)
    }
}

pub fn escape_for_latex(s: &str) -> String {
    let mut escapes = HashMap::new();
    escapes.insert("&", "\\&");
    escapes.insert("%", "\\%");
    escapes.insert("$", "\\$");
    escapes.insert("#", "\\#");
    escapes.insert("_", "\\_");
    escapes.insert("{", "\\{");
    escapes.insert("}", "\\}");
    escapes.insert("~", "\\textasciitilde");
    escapes.insert("^", "\\textasciicircum");
    let escapes = escapes;
    let mut s = String::from(s);
    // Needs to be done before all other escapes, otherwise you get results such as "_" -> "\\textbackslash_"
    s = s.replace("\\", "\\textbackslash");
    for (to_escape, with) in escapes {
        s = s.replace(to_escape, with);
    }
    s
}
