use chrono::Duration;

use crate::email::templates::{self, create_email};
use crate::email::*;
use crate::error::*;
use crate::logic::authorization::*;
use crate::logic::debate::summary::*;
use crate::logic::participation::*;
use crate::logic::*;
use db_access::db::Db;
use db_access::models::debate_summaries::*;

pub fn perform(db: &dyn Db, email_sender: &dyn EmailSender) -> Result<(), Error> {
    generate_missing_summaries(db, email_sender)?;
    delete_old_debates(db)?;
    delete_expired_auth_tokens(db)?;
    delete_expired_links(db)?;
    delete_deleted_accounts(db)?;
    delete_old_temporary_accounts(db)?;
    Ok(())
}

fn generate_missing_summaries(db: &dyn Db, email_sender: &dyn EmailSender) -> Result<(), Error> {
    for debate_missing_summary in db.get_debates_missing_summary()? {
        let permissions = BackgroundTaskPermissions::new();
        let summary = summarize_debate(db, &permissions, debate_missing_summary.external_id)?;
        let pdf = pdf::summarized_debate_to_pdf(&summary)?;
        let json = json::summarized_debate_to_json(&summary)?;
        let pdf = db
            .insert_debate_summary(DebateSummary {
                debate: debate_missing_summary.id,
                pdf,
                json,
            })?
            .pdf;
        let ballots = ballot::get_ballots_of_debate(db, &permissions, &debate_missing_summary)?
            .ok_or(RetrievalErrorKind::Other)?;
        let ranking = vote::ranked_pairs::count_ballots(&ballots)
            .ok()
            .map(|raw_ranking| {
                raw_ranking
                    .0
                    .into_iter()
                    .map(|ranked_answer| -> Result<_, Error> {
                        Ok(
                            answer::get_answer_of_ranked_answer(db, &permissions, &ranked_answer)?
                                .answer,
                        )
                    })
                    .collect::<Result<Vec<_>, _>>()
            })
            .transpose()?;
        let top_answer = ranking.map(|r| r.into_iter().next().unwrap_or_default());
        for account in participants_of_debate(db, &permissions, &debate_missing_summary)? {
            let account = account::get_account(db, &permissions, &account.external_id)?;
            let email = create_email(&templates::summary::EmailInput {
                locale: account.locale.into(),
                account: &account,
                debate: &debate_missing_summary,
                pdf_summary: &pdf,
                top_answer: top_answer.as_ref().map(|s| &s[..]),
            });
            if !account.temporary {
                email_sender.send(email)?;
            }
        }
    }
    Ok(())
}

fn delete_old_debates(db: &dyn Db) -> Result<(), Error> {
    for debate in db.get_old_finished_debates(Duration::days(30))?.iter() {
        db.delete_debate(debate.id)?;
    }
    Ok(())
}

fn delete_expired_links(db: &dyn Db) -> Result<(), Error> {
    db.delete_old_login_links(Duration::days(3))?;
    db.delete_old_password_resets(Duration::days(3))?;
    db.delete_old_verify_email_links(Duration::days(3))?;
    Ok(())
}

fn delete_expired_auth_tokens(db: &dyn Db) -> Result<(), Error> {
    db.delete_old_auth_tokens(Duration::days(90))?;
    Ok(())
}

fn delete_deleted_accounts(db: &dyn Db) -> Result<(), Error> {
    db.delete_accounts_ready_for_deletion()?;
    Ok(())
}

fn delete_old_temporary_accounts(db: &dyn Db) -> Result<(), Error> {
    db.delete_old_temporary_accounts(Duration::minutes(30))?;
    Ok(())
}
