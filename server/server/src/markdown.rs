use pulldown_cmark::{Event, Parser, Tag};

use crate::tex::escape_for_latex;

pub fn truncate_markdown(max_length: usize, markdown: &str) -> String {
    let (end, closing_syntax) = find_end(max_length, markdown);
    markdown[0..end].to_owned() + &closing_syntax
}

pub fn markdown_to_latex(markdown: &str) -> Result<String, askama::Error> {
    let parser = Parser::new(markdown);
    Ok(parser
        .into_offset_iter()
        .map(|(event, _)| match event {
            Event::Code(code) => format!(r#"\verb|{}|"#, escape_for_latex(&code)),
            Event::HardBreak => r#"\\"#.to_owned(),
            Event::Rule => r#"\hrule"#.to_owned(),
            Event::SoftBreak => "\n".to_owned(),
            Event::Text(text) => escape_for_latex(&text),
            Event::Start(Tag::BlockQuote) => r#"\begin{quote}"#.to_owned(),
            Event::End(Tag::BlockQuote) => r#"\end{quote}"#.to_owned(),
            Event::Start(Tag::CodeBlock(_)) => r#"\begin{verbatim}"#.to_owned(),
            Event::End(Tag::CodeBlock(_)) => r#"\end{verbatim}"#.to_owned(),
            Event::Start(Tag::Emphasis) => r#"\emph{"#.to_owned(),
            Event::End(Tag::Emphasis) => r#"}"#.to_owned(),
            Event::Start(Tag::Heading(1)) => r#"\userheading{"#.to_owned(),
            Event::End(Tag::Heading(1)) => r#"}"#.to_owned(),
            Event::Start(Tag::Heading(2)) => r#"\usersubheading{"#.to_owned(),
            Event::End(Tag::Heading(2)) => r#"}"#.to_owned(),
            Event::Start(Tag::Item) => r#"\item "#.to_owned(),
            Event::End(Tag::Item) => "\n".to_owned(),
            Event::Start(Tag::Link(_, url, title)) => format!(
                r#"\href{{{}}}{{\texttt{{{}}}}}"#,
                escape_for_latex(&url),
                escape_for_latex(&title)
            ),
            Event::Start(Tag::List(_)) => r#"\begin{itemize}"#.to_owned(),
            Event::End(Tag::List(_)) => r#"\end{itemize}"#.to_owned(),
            Event::Start(Tag::Paragraph) => String::new(),
            Event::End(Tag::Paragraph) => "\n\n".to_owned(),
            Event::Start(Tag::Strong) => r#"\textbf{"#.to_owned(),
            Event::End(Tag::Strong) => r#"}"#.to_owned(),
            _ => String::new(),
        })
        .collect())
}

fn find_end(max_length: usize, markdown: &str) -> (usize, String) {
    let parser = Parser::new(markdown);
    let mut open_tags = Vec::new();
    let mut closing_syntax = String::new();
    let mut possible_ends: Vec<(usize, String)> = Vec::new();
    for (event, range) in parser.into_offset_iter() {
        match event {
            Event::Start(tag) => {
                closing_syntax += get_closing_syntax(&tag).unwrap_or("");
                open_tags.push(tag);
            }
            Event::End(tag) => {
                let position = range.end;
                open_tags.pop();
                let d = get_closing_syntax(&tag).map(|s| s.len()).unwrap_or(0);
                closing_syntax.truncate(closing_syntax.len() - d);

                if can_truncate(&open_tags) {
                    possible_ends.push((position, closing_syntax.clone()));
                }
            }
            Event::Text(text) => {
                let position = range.start;
                let mut offset = 0;
                if can_truncate(&open_tags) {
                    let too_long = position + closing_syntax.len() > max_length;
                    if too_long {
                        return possible_ends.last().unwrap_or(&(0, String::new())).clone();
                    }
                    for c in text.chars() {
                        offset += 1;

                        let too_long = if c == ' ' || c == '\n' {
                            position + offset - 1 + closing_syntax.len() > max_length
                        } else {
                            position + offset + closing_syntax.len() > max_length
                        };
                        if too_long {
                            return possible_ends.last().unwrap_or(&(0, String::new())).clone();
                        }
                        if c == ' ' || c == '\n' {
                            possible_ends.push((position + offset - 1, closing_syntax.clone()));
                        }
                    }
                    possible_ends.push((position + offset, closing_syntax.clone()));
                }
            }
            _ => (),
        }
    }
    (markdown.len(), String::new())
}

fn can_truncate<'a>(open_tags: &[Tag<'a>]) -> bool {
    open_tags
        .iter()
        .map(|tag| get_closing_syntax(tag).is_some())
        .all(|b| b)
}

fn get_closing_syntax(tag: &Tag) -> Option<&'static str> {
    match tag {
        Tag::Emphasis => Some("*"),
        Tag::Strong => Some("**"),
        Tag::BlockQuote | Tag::Heading(_) | Tag::Item | Tag::List(_) | Tag::Paragraph => Some(""),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_truncate_markdown() {
        assert_eq!(truncate_markdown(9, "bla *bla bla*"), "bla *bla*");
        assert_eq!(truncate_markdown(8, "bla *bla bla*"), "bla ");
        assert_eq!(truncate_markdown(11, "bla **bla bla**"), "bla **bla**");
        assert_eq!(truncate_markdown(8, "> bla\n> bla"), "> bla");
        assert_eq!(truncate_markdown(4, "bla"), "bla");
        assert_eq!(truncate_markdown(0, ""), "");
        assert_eq!(truncate_markdown(1, ""), "");
    }
}
