pub trait SwapResult<T, E> {
    fn swap(self) -> Result<E, T>;
}

impl<T, E> SwapResult<T, E> for Result<T, E> {
    fn swap(self) -> Result<E, T> {
        match self {
            Ok(x) => Err(x),
            Err(e) => Ok(e),
        }
    }
}
