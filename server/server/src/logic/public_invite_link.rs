use chrono::Utc;

use crate::email::templates::*;
use crate::email::*;
use crate::error::public::*;
use crate::logic::authorization::*;
use crate::logic::tokens::secret_token::*;
use db_access::db::*;
use db_access::models::invite_links::*;
use db_access::models::participations::*;
use db_access::models::types::*;

#[derive(Debug, GraphQLEnum)]
pub enum UsePublicInviteLinkAction {
    EmailSent,
    AccountAdded,
}

pub fn use_public_invite_link(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    token: &str,
    email: &str,
    locale: Locale,
) -> Result<UsePublicInviteLinkAction, PublicError<UsePublicInviteLinkError>> {
    match db.get_debate_with_public_invite_link_token(token) {
        Ok(debate) => {
            if debate
                .collecting_answers_start
                .ok_or(PublicErrorKind::Other)?
                > Utc::now()
            {
                match db.get_account_with_email(email) {
                    Ok(account) => {
                        db.insert_participations(&[NewParticipation {
                            debate: debate.id,
                            participant: account.id,
                        }])
                        .context(PublicErrorKind::Other)?;
                        Ok(UsePublicInviteLinkAction::AccountAdded)
                    }
                    Err(QueryError::NotFound) => {
                        let new_token = SecretToken::generate();
                        db.insert_invite_link(NewInviteLink {
                            debate: debate.id,
                            email: email.to_owned(),
                            name: String::new(),
                            proof_token_hash: hash_secret_token(&new_token),
                            invited_time: Utc::now(),
                            invited_by: None,
                        })
                        .context(PublicErrorKind::Other)?;
                        email_sender
                            .send(create_email(&templates::invitation_public::EmailInput {
                                locale: locale.into(),
                                debate: &debate,
                                invite_link: &format!(
                                    "{}/quickstart/{}?email={}",
                                    email_sender.get_link_server(),
                                    new_token.to_str(),
                                    email
                                ),
                                invitee_email: email,
                            }))
                            .map_err(|_| {
                                PublicErrorKind::Public(UsePublicInviteLinkError::CannotSendEmail)
                            })?;
                        Ok(UsePublicInviteLinkAction::EmailSent)
                    }
                    Err(e) => Err(e).context(PublicErrorKind::Other)?,
                }
            } else {
                Err(
                    PublicErrorKind::Public(UsePublicInviteLinkError::DebateHasAlreadyStarted)
                        .into(),
                )
            }
        }
        Err(QueryError::NotFound) => {
            Err(PublicErrorKind::Public(UsePublicInviteLinkError::InvalidToken).into())
        }
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    }
}

pub fn get_public_invite_link_of_debate(link_host: &str, debate: &Debate) -> Option<String> {
    debate
        .public_invite_link_token
        .as_ref()
        .map(|token| format!("{}/quickstart/public/{}", link_host, token))
}
