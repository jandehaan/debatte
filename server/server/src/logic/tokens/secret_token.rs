use sha2::Digest;

use super::random_bytes;

const SECRET_LENGTH: usize = 32;

fn secret_bytes_from_slice(slice: &[u8]) -> [u8; SECRET_LENGTH] {
    let mut array = [0; SECRET_LENGTH];
    let bytes = &slice[..array.len()];
    array.copy_from_slice(bytes);
    array
}

#[derive(Debug, PartialEq, Eq)]
pub struct SecretToken([u8; SECRET_LENGTH]);

impl SecretToken {
    pub fn generate() -> SecretToken {
        SecretToken(secret_bytes_from_slice(&random_bytes(SECRET_LENGTH)))
    }

    pub fn try_from_str(s: &str) -> Option<SecretToken> {
        let decoded_bytes = base64::decode_config(s, base64::URL_SAFE).ok()?;
        let secret = secret_bytes_from_slice(&decoded_bytes[..]);
        Some(SecretToken(secret))
    }

    pub fn to_str(&self) -> String {
        base64::encode_config(&self.0, base64::URL_SAFE)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_secret_token_parsing() {
        let secret = [1; SECRET_LENGTH];
        let token = SecretToken(secret);
        let s = token.to_str();
        assert_eq!(SecretToken::try_from_str(&s), Some(token));
    }
}

pub fn hash_secret_token(secret_token: &SecretToken) -> String {
    let mut hasher = sha2::Sha256::new();
    hasher.input(&secret_token.0.clone());
    base64::encode_config(&hasher.result()[..], base64::URL_SAFE)
}
