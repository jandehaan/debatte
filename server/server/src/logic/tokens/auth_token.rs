use chrono::{Duration, Utc};
use failure::Error;
use sha2::Digest;
use uuid::Uuid;

use super::random_bytes;
use crate::error::public::*;
use crate::logic::account::*;
use crate::logic::authorization::*;
use crate::logic::tokens::secret_token::*;
use db_access::db::*;
use db_access::models::auth_tokens::*;

pub use crate::error::public::{LoginError, TokenAuthError};

const SECRET_LENGTH: usize = 32;

fn secret_bytes_from_slice(slice: &[u8]) -> [u8; SECRET_LENGTH] {
    let mut array = [0; SECRET_LENGTH];
    let bytes = &slice[..array.len()];
    array.copy_from_slice(bytes);
    array
}

#[derive(Debug, PartialEq, Eq)]
pub struct OpaqueAuthToken {
    account_id: Uuid,
    secret: [u8; SECRET_LENGTH],
}

impl OpaqueAuthToken {
    pub fn generate(account_id: Uuid) -> OpaqueAuthToken {
        let secret = secret_bytes_from_slice(&random_bytes(SECRET_LENGTH));
        OpaqueAuthToken { account_id, secret }
    }

    pub fn try_from_str(s: &str) -> Option<OpaqueAuthToken> {
        const UUID_LENGTH: usize = 16;

        let decoded_bytes = base64::decode(s).ok()?;
        let uuid_bytes = &decoded_bytes[0..UUID_LENGTH];
        let secret_bytes = &decoded_bytes[UUID_LENGTH..UUID_LENGTH + SECRET_LENGTH];
        let account_id = Uuid::from_slice(uuid_bytes).ok()?;
        let secret = secret_bytes_from_slice(secret_bytes);
        Some(OpaqueAuthToken { account_id, secret })
    }

    pub fn to_str(&self) -> String {
        let uuid_bytes = self.account_id.as_bytes();
        let secret_bytes = self.secret;
        let mut token_bytes = Vec::from(&uuid_bytes[..]);
        token_bytes.append(&mut Vec::from(&secret_bytes[..]));
        base64::encode(&token_bytes)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_opaque_token_parsing() {
        let account_id = Uuid::new_v4();
        let secret = [1; SECRET_LENGTH];
        let token = OpaqueAuthToken { account_id, secret };
        let s = token.to_str();
        assert_eq!(OpaqueAuthToken::try_from_str(&s), Some(token));
    }
}

pub fn login_with_password(
    db: &dyn Db,
    email: &str,
    password: &str,
) -> Result<OpaqueAuthToken, PublicError<LoginError>> {
    let auth_context = login_once_with_password(db, email, password)?;
    let account = auth_context.account();
    if account.email_verified {
        let token = save_new_auth_token(db, account.id).context(PublicErrorKind::Other)?;
        Ok(token)
    } else {
        Err(PublicErrorKind::Public(LoginError::EmailNotVerified).into())
    }
}

pub fn login_once_with_password(
    db: &dyn Db,
    email: &str,
    password: &str,
) -> Result<AuthContext, PublicError<LoginError>> {
    let account = db.get_account_with_email(email);
    match account {
        Ok(account) => match (account.password_hash.as_ref(), account.salt.as_ref()) {
            (Some(password_hash), Some(salt)) => {
                if verify_password(password_hash, salt, password) {
                    Ok(AuthContext::new(account))
                } else {
                    Err(PublicErrorKind::Public(LoginError::InvalidEmailOrPassword).into())
                }
            }
            (None, None) => Err(PublicErrorKind::Public(LoginError::QuicklyCreatedAccount).into()),
            (_, _) => Err(format_err!("Inconsistent data in DB – the account with this ID has a salt or a password hash, but not both: {}", account.external_id))
                .context(PublicErrorKind::Other)?,
        },
        Err(QueryError::NotFound) => Err(PublicErrorKind::Public(LoginError::InvalidEmailOrPassword).into()),
        Err(e) => Err(format_err!("While trying to get account by email: {:?}", e)).context(PublicErrorKind::Other)?,
    }
}

pub fn login_special(db: &dyn Db, email: &str) -> Result<OpaqueAuthToken, PublicError<LoginError>> {
    let account = db
        .get_account_with_email(email)
        .context(PublicErrorKind::Other)?;
    if account.password_hash.is_none() {
        let token = save_new_auth_token(db, account.id).context(PublicErrorKind::Other)?;
        Ok(token)
    } else {
        Err(PublicErrorKind::Public(LoginError::InvalidEmailOrPassword).into())
    }
}

pub fn login_with_link(
    db: &dyn Db,
    token: &str,
) -> Result<OpaqueAuthToken, PublicError<TokenAuthError>> {
    let token = SecretToken::try_from_str(token)
        .ok_or(PublicErrorKind::Public(TokenAuthError::MalformedToken))?;
    let hash = hash_secret_token(&token);
    let login_link = match db.get_login_link_with_hash(&hash) {
        Ok(token) => Ok(token),
        Err(QueryError::NotFound) => Err(PublicErrorKind::Public(TokenAuthError::InvalidToken)),
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    }?;
    db.delete_login_link(login_link.id)
        .context(PublicErrorKind::Other)?;
    if login_link.issued_time >= Utc::now() - Duration::hours(1) {
        Ok(save_new_auth_token(db, login_link.account_id).context(PublicErrorKind::Other)?)
    } else {
        Err(PublicErrorKind::Public(TokenAuthError::InvalidToken).into())
    }
}

pub fn login_to_temporary_account(db: &dyn Db, account_id: Uuid) -> Result<OpaqueAuthToken, Error> {
    let account = db.get_account_with_external_id(&account_id)?
        .map_err(|_| format_err!("Temporary account has no email instead of empty email, is thus not recognized as a proper account."))?;
    let token = save_new_auth_token(db, account.id)?;
    Ok(token)
}

#[derive(Debug, juniper::GraphQLEnum)]
pub enum LogOutResult {
    Success,
    Error,
}

pub fn log_out(db: &dyn Db, auth_context: &AuthContext) -> LogOutResult {
    match db.delete_auth_tokens_of_account(auth_context.account().id) {
        Ok(_) => LogOutResult::Success,
        Err(_) => LogOutResult::Error,
    }
}

pub fn authenticate_with_token(
    db: &dyn Db,
    auth_token: &OpaqueAuthToken,
) -> Result<Account, PublicError<TokenAuthError>> {
    let token_row = db.get_auth_token_with_hash(&hash_auth_token_secret(auth_token));
    let claimed_account: Account = match db.get_account_with_external_id(&auth_token.account_id) {
        Err(QueryError::NotFound) => {
            return Err(PublicErrorKind::Public(TokenAuthError::UnknownAccount).into())
        }
        Ok(Err(_)) => return Err(PublicErrorKind::Public(TokenAuthError::UnknownAccount).into()),
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
        Ok(Ok(a)) => a,
    };
    match token_row {
        Ok(token_row) => {
            if token_row.account_id == claimed_account.id {
                Ok(claimed_account)
            } else {
                Err(PublicErrorKind::Public(TokenAuthError::InvalidToken).into())
            }
        }
        Err(QueryError::NotFound) => {
            Err(PublicErrorKind::Public(TokenAuthError::InvalidToken).into())
        }
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    }
}

fn save_new_auth_token(db: &dyn Db, account_id: i64) -> QueryResult<OpaqueAuthToken> {
    let external_account_id = db
        .get_account_with_id(account_id)?
        .map_err(|_| QueryError::NotFound)?
        .external_id;
    let auth_token = OpaqueAuthToken::generate(external_account_id);
    let auth_token_hash = hash_auth_token_secret(&auth_token);
    let new_auth_token = NewAuthToken {
        auth_token_hash,
        account_id,
        issued_time: chrono::Utc::now(),
    };
    db.insert_auth_token(&new_auth_token).map(|_| auth_token)
}

fn hash_auth_token_secret(auth_token: &OpaqueAuthToken) -> String {
    let mut hasher = sha2::Sha256::new();
    hasher.input(&auth_token.secret.clone());
    base64::encode(&hasher.result()[..])
}
