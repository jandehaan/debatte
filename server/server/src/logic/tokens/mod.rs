pub mod auth_token;
pub mod secret_token;

pub fn random_bytes(n: usize) -> Vec<u8> {
    vec![0; n].iter().map(|_| rand::random::<u8>()).collect()
}
