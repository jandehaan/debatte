use duct::cmd;

pub fn to_tikz(s: &str) -> Result<String, ()> {
    let tikz_code = cmd!("dot", "-Txdot")
        .pipe(cmd!("dot2tex", "--figonly"))
        .stdin_bytes(s)
        .read()
        .map_err(|_| ())?;
    Ok(tikz_code)
}
