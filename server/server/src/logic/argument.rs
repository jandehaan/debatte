use std::convert::TryFrom;

use chrono::Utc;
use failure::Error;
use uuid::Uuid;

use crate::error::*;
use crate::logic::answer::*;
use crate::logic::authorization::*;
use crate::logic::ballot::*;
use crate::logic::debate::*;
use crate::logic::participation::*;
use crate::logic::vote::*;
use db_access::db::*;
use db_access::models;
use db_access::models::arguments::*;
use db_access::models::supports::*;

pub use db_access::models::answers::Answer;
pub use db_access::models::arguments::Argument;
pub use db_access::models::debates::Debate;

pub fn get_argument(db: &dyn Db, auth_context: &AuthContext, id: i64) -> Result<Argument, Error> {
    let argument = db.get_argument_with_id(id)?;
    if auth_context.may_access_argument(db, &argument)? {
        Ok(argument)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_argument_with_external_id(
    db: &dyn Db,
    auth_context: &AuthContext,
    external_id: Uuid,
) -> Result<WithAuthContext<Argument>, Error> {
    let argument = db.get_argument_with_external_id(external_id)?;
    if auth_context.may_access_argument(db, &argument)? {
        Ok(auth_context.with(argument))
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn belongs_to(argument: &Argument, debate: &Debate) -> bool {
    argument.debate == debate.id
}

pub fn get_arguments_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Vec<Argument>, Error> {
    if permissions.may_access_debate(db, debate)? {
        let arguments = db.get_arguments_of_debate(debate.id)?;
        Ok(arguments)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_considered_arguments_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Vec<Argument>, Error> {
    let all_arguments = get_arguments_of_debate(db, permissions, debate)?;
    let debate_state = implicit_debate_state(db, debate, Utc::now())?;
    let considered_arguments =
        if debate_state == DebateState::Voting || debate_state == DebateState::Finished {
            all_arguments
                .iter()
                .map(|argument| consider(db, permissions, argument))
                .collect::<Result<Vec<_>, _>>()?
                .into_iter()
                .zip(all_arguments.into_iter())
                .filter(|(consider, _)| *consider)
                .map(|(_, argument)| argument)
                .collect()
        } else {
            all_arguments
        };
    Ok(considered_arguments)
}

pub fn get_unconsidered_arguments_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Vec<Argument>, Error> {
    let all_arguments = get_arguments_of_debate(db, permissions, debate)?;
    let debate_state = implicit_debate_state(db, debate, Utc::now())?;
    let unconsidered_arguments =
        if debate_state == DebateState::Voting || debate_state == DebateState::Finished {
            all_arguments
                .iter()
                .map(|argument| consider(db, permissions, argument))
                .collect::<Result<Vec<_>, _>>()?
                .into_iter()
                .zip(all_arguments.into_iter())
                .filter(|(consider, _)| !consider)
                .map(|(_, argument)| argument)
                .collect()
        } else {
            all_arguments
        };
    Ok(unconsidered_arguments)
}

pub fn get_concerned_argument_of_debate(
    db: &dyn Db,
    auth_context: &AuthContext,
    debate: &Debate,
) -> Result<Option<WithAuthContext<Argument>>, Error> {
    match debate.concerned_argument {
        None => Ok(None),
        Some(concerned_argument_id) => {
            let argument = db.get_argument_with_id(concerned_argument_id)?;
            Ok(Some(auth_context.with(argument)))
        }
    }
}

pub fn get_supported_answers(
    db: &dyn Db,
    permissions: &impl Permissions,
    argument: &Argument,
) -> Result<Vec<Answer>, Error> {
    let debate = db.get_debate_of_argument(argument.id)?;
    if permissions.may_access_debate(db, &debate)? {
        let answers = db.get_supported_answers_of_argument(argument.id)?;
        Ok(answers)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

#[derive(Debug)]
pub enum ArgumentWeight {
    Infinite,
    Regular(i32),
}

impl ArgumentWeight {
    fn into_db_i32(self) -> i32 {
        match self {
            ArgumentWeight::Infinite => 6,
            ArgumentWeight::Regular(weight) => weight,
        }
    }
}

impl TryFrom<models::argument_weights::ArgumentWeight> for ArgumentWeight {
    type Error = ();

    fn try_from(
        model_weight: models::argument_weights::ArgumentWeight,
    ) -> Result<ArgumentWeight, Self::Error> {
        match model_weight.weight {
            weight @ 0..=5 => Ok(ArgumentWeight::Regular(weight)),
            6 => Ok(ArgumentWeight::Infinite),
            _ => Err(()),
        }
    }
}

pub fn get_argument_weight(
    db: &dyn Db,
    auth_context: &AuthContext,
    argument: &Argument,
) -> Result<Option<ArgumentWeight>, Error> {
    if auth_context.may_access_argument(db, argument)? {
        let model_weight = match db.get_argument_weight(auth_context.account().id, argument.id) {
            Err(QueryError::NotFound) => Ok(None),
            Ok(model_weight) => Ok(Some(model_weight)),
            Err(e) => Err(e),
        }?;
        let model_weight_id = model_weight.as_ref().map(|model_weight| model_weight.id);
        model_weight
            .map(ArgumentWeight::try_from)
            .transpose()
            .map_err(|_| format_err!("Could not create argument weight from data in DB. Argument weight’s DB id: {:?}", model_weight_id))
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn consider(
    db: &dyn Db,
    permissions: &impl Permissions,
    argument: &Argument,
) -> Result<bool, Error> {
    let debates_about_argument = get_debates_about_argument(db, permissions, argument)?;
    let consider = debates_about_argument
        .iter()
        .map(|debate_about_argument| -> Result<_, Error> {
            if debate_about_argument.kind == DebateKind::MeritOfArgument
                || debate_about_argument.kind == DebateKind::RelevanceOfArgument
            {
                let YesNoAnswers { yes, no } =
                    get_yes_no_answers_of_debate(db, permissions, &debate_about_argument)?;
                let ballots = get_ballots_of_debate(db, permissions, debate_about_argument)?
                    .ok_or(RetrievalErrorKind::Other)?;
                let (n_yes, _n_no) =
                    two_choices::count_ballots(ballots, yes.id, no.id).map_err(|_| {
                        format_err!(
                            "Error while counting ballots for argument: {}",
                            argument.external_id
                        )
                    })?;
                let n_participants =
                    participants_of_debate(db, permissions, &debate_about_argument)?.len();
                let consider = (n_yes as f64) / (n_participants as f64) > 0.5;
                Ok(consider)
            } else {
                Ok(true)
            }
        })
        .collect::<Result<Vec<bool>, Error>>()?
        .into_iter()
        .all(|x| x);
    Ok(consider)
}

#[derive(Debug, juniper::GraphQLInputObject)]
pub struct ArgumentWeightInput {
    pub argument_id: Uuid,
    pub infinite: bool,
    pub regular_weight: Option<i32>,
}

impl TryFrom<&ArgumentWeightInput> for ArgumentWeight {
    type Error = ();

    fn try_from(input: &ArgumentWeightInput) -> Result<ArgumentWeight, Self::Error> {
        if input.infinite {
            if input.regular_weight.is_none() {
                Ok(ArgumentWeight::Infinite)
            } else {
                Err(())
            }
        } else {
            match input.regular_weight {
                Some(weight @ 0..=5) => Ok(ArgumentWeight::Regular(weight)),
                _ => Err(()),
            }
        }
    }
}

pub fn save_argument_weights(
    db: &dyn Db,
    auth_context: &AuthContext,
    argument_weights: &[ArgumentWeightInput],
) -> Result<Vec<(Argument, ArgumentWeight)>, Error> {
    argument_weights
        .iter()
        .map(|input| {
            let argument = db.get_argument_with_external_id(input.argument_id)?;
            let argument_weight =
                ArgumentWeight::try_from(input).map_err(|_| -> Error {CreationError::InvalidData.into()})?;
            if auth_context.may_access_argument(db, &argument)? {
                let model_weight =
                    match db.get_argument_weight(auth_context.account().id, argument.id) {
                        Err(QueryError::NotFound) => {
                            db.insert_argument_weight(models::argument_weights::NewArgumentWeight {
                                account: auth_context.account().id,
                                argument: argument.id,
                                weight: argument_weight.into_db_i32(),
                            })
                        }
                        Ok(model_weight) => {
                            db.update_argument_weight(&models::argument_weights::ArgumentWeight {
                                weight: argument_weight.into_db_i32(),
                                ..model_weight
                            })
                        }
                        Err(e) => Err(e),
                    }?;
                let model_weight_id = model_weight.id;
                let argument_weight =
                    ArgumentWeight::try_from(model_weight).map_err(|_| format_err!("Could not create argument weight from data in DB. Argument weight’s DB id: {}", model_weight_id))?;
                Ok((argument, argument_weight))
            } else {
                Err(CreationError::Unauthorized.into())
            }
        })
        .collect::<Result<_, _>>()
}

#[derive(Debug, GraphQLInputObject)]
pub struct ArgumentInput {
    pub heading: String,
    pub body: String,
    pub debate_id: Uuid,
    pub supported_answers_ids: Vec<Uuid>,
}

pub fn add_argument(
    db: &dyn Db,
    auth_context: &AuthContext,
    argument: &ArgumentInput,
) -> Result<WithAuthContext<Argument>, Error> {
    let debate = db.get_debate_with_external_id(argument.debate_id)?;
    if auth_context.may_access_debate(db, &debate)?
        && [DebateState::Debating, DebateState::Combined].contains(&implicit_debate_state(
            db,
            &debate,
            Utc::now(),
        )?)
    {
        let new_argument = NewArgument {
            external_id: Uuid::new_v4(),
            heading: argument.heading.clone(),
            body: argument.body.clone(),
            debate: debate.id,
            author: auth_context.account().id,
            creation_time: Utc::now(),
        };

        let inserted_argument = db.insert_argument(&new_argument)?;

        let supports = argument
            .supported_answers_ids
            .iter()
            .map(
                |supported_answer_external_id| -> Result<NewSupport, Error> {
                    let supported_answer = get_answer_with_external_id(
                        db,
                        auth_context,
                        *supported_answer_external_id,
                    )?;
                    if supported_answer.debate == debate.id {
                        Ok(NewSupport {
                            answer: supported_answer.id,
                            argument: inserted_argument.id,
                        })
                    } else {
                        Err(CreationError::InvalidData.into())
                    }
                },
            )
            .collect::<Result<Vec<_>, _>>()?;
        db.insert_supports(&supports)?;

        Ok(auth_context.with(inserted_argument))
    } else {
        Err(CreationError::Unauthorized.into())
    }
}

pub fn considered(db: &dyn Db, argument: &Argument, voter: &Account) -> Result<bool, Error> {
    match db.get_ballot_of_voter_of_debate(argument.debate, voter.id) {
        Err(QueryError::NotFound) => Ok(true),
        Ok(ballot) => match ballot.prepared_time {
            Some(prepared_time) => Ok(argument.creation_time <= prepared_time),
            None => Ok(true),
        },
        Err(e) => Err(e.into()),
    }
}
