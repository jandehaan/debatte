use failure::Error;

use crate::error::*;
use crate::extensions::SwapResult;
use crate::logic::authorization::*;
use crate::logic::debate::*;
use db_access::db::{Db, QueryError};

pub use db_access::models::accounts::{Account, OtherAccount};
pub use db_access::models::debates::Debate;

pub fn participates_in(
    db: &dyn Db,
    auth_context: &AuthContext,
    participant: &Account,
    debate: &Debate,
) -> Result<bool, Error> {
    if participates_in_no_auth(db, &auth_context.account(), debate)? {
        participates_in_no_auth(db, participant, debate)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn participants_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Vec<OtherAccount>, Error> {
    if permissions.may_access_debate(db, debate)? {
        let top_level_debate = get_top_level_debate_recursively(db, debate.id)?;
        let participants = db.get_participants_of_debate(top_level_debate.id)?;
        let participants = participants
            .into_iter()
            .map(|participant| participant.swap().unwrap_or_else(OtherAccount::from))
            .collect();
        Ok(participants)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

fn participates_in_no_auth(
    db: &dyn Db,
    participant: &Account,
    debate: &Debate,
) -> Result<bool, Error> {
    let debate_id = get_top_level_debate_recursively(db, debate.id)?.id;
    match db.get_participation(participant.id, debate_id) {
        Ok(_) => Ok(true),
        Err(QueryError::NotFound) => Ok(false),
        Err(e) => Err(e.into()),
    }
}
