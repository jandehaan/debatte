use failure::Error;

use crate::email::templates::{self, create_email};
use crate::email::EmailSender;
use crate::error::public::*;
use crate::logic::authorization::*;
use crate::logic::tokens::secret_token::*;
use db_access::db::*;
use db_access::models::verify_email_links::*;

pub use crate::logic::tokens::secret_token::SecretToken;
pub use db_access::models::accounts::OtherAccount;

pub fn create_verify_email_link(
    db: &dyn Db,
    auth_context: &AuthContext,
) -> Result<SecretToken, Error> {
    let token = SecretToken::generate();
    let token_secret_hash = hash_secret_token(&token);
    let new_verify_email_link = NewVerifyEmailLink {
        proof_token_hash: token_secret_hash,
        account_id: auth_context.account().id,
        issued_time: chrono::Utc::now(),
    };
    db.insert_verify_email_link(&new_verify_email_link)?;
    Ok(token)
}

pub fn verify_email(
    db: &dyn Db,
    token: &str,
) -> Result<OtherAccount, PublicError<VerifyEmailError>> {
    let token = SecretToken::try_from_str(token)
        .ok_or(PublicErrorKind::Public(VerifyEmailError::MalformedToken))?;
    let hash = hash_secret_token(&token);
    let matching_link = match db.get_verify_email_link_with_hash(&hash) {
        Ok(link) => link,
        Err(QueryError::NotFound) => {
            return Err(PublicErrorKind::Public(VerifyEmailError::InvalidToken).into())
        }
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    };
    let account = db
        .get_account_with_id(matching_link.account_id)
        .context(PublicErrorKind::Other)?
        .map_err(|_| format_err!("Trying to verify email of deleted account"))
        .context(PublicErrorKind::Other)?;
    let account = db
        .update_account(Account {
            email_verified: true,
            ..account
        })
        .context(PublicErrorKind::Other)?;
    db.delete_verify_email_link(matching_link.id)
        .context(PublicErrorKind::Other)?;
    Ok(account.into())
}

pub fn resend_verification_email<'a>(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    auth_context: &'a AuthContext,
) -> Result<&'a str, PublicError<ResendEmailError>> {
    let existing_links = db
        .get_verify_email_links_of_account(auth_context.account().id)
        .context(PublicErrorKind::Other)?;
    if existing_links.len() > 3 {
        Err(PublicErrorKind::Public(ResendEmailError::RateLimited).into())
    } else if auth_context.account().temporary {
        Err(PublicErrorKind::Public(ResendEmailError::InvalidEmailOrPassword).into())
    } else {
        let verify_email_token =
            create_verify_email_link(db, auth_context).context(PublicErrorKind::Other)?;
        let email = create_email(&templates::verify::EmailInput {
            locale: auth_context.account().locale.into(),
            account: auth_context.account(),
            verification_link: &format!(
                "{}/verify-email/{}",
                email_sender.get_link_server(),
                verify_email_token.to_str()
            ),
        });
        email_sender.send(email).context(PublicErrorKind::Other)?;
        Ok(&auth_context.account().email)
    }
}
