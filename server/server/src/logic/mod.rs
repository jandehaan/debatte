/// # Guidelines to make sure that there are no authorization bugs
///
/// Every function should only use a single method of the [`Db`](db_access::db::Db) trait.
/// Restricting what the function does this much ensures that every database call is proceeded by the required authorization checks.
/// Of course, other functions from this module can be called.
pub mod account;
pub mod answer;
pub mod argument;
pub mod authorization;
pub mod ballot;
pub mod debate;
pub mod graphviz;
pub mod invitation;
pub mod invite_link;
pub mod login_link;
pub mod participation;
pub mod public_invite_link;
pub mod tokens;
pub mod verify_email_link;
pub mod vote;
