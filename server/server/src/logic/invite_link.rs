use chrono::Utc;
use uuid::Uuid;

use crate::email::templates::{self, create_email};
use crate::email::EmailSender;
use crate::error::public::*;
use crate::extensions::SwapResult;
use crate::logic::account::*;
use crate::logic::tokens::auth_token::*;
use crate::logic::tokens::secret_token::*;
use db_access::models::invite_links::*;
use db_access::models::participations::*;
use db_access::models::types::Locale;
use db_access::{db::*, models::types::DebateState};

use super::debate::implicit_debate_state;

#[derive(Debug, GraphQLObject)]
pub struct InviteeInfo {
    pub debate_id: Uuid,
    pub auth_token: String,
}

pub fn use_invite_link(
    db: &dyn Db,
    token: &str,
    name: String,
    locale: Locale,
) -> Result<InviteeInfo, PublicError<UseInviteLinkError>> {
    let token = match SecretToken::try_from_str(token) {
        Some(token) => Ok(token),
        None => Err(PublicErrorKind::Public(UseInviteLinkError::MalformedToken)),
    }?;
    let hash = hash_secret_token(&token);
    match db.get_invite_link_with_hash(&hash) {
        Ok(link) => {
            let debate = db
                .get_debate_with_id(link.debate)
                .context(PublicErrorKind::Other)?;
            if implicit_debate_state(db, &debate, Utc::now()).context(PublicErrorKind::Other)?
                != DebateState::Waiting
            {
                return Err(PublicErrorKind::Public(UseInviteLinkError::InvalidToken).into());
            }
            let account =
                create_quick_account(db, link.email.clone(), name, locale).map_err(|e| {
                    e.map_inner(|e| match e {
                        AccountCreationError::CannotSendEmail => {
                            Some(UseInviteLinkError::CannotSendEmail)
                        }
                        AccountCreationError::EmailInUse => Some(UseInviteLinkError::EmailInUse),
                        AccountCreationError::InvalidEmail => Some(UseInviteLinkError::EmailInUse),
                        AccountCreationError::MissingName => Some(UseInviteLinkError::MissingName),
                        AccountCreationError::WeakPassword => None,
                    })
                })?;
            let auth_token = login_special(db, &link.email)
                .context(PublicErrorKind::Other)?
                .to_str();
            let participation = NewParticipation {
                participant: account.id,
                debate: debate.id,
            };
            db.insert_participations(&[participation])
                .context(PublicErrorKind::Other)?;
            db.delete_invite_link(link.id)
                .context(PublicErrorKind::Other)?;
            Ok(InviteeInfo {
                debate_id: debate.external_id,
                auth_token,
            })
        }
        Err(QueryError::NotFound) => {
            Err(PublicErrorKind::Public(UseInviteLinkError::InvalidToken).into())
        }
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    }
}

pub fn change_invite_link_email(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    token: &str,
    new_email: String,
    locale: Locale,
) -> Result<String, PublicError<ChangeInviteLinkEmailError>> {
    let token = match SecretToken::try_from_str(token) {
        Some(token) => token,
        None => {
            return Err(PublicErrorKind::Public(ChangeInviteLinkEmailError::MalformedToken).into())
        }
    };
    let hash = hash_secret_token(&token);
    match db.get_invite_link_with_hash(&hash) {
        Ok(old_link) => {
            db.delete_invite_link(old_link.id)
                .context(PublicErrorKind::Other)?;
            let new_token = SecretToken::generate();
            let new_invite_link = db
                .insert_invite_link(NewInviteLink {
                    debate: old_link.debate,
                    email: new_email,
                    name: old_link.name,
                    proof_token_hash: hash_secret_token(&new_token),
                    invited_time: old_link.invited_time,
                    invited_by: old_link.invited_by,
                })
                .context(PublicErrorKind::Other)?;
            let debate = db
                .get_debate_with_id(new_invite_link.debate)
                .context(PublicErrorKind::Other)?;
            let inviting_user_name = new_invite_link
                .invited_by
                .map(|id| {
                    db.get_account_with_id(id)
                        .map(|account| account.swap().unwrap_or_else(OtherAccount::from).name)
                })
                .transpose()
                .context(PublicErrorKind::Other)?
                .unwrap_or_else(|| {
                    match locale {
                        Locale::EnUS => "Someone",
                        Locale::DeDE => "Jemand",
                    }
                    .to_owned()
                });
            email_sender
                .send(create_email(&templates::invitation::EmailInput {
                    locale: locale.into(),
                    invitee_name: &new_invite_link.name,
                    invitee_email: &new_invite_link.email,
                    debate: &debate,
                    inviting_user_name: &inviting_user_name,
                    invite_link: &format!(
                        "{}/quickstart/{}?email={}&name={}",
                        email_sender.get_link_server(),
                        new_token.to_str(),
                        &new_invite_link.email,
                        &new_invite_link.email
                    ),
                }))
                .map_err(|_| {
                    PublicErrorKind::Public(ChangeInviteLinkEmailError::CannotSendEmail)
                })?;
            Ok(new_invite_link.email)
        }
        Err(QueryError::NotFound) => {
            Err(PublicErrorKind::Public(ChangeInviteLinkEmailError::InvalidToken).into())
        }
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    }
}
