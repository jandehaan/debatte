use chrono::Utc;
use failure::Error;
use uuid::Uuid;

use crate::error::public::*;
use crate::logic::debate::*;
use crate::logic::tokens::auth_token::*;
use db_access::db::*;
use db_access::models::types::Locale;

pub use db_access::models::accounts::Account;
pub use db_access::models::accounts::OtherAccount;
pub use db_access::models::answers::Answer;
pub use db_access::models::arguments::Argument;
pub use db_access::models::debates::Debate;

pub trait Permissions {
    fn may_access_account(&self, db: &dyn Db, account: &Account) -> Result<bool, Error>;
    fn may_access_debate(&self, db: &dyn Db, debate: &Debate) -> Result<bool, Error>;
    fn may_access_argument(&self, db: &dyn Db, argument: &Argument) -> Result<bool, Error>;
    fn may_access_answer(&self, db: &dyn Db, answer: &Answer) -> Result<bool, Error>;
    fn may_access_other_account(
        &self,
        db: &dyn Db,
        other_account: &OtherAccount,
    ) -> Result<bool, Error>;
}

#[derive(Debug)]
pub struct AuthContext {
    account: std::rc::Rc<Account>,
}

impl AuthContext {
    pub fn from_token(
        db: &dyn Db,
        token_str: &str,
    ) -> Result<AuthContext, PublicError<TokenAuthError>> {
        let token = OpaqueAuthToken::try_from_str(token_str)
            .ok_or(PublicErrorKind::Public(TokenAuthError::MalformedToken))?;
        let account = authenticate_with_token(db, &token).context(PublicErrorKind::Other)?;
        Ok(AuthContext {
            account: std::rc::Rc::new(account),
        })
    }

    pub fn new(account: Account) -> AuthContext {
        AuthContext {
            account: std::rc::Rc::new(account),
        }
    }

    pub fn no_account() -> AuthContext {
        AuthContext::new(Account {
            id: -1,
            external_id: Uuid::new_v4(),
            email: String::new(),
            salt: None,
            password_hash: None,
            name: String::new(),
            email_verified: false,
            locale: Locale::EnUS,
            created: Utc::now(),
            temporary: true,
        })
    }

    pub fn account(&self) -> &Account {
        &self.account
    }

    pub fn with<T>(&self, x: T) -> WithAuthContext<T> {
        WithAuthContext {
            inner: x,
            auth_context: AuthContext {
                account: self.account.clone(),
            },
        }
    }
}

impl Permissions for AuthContext {
    fn may_access_account(&self, _: &dyn Db, account: &Account) -> Result<bool, Error> {
        Ok(self.account.id == account.id)
    }

    fn may_access_debate(&self, db: &dyn Db, debate: &Debate) -> Result<bool, Error> {
        let top_level_debate = get_top_level_debate_recursively(db, debate.id)
            .map_err(|_| format_err!("Error while retrieving top-level debate"))?;
        let participants = db.get_participants_of_debate(top_level_debate.id)?;
        Ok(participants
            .into_iter()
            .filter(|r| r.is_ok())
            .map(|r| r.unwrap())
            .any(|participant| participant.id == self.account.id))
    }

    fn may_access_argument(&self, db: &dyn Db, argument: &Argument) -> Result<bool, Error> {
        let debate = db.get_debate_of_argument(argument.id)?;
        self.may_access_debate(db, &debate)
    }

    fn may_access_answer(&self, db: &dyn Db, answer: &Answer) -> Result<bool, Error> {
        let debate = db.get_debate_with_id(answer.debate)?;
        self.may_access_debate(db, &debate)
    }

    fn may_access_other_account(
        &self,
        db: &dyn Db,
        other_account: &OtherAccount,
    ) -> Result<bool, Error> {
        let account = db.get_account_with_external_id(&other_account.external_id)?;
        match account {
            Ok(account) => {
                let known_accounts = db.get_accounts_of_all_debates(account.id)?;
                Ok(known_accounts
                    .iter()
                    .any(|account| account.external_id == self.account.external_id)
                    || account.id == -1)
            }
            Err(_) => Ok(false),
        }
    }
}

pub struct BackgroundTaskPermissions(());

impl Default for BackgroundTaskPermissions {
    fn default() -> Self {
        BackgroundTaskPermissions::new()
    }
}

impl BackgroundTaskPermissions {
    pub fn new() -> BackgroundTaskPermissions {
        BackgroundTaskPermissions(())
    }
}

impl Permissions for BackgroundTaskPermissions {
    fn may_access_account(&self, _: &dyn Db, _: &Account) -> Result<bool, Error> {
        Ok(true)
    }

    fn may_access_debate(&self, _: &dyn Db, _: &Debate) -> Result<bool, Error> {
        Ok(true)
    }

    fn may_access_argument(&self, _: &dyn Db, _: &Argument) -> Result<bool, Error> {
        Ok(true)
    }

    fn may_access_answer(&self, _: &dyn Db, _: &Answer) -> Result<bool, Error> {
        Ok(true)
    }

    fn may_access_other_account(&self, _: &dyn Db, _: &OtherAccount) -> Result<bool, Error> {
        Ok(true)
    }
}

#[derive(Debug)]
pub struct WithAuthContext<T> {
    pub inner: T,
    pub auth_context: AuthContext,
}

#[derive(Debug, Fail)]
pub enum AccessCheckError {
    #[fail(display = "not found")]
    NotFound,
    #[fail(display = "{}", _0)]
    Other(String),
}

impl From<QueryError> for AccessCheckError {
    fn from(e: QueryError) -> AccessCheckError {
        match e {
            QueryError::NotFound => AccessCheckError::NotFound,
            e => AccessCheckError::Other(format!("{:?}", e)),
        }
    }
}
