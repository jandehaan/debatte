use chrono::Utc;
use failure::Error;
use uuid::Uuid;

use crate::email::templates::*;
use crate::email::*;
use crate::error::*;
use crate::extensions::SwapResult;
use crate::logic::authorization::*;
use crate::logic::participation::*;
use crate::logic::tokens::secret_token::*;
use db_access::db::*;
use db_access::models::invitations::*;
use db_access::models::invite_links::*;
use db_access::models::participations::*;

pub use db_access::models::invitations::Invitation;

pub fn invite_with_email(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    invitee: &EmailInvitee,
    invited_by: &Account,
    debate: &Debate,
) -> Result<(), Error> {
    match db.get_account_with_email(&invitee.email) {
        Ok(invitee_account) => {
            invite_account(db, email_sender, &invitee_account, invited_by, debate)
        }
        Err(QueryError::NotFound) => {
            let invite_link_token = SecretToken::generate();
            db.insert_invite_link(NewInviteLink {
                debate: debate.id,
                email: invitee.email.clone(),
                name: invitee.name.clone(),
                proof_token_hash: hash_secret_token(&invite_link_token),
                invited_time: Utc::now(),
                invited_by: Some(invited_by.id),
            })?;
            email_sender.send(create_email(&templates::invitation::EmailInput {
                locale: debate.locale.into(),
                invitee_name: &invitee.name,
                invitee_email: &invitee.email,
                debate: &debate,
                inviting_user_name: &invited_by.name,
                invite_link: &format!(
                    "{}/quickstart/{}?email={}&name={}",
                    email_sender.get_link_server(),
                    invite_link_token.to_str(),
                    &invitee.email,
                    &invitee.name
                ),
            }))?;
            Ok(())
        }
        Err(e) => Err(e.into()),
    }
}

pub fn invite_account(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    invitee: &Account,
    invited_by: &Account,
    debate: &Debate,
) -> Result<(), Error> {
    db.insert_invitation(NewInvitation {
        external_id: Uuid::new_v4(),
        invitee: invitee.id,
        debate: debate.id,
        invited_by: invited_by.id,
    })?;
    if !invitee.temporary {
        email_sender.send(create_email(
            &templates::invitation_for_account::EmailInput {
                locale: invitee.locale.into(),
                account: invitee,
                debate,
                link: &format!("{}/profile", email_sender.get_link_server()),
                inviting_user_name: &invited_by.name,
            },
        ))?;
    }
    Ok(())
}

pub fn get_invitations_of_invitee(
    db: &dyn Db,
    auth_context: &AuthContext,
) -> Result<Vec<WithAuthContext<Invitation>>, Error> {
    let invitations = db.get_invitations_of_invitee(auth_context.account().id)?;
    Ok(invitations
        .into_iter()
        .map(|invitation| auth_context.with(invitation))
        .collect())
}

pub fn get_debate_id(db: &dyn Db, invitation: &Invitation) -> Result<Uuid, Error> {
    let debate = db.get_debate_with_id(invitation.debate)?;
    Ok(debate.external_id)
}

pub fn get_debate_question(db: &dyn Db, invitation: &Invitation) -> Result<String, Error> {
    let debate = db.get_debate_with_id(invitation.debate)?;
    Ok(debate.question)
}

pub fn get_debate_details(db: &dyn Db, invitation: &Invitation) -> Result<String, Error> {
    let debate = db.get_debate_with_id(invitation.debate)?;
    match debate.details {
        Some(details) => Ok(details),
        None => Err(format_err!(
            "Invitation to non top-level debate: {:?}",
            invitation
        )),
    }
}

pub fn get_invited_by(db: &dyn Db, invitation: &Invitation) -> Result<OtherAccount, Error> {
    let invited_by = db
        .get_account_with_id(invitation.invited_by)?
        .swap()
        .unwrap_or_else(OtherAccount::from);
    Ok(invited_by)
}

pub fn decline_invitation(db: &dyn Db, auth_context: &AuthContext, id: Uuid) -> Result<(), Error> {
    let invitation = db.get_invitation_with_external_id(id)?;
    if auth_context.account().id == invitation.invitee {
        db.delete_invitation(invitation.id)?;
        Ok(())
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn accept_invitation(db: &dyn Db, auth_context: &AuthContext, id: Uuid) -> Result<(), Error> {
    let invitation = db.get_invitation_with_external_id(id)?;
    let debate = db.get_debate_with_id(invitation.id)?;
    let invitee = db
        .get_account_with_id(invitation.invitee)?
        .map_err(|_| RetrievalErrorKind::NotFound)?;
    if auth_context.account().id == invitation.invitee
        && !participates_in(db, auth_context, &invitee, &debate)?
    {
        let participation = NewParticipation {
            participant: invitation.invitee,
            debate: invitation.debate,
        };
        db.insert_participations(&[participation])?;
        db.delete_invitation(invitation.id)?;
        Ok(())
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}
