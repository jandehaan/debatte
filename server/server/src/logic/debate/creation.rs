use chrono::*;
use chrono_tz;
use failure::Error;
use std::str::FromStr;
use uuid::Uuid;

use crate::email::{self, EmailSender};
use crate::error::*;
use crate::graphql::query::enums::*;
use crate::logic::authorization::*;
use crate::logic::debate::implicit_debate_state;
use crate::logic::invitation::*;
use crate::logic::tokens::secret_token::*;
use db_access::db::*;
use db_access::models::answers::*;
use db_access::models::debates::*;
use db_access::models::participations::*;
use db_access::models::types::*;

#[derive(Debug, GraphQLInputObject)]
pub struct TopLevelDebateInput {
    pub question: String,
    pub details: String,
    pub timetable: DebateTimetable,
    pub known_invitees: Vec<OtherAccountInput>,
    pub email_invitees: Vec<email::templates::EmailInvitee>,
    pub first_answers: Vec<FirstAnswerInput>,
    pub locale: GQLLocale,
    pub time_zone: String,
    pub create_public_invite_link: bool,
}

#[derive(Debug, GraphQLInputObject)]
pub struct SpecialSubDebateInput {
    pub kind: GQLDebateKind,
    pub concerned_argument_id: Uuid,
}

#[derive(Debug, GraphQLInputObject)]
pub struct OtherAccountInput {
    pub id: Uuid,
}

#[derive(Debug, GraphQLInputObject)]
pub struct FirstAnswerInput {
    pub answer: String,
}

#[derive(Debug, GraphQLInputObject)]
pub struct DebateTimetable {
    pub collecting_answers_start: DateTime<Utc>,
    pub debating_start: DateTime<Utc>,
    pub voting_start: DateTime<Utc>,
    pub voting_end: DateTime<Utc>,
}

pub fn create_top_level_debate(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    auth_context: &AuthContext,
    debate: TopLevelDebateInput,
) -> Result<Debate, Error> {
    if chrono_tz::Tz::from_str(&debate.time_zone).is_err() {
        return Err(CreationError::InvalidData.into());
    }
    if !(debate.timetable.collecting_answers_start >= Utc::now()
        && debate.timetable.debating_start >= debate.timetable.collecting_answers_start
        && debate.timetable.voting_start >= debate.timetable.debating_start
        && debate.timetable.voting_end >= debate.timetable.voting_start)
    {
        return Err(CreationError::InvalidData.into());
    }
    let new_debate = NewDebate {
        external_id: Uuid::new_v4(),
        question: debate.question,
        details: Some(debate.details),
        allow_adding_more_answers: true,
        collecting_answers_start: Some(debate.timetable.collecting_answers_start),
        debating_start: Some(debate.timetable.debating_start),
        voting_start: Some(debate.timetable.voting_start),
        voting_end: Some(debate.timetable.voting_end),
        concerned_argument: None,
        kind: DebateKind::Other,
        creation_time: Utc::now(),
        locale: debate.locale.into(),
        time_zone: debate.time_zone,
        public_invite_link_token: if debate.create_public_invite_link {
            Some(SecretToken::generate().to_str())
        } else {
            None
        },
    };

    let inserted_debate = db.insert_debate(&new_debate)?;

    let author_participation = NewParticipation {
        debate: inserted_debate.id,
        participant: auth_context.account().id,
    };
    db.insert_participations(&[author_participation])?;

    for known_invitee in debate.known_invitees {
        let known_invitee = db
            .get_account_with_external_id(&known_invitee.id)?
            .map_err(|_| format_err!("Trying to invite deleted account"))?;
        invite_account(
            db,
            email_sender,
            &known_invitee,
            auth_context.account(),
            &inserted_debate,
        )?;
    }

    if !auth_context.account().temporary {
        for email_invitee in debate.email_invitees {
            invite_with_email(
                db,
                email_sender,
                &email_invitee,
                auth_context.account(),
                &inserted_debate,
            )
            .ok();
        }
    }

    let answers = new_answers_from_input(
        debate.first_answers,
        inserted_debate.id,
        auth_context.account().id,
    );
    db.insert_answers(&answers)?;

    Ok(inserted_debate)
}

pub fn create_special_sub_debate(
    db: &dyn Db,
    auth_context: &AuthContext,
    debate: SpecialSubDebateInput,
) -> Result<Debate, Error> {
    let concerned_argument = db.get_argument_with_external_id(debate.concerned_argument_id)?;
    let concerned_debate = db.get_debate_of_argument(concerned_argument.id)?;
    if !auth_context.may_access_debate(db, &concerned_debate)?
        || implicit_debate_state(db, &concerned_debate, Utc::now())? != DebateState::Debating
    {
        return Err(CreationError::Unauthorized.into());
    }

    let question = match debate.kind.into() {
        DebateKind::MeritOfArgument => Ok(match concerned_debate.locale {
            Locale::EnUS => "Does this argument have merit?",
            Locale::DeDE => "Ist dieses Argument an sich sachlich richtig?",
        }
        .to_owned()),
        DebateKind::RelevanceOfArgument => Ok(match concerned_debate.locale {
            Locale::EnUS => "Is this argument relevant?",
            Locale::DeDE => "Ist dieses Argument relevant?",
        }
        .to_owned()),
        DebateKind::Other => Err(CreationError::InvalidData),
    }?;

    let new_debate = NewDebate {
        external_id: Uuid::new_v4(),
        question,
        details: None,
        allow_adding_more_answers: false,
        collecting_answers_start: None,
        debating_start: None,
        voting_start: None,
        voting_end: None,
        concerned_argument: Some(concerned_argument.id),
        kind: debate.kind.into(),
        creation_time: Utc::now(),
        locale: concerned_debate.locale,
        time_zone: concerned_debate.time_zone,
        public_invite_link_token: None,
    };

    let inserted_debate = db.insert_debate(&new_debate)?;

    let answers = vec![
        NewAnswer {
            external_id: Uuid::new_v4(),
            answer: match concerned_debate.locale {
                Locale::EnUS => "Yes",
                Locale::DeDE => "Ja",
            }
            .to_owned(),
            debate: inserted_debate.id,
            author: auth_context.account().id,
            creation_time: Utc::now(),
            semantics: AnswerSemantics::Yes,
        },
        NewAnswer {
            external_id: Uuid::new_v4(),
            answer: match concerned_debate.locale {
                Locale::EnUS => "No",
                Locale::DeDE => "Nein",
            }
            .to_owned(),
            debate: inserted_debate.id,
            author: auth_context.account().id,
            creation_time: Utc::now(),
            semantics: AnswerSemantics::No,
        },
    ];

    db.insert_answers(&answers)?;

    Ok(inserted_debate)
}

fn new_answers_from_input(
    input: Vec<FirstAnswerInput>,
    debate_id: i64,
    author_id: i64,
) -> Vec<NewAnswer> {
    input
        .into_iter()
        .map(|first_answer_input| NewAnswer {
            external_id: Uuid::new_v4(),
            answer: first_answer_input.answer,
            debate: debate_id,
            author: author_id,
            creation_time: Utc::now(),
            semantics: AnswerSemantics::None,
        })
        .collect::<Vec<NewAnswer>>()
}
