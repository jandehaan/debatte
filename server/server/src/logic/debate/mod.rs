pub mod creation;
pub mod summary;

use chrono::{DateTime, Utc};
use failure::Error;
use uuid::Uuid;

use crate::error::*;
use crate::logic::answer::get_answers_of_debate;
use crate::logic::argument::get_arguments_of_debate;
use crate::logic::authorization::*;
use db_access::db::Db;
use db_access::models::debates::*;

pub use db_access::models::accounts::Account;
pub use db_access::models::arguments::Argument;
pub use db_access::models::debates::Debate;
pub use db_access::models::types::{DebateKind, DebateState};

pub fn get_debate(db: &dyn Db, permissions: &impl Permissions, id: Uuid) -> Result<Debate, Error> {
    let debate = db.get_debate_with_external_id(id)?;

    if permissions.may_access_debate(db, &debate)? {
        Ok(debate)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_top_level_debates_of_account(
    db: &dyn Db,
    auth_context: &AuthContext,
    account: &Account,
) -> Result<Vec<WithAuthContext<Debate>>, Error> {
    if auth_context.may_access_account(db, account)? {
        let debates = db.get_top_level_debates_of_participant(account.id)?;
        let debates_auth = debates
            .into_iter()
            .map(|debate| auth_context.with(debate))
            .collect();
        Ok(debates_auth)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_debate_of_argument(
    db: &dyn Db,
    auth_context: &AuthContext,
    argument: &Argument,
) -> Result<WithAuthContext<Debate>, Error> {
    let debate = db.get_debate_of_argument(argument.id)?;
    if auth_context.may_access_debate(db, &debate)? {
        Ok(auth_context.with(debate))
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_debates_about_argument(
    db: &dyn Db,
    permissions: &impl Permissions,
    argument: &Argument,
) -> Result<Vec<Debate>, Error> {
    if permissions.may_access_argument(db, argument)? {
        let debates = db.get_debates_about_argument(argument.id)?;
        Ok(debates)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_sub_debates_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Vec<Debate>, Error> {
    if permissions.may_access_debate(db, debate)? {
        let sub_debates = db.get_sub_debates_of_debate(debate.id)?;
        Ok(sub_debates)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_debate_details(
    db: &dyn Db,
    auth_context: &AuthContext,
    debate: &Debate,
) -> Result<String, Error> {
    if auth_context.may_access_debate(db, debate)? {
        match &debate.details {
            Some(details) => Ok(details.to_owned()),
            None => {
                let concerned_argument = db.get_argument_with_id(
                    debate.concerned_argument.ok_or_else(|| format_err!("A debate whose details are None should concern an argument (debate ID: {})", debate.external_id))?,
                )?;
                let mut details = format!("> # {}\n>\n", concerned_argument.heading);
                details.push_str(
                    &concerned_argument
                        .body
                        .lines()
                        .map(|line| "> ".to_owned() + line + "\n")
                        .collect::<Vec<_>>()
                        .concat(),
                );
                Ok(details)
            }
        }
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_last_participation_time(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<DateTime<Utc>, Error> {
    if permissions.may_access_debate(db, debate)? {
        let last_answer_created = get_answers_of_debate(db, permissions, debate)?
            .iter()
            .map(|answer| answer.creation_time)
            .max();
        let last_argument_created = get_arguments_of_debate(db, permissions, debate)?
            .iter()
            .map(|argument| argument.creation_time)
            .max();
        let last_sub_debate_participation_time =
            get_sub_debates_of_debate(db, permissions, debate)?
                .iter()
                .map(|debate| get_last_participation_time(db, permissions, debate))
                .collect::<Result<Vec<_>, _>>()?
                .into_iter()
                .max();
        let debate_created = debate.creation_time;
        // There will always be at least one element, the debate creation time, so this unwrap() will never panic.
        Ok(vec![
            last_answer_created,
            last_argument_created,
            last_sub_debate_participation_time,
            Some(debate_created),
        ]
        .into_iter()
        .flatten()
        .max()
        .unwrap())
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_top_level_debate_recursively(
    db: &dyn Db,
    debate_id: i64,
) -> Result<TopLevelDebate, Error> {
    let debate = db.get_debate_with_id(debate_id)?;
    let debate_id = debate.external_id;
    match debate.concerned_argument {
        Some(concerned_argument_id) => {
            let concerned_argument = db.get_argument_with_id(concerned_argument_id)?;
            get_top_level_debate_recursively(db, concerned_argument.debate)
        }
        None => TopLevelDebate::try_from(debate).map_err(|_| format_err!("Unable to convert debate without parent (i.e. without a concerned argument) into a top-level debate (debate ID: {})", debate_id)),
    }
}

pub fn top_level_debate_state(
    db: &dyn Db,
    debate: &Debate,
    time: DateTime<Utc>,
) -> Result<DebateState, Error> {
    let top_level_debate = get_top_level_debate_recursively(db, debate.id)?;
    implicit_debate_state(db, &Debate::from(top_level_debate), time)
}

pub fn implicit_debate_state(
    db: &dyn Db,
    debate: &Debate,
    time: DateTime<Utc>,
) -> Result<DebateState, Error> {
    let top_level_debate = get_top_level_debate_recursively(db, debate.id)?;
    if debate.concerned_argument.is_some() {
        let debate_id = top_level_debate.external_id;
        match implicit_debate_state(db, &Debate::from(top_level_debate), time)? {
            DebateState::Waiting => Ok(DebateState::Waiting),
            DebateState::CollectingAnswers => Ok(DebateState::Waiting),
            DebateState::Debating => Ok(DebateState::Combined),
            DebateState::Voting => Ok(DebateState::Finished),
            DebateState::Finished => Ok(DebateState::Finished),
            DebateState::Combined => Err(format_err!(
                "This top-level debate’s state is DebateState::Combined: {}",
                debate_id
            )),
        }
    } else if top_level_debate.voting_end < time {
        Ok(DebateState::Finished)
    } else if top_level_debate.voting_start < time {
        Ok(DebateState::Voting)
    } else if top_level_debate.debating_start < time {
        Ok(DebateState::Debating)
    } else if top_level_debate.collecting_answers_start < time {
        Ok(DebateState::CollectingAnswers)
    } else {
        Ok(DebateState::Waiting)
    }
}
