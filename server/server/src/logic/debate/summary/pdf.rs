use crate::logic::debate::summary::latex::*;
use crate::logic::debate::summary::*;

pub fn summarized_debate_to_pdf(debate: &SummarizedDebate) -> Result<Vec<u8>, Error> {
    let latex = summarized_debate_to_latex(debate)?;
    let pdf = tectonic::latex_to_pdf(&latex).map_err(|_| {
        eprintln!("{}", &latex);
        format_err!("Error compiling LaTeX")
    })?;
    Ok(pdf)
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_latex_env() {
        let latex = r#"\documentclass[a4paper,nobib]{tufte-handout}

\usepackage{fontspec}
\usepackage[english]{babel}
\usepackage{microtype}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{tikz}
\usepackage{longtable}

\setmainfont[Path=../res/fonts/,UprightFont=*-Regular,BoldFont=*-Bold,ItalicFont=*-Italic]{SourceSansPro}
\setmonofont[Path=../res/fonts/,UprightFont=*-Regular,BoldFont=*-Bold,ItalicFont=*-Italic,Scale=MatchLowercase]{SourceCodePro}

\begin{document}
\includegraphics{../res/logo-small-print-bw.pdf}
Test
\end{document}"#.to_owned();
        println!("{}", std::env::current_dir().unwrap().to_str().unwrap());
        tectonic::latex_to_pdf(latex).expect("Error compiling LaTeX");
    }
}
