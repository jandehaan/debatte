use askama::Template;
use chrono::{DateTime, TimeZone, Utc};
use chrono_tz;
use serde::{Deserialize, Serialize};
use std::cmp::{Ord, Ordering, PartialOrd};
use std::collections::HashMap;
use std::hash::Hash;
use std::str::FromStr;
use uuid::Uuid;

use crate::error::public::*;
use crate::error::*;
use crate::locale::Locale;
use crate::locale::*;
use crate::logic::authorization::Permissions;
use crate::logic::debate::*;
use crate::logic::*;
use db_access::db::*;
use db_access::models::types::DebateKind;

pub mod json;
pub mod latex;
pub mod pdf;

pub fn summarize_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    external_id: Uuid,
) -> Result<SummarizedDebate, Error> {
    let debate = debate::get_debate(db, permissions, external_id)?;
    let answers = answer::get_answers_of_debate(db, permissions, &debate)?
        .into_iter()
        .map(|answer| -> Result<_, Error> {
            let author = account::get_other_account(db, permissions, answer.author)?;
            Ok(SummarizedAnswer {
                creation_time: answer.creation_time,
                answer: answer.answer,
                author: author.name,
            })
        })
        .collect::<Result<Vec<_>, _>>()?;
    let arguments = argument::get_arguments_of_debate(db, permissions, &debate)?
        .into_iter()
        .map(|argument| get_summarized_argument(db, permissions, &argument))
        .collect::<Result<Vec<_>, _>>()?;
    let participants = participation::participants_of_debate(db, permissions, &debate)?
        .into_iter()
        .map(|other_account| other_account.name)
        .collect();
    let raw_ballots = ballot::get_ballots_of_debate(db, permissions, &debate)?
        .ok_or(RetrievalErrorKind::Other)?;
    let voting_results: Result<Option<SummarizedVotingResults>, Error> =
        match vote::ranked_pairs::count_ballots(&raw_ballots) {
            Ok((raw_ranking, raw_audit_info)) => {
                let ids_to_displayed_ids = raw_ranking
                    .iter()
                    .map(|a| a.answer_id)
                    .zip(1..)
                    .collect::<HashMap<i64, i64>>();
                let ranking = raw_ranking
                    .into_iter()
                    .map(|ranked_answer| -> Result<_, Error> {
                        Ok(
                            answer::get_answer_of_ranked_answer(db, permissions, &ranked_answer)?
                                .answer,
                        )
                    })
                    .collect::<Result<Vec<_>, Error>>()?;
                let mut display_graph = raw_audit_info.complete_graph;
                for i in display_graph.node_indices() {
                    let id = display_graph[i];
                    let new_id = ids_to_displayed_ids[&id];
                    display_graph[i] = new_id;
                }
                let mut ballots = raw_ballots
                    .into_iter()
                    .map(|raw_ballot| {
                        let mut ranking = raw_ballot.answer_ranking;
                        ranking.sort_by_key(|answer_ranking| answer_ranking.rank);
                        let ranking = ranking
                            .into_iter()
                            .map(|ranked_answer| {
                                *ids_to_displayed_ids.get(&ranked_answer.answer_id).unwrap()
                            })
                            .collect();
                        let voter = match db.get_account_with_id(raw_ballot.voter_id)? {
                            Ok(a) => a.name,
                            Err(a) => a.name,
                        };
                        Ok(SummarizedBallot { ranking, voter })
                    })
                    .collect::<Result<Vec<_>, Error>>()?;
                ballots.sort_unstable();
                let ballots = ballots;
                let pair_contest_margins = raw_audit_info
                    .pair_contest_margins
                    .iter()
                    .map(|((x, y), margin)| {
                        (
                            (
                                *ids_to_displayed_ids.get(x).unwrap(),
                                *ids_to_displayed_ids.get(y).unwrap(),
                            ),
                            *margin,
                        )
                    })
                    .collect::<HashMap<_, _>>();
                let graphviz = format!(
                    "{:?}",
                    petgraph::dot::Dot::with_config(
                        &display_graph,
                        &[petgraph::dot::Config::EdgeNoLabel]
                    )
                );
                let tikz = graphviz::to_tikz(&graphviz)
                    .map_err(|_| format_err!("Could not run `dot` or `dot2tex`."))?;
                let audit_info = SummarizedAuditInfo {
                    ballots,
                    pair_contest_margins,
                    graphviz,
                    tikz,
                    ranks: (1..(answers.len() + 1) as i64).collect(),
                };
                Ok(Some(SummarizedVotingResults {
                    ranking,
                    audit_info,
                }))
            }
            Err(_) => Ok(None),
        };
    let voting_results = voting_results?;
    let details = debate
        .details
        .ok_or_else(|| format_err!("Not a top-level debate"))?;
    Ok(SummarizedDebate {
        creation_time: debate.creation_time,
        question: debate.question,
        details,
        collecting_answers_start: debate
            .collecting_answers_start
            .ok_or_else(|| format_err!("Not a top-level debate"))?,
        debating_start: debate
            .debating_start
            .ok_or_else(|| format_err!("Not a top-level debate"))?,
        voting_start: debate
            .voting_start
            .ok_or_else(|| format_err!("Not a top-level debate"))?,
        voting_end: debate
            .voting_end
            .ok_or_else(|| format_err!("Not a top-level debate"))?,
        locale: Locale::from(debate.locale),
        time_zone: chrono_tz::Tz::from_str(&debate.time_zone)
            .map_err(|_| format_err!("Invalid timezone in database"))?,
        answer_ids: (1..i64::try_from(answers.len() + 1).unwrap()).collect(),
        answers,
        arguments,
        participants,
        voting_results,
    })
}

#[derive(Debug, juniper::GraphQLObject)]
pub struct SummaryLinks {
    pdf: String,
    json: String,
    authenticate: String,
}

pub fn get_summary_links(
    db: &dyn Db,
    api_host: &str,
    debate: &Debate,
) -> Result<SummaryLinks, PublicError<GetSummaryLinksError>> {
    if implicit_debate_state(db, debate, Utc::now()).map_err(|_| PublicErrorKind::Other)?
        != DebateState::Finished
    {
        return Err(PublicErrorKind::Public(GetSummaryLinksError::DebateInProgress).into());
    }
    match db.get_debate_summary_of_debate(debate.id) {
        Err(QueryError::NotFound) => {
            Err(PublicErrorKind::Public(GetSummaryLinksError::NotReadyYet).into())
        }
        Err(_) => Err(PublicErrorKind::Other.into()),
        Ok(_) => Ok(SummaryLinks {
            pdf: format!("{}/summary/pdf?debate={}", api_host, debate.external_id),
            json: format!("{}/summary/json?debate={}", api_host, debate.external_id),
            authenticate: format!("{}/set-cookie", api_host),
        }),
    }
}

#[derive(Serialize, Deserialize, Template)]
#[template(path = "debate_summary.tex", syntax = "tex_syntax")]
pub struct SummarizedDebate {
    pub creation_time: DateTime<Utc>,
    pub question: String,
    pub details: String,
    pub collecting_answers_start: DateTime<Utc>,
    pub debating_start: DateTime<Utc>,
    pub voting_start: DateTime<Utc>,
    pub voting_end: DateTime<Utc>,
    pub locale: Locale,
    pub time_zone: chrono_tz::Tz,
    pub answers: Vec<SummarizedAnswer>,
    pub arguments: Vec<SummarizedArgument>,
    pub voting_results: Option<SummarizedVotingResults>,
    pub participants: Vec<String>,
    #[serde(skip)]
    pub answer_ids: Vec<i64>,
}

mod filters {
    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};

    use super::*;

    pub use crate::markdown::markdown_to_latex;

    pub fn hash<T: Hash>(x: T) -> Result<String, askama::Error> {
        let mut hasher = DefaultHasher::new();
        x.hash(&mut hasher);
        Ok(base64::encode_config(
            &hasher.finish().to_be_bytes(),
            base64::URL_SAFE,
        ))
    }

    pub fn sub_debates(
        arguments: &[SummarizedArgument],
    ) -> Result<Vec<&SummarizedSubDebate>, askama::Error> {
        Ok(arguments
            .iter()
            .flat_map(|argument| list_all_sub_debates(argument))
            .map(|(_, sub_debate)| sub_debate)
            .collect())
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn date_and_time(
        date_time: &DateTime<Utc>,
        locale: &Locale,
        time_zone: &chrono_tz::Tz,
    ) -> Result<String, askama::Error> {
        let format = match locale {
            Locale::EnUS => "%B %-d, %-Y, %H:%M",
            Locale::DeDE => "%d.%m.%Y, %H:%M",
        };
        Ok(format!(
            "{}",
            time_zone
                .from_utc_datetime(&date_time.naive_utc())
                .format(format)
        ))
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn just_date(date_time: &DateTime<Utc>, locale: &Locale) -> Result<String, askama::Error> {
        let format = match locale {
            Locale::EnUS => "%B %-d, %-Y",
            Locale::DeDE => "%d.%m.%Y",
        };
        Ok(format!("{}", date_time.date().format(format)))
    }
}

#[derive(Serialize, Deserialize)]
pub struct SummarizedAnswer {
    pub creation_time: DateTime<Utc>,
    pub answer: String,
    pub author: String,
}

#[derive(Serialize, Deserialize, Hash)]
pub struct SummarizedArgument {
    pub creation_time: DateTime<Utc>,
    pub heading: String,
    pub body: String,
    pub supported_answers: Vec<String>,
    pub author: String,
    pub debates_about_relevance: Vec<SummarizedSubDebate>,
    pub debates_about_merit: Vec<SummarizedSubDebate>,
}

#[derive(Serialize, Deserialize, Hash)]
pub struct SummarizedSubDebate {
    pub creation_time: DateTime<Utc>,
    pub argument_heading: String,
    pub arguments: Vec<SummarizedArgument>,
    pub voting_results: YesNoVotingResults,
    pub kind: DebateKind,
}

#[derive(Serialize, Deserialize, Hash)]
pub struct YesNoVotingResults {
    pub yes: bool,
    pub number_of_yes_votes: usize,
    pub number_of_no_votes: usize,
}

#[derive(Serialize, Deserialize)]
pub struct SummarizedAuditInfo {
    pub ballots: Vec<SummarizedBallot>,
    #[serde(skip)]
    pub pair_contest_margins: HashMap<(i64, i64), i64>,
    pub graphviz: String,
    pub tikz: String,
    #[serde(skip)]
    pub ranks: Vec<i64>,
}

impl SummarizedAuditInfo {
    // Required because askama for-loops always borrow values.
    #[allow(clippy::trivially_copy_pass_by_ref)]
    fn get_margin(&self, i: &i64, j: &&i64) -> Option<i64> {
        self.pair_contest_margins.get(&(*i, **j)).copied()
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq)]
pub struct SummarizedBallot {
    pub ranking: Vec<i64>,
    pub voter: String,
}

impl Ord for SummarizedBallot {
    fn cmp(&self, other: &SummarizedBallot) -> Ordering {
        self.ranking
            .iter()
            .zip(other.ranking.iter())
            .map(|(x, y)| x.cmp(y))
            .fold(std::cmp::Ordering::Equal, |acc, elem| match (acc, elem) {
                (Ordering::Less, _) => Ordering::Less,
                (Ordering::Equal, Ordering::Greater) => Ordering::Greater,
                (Ordering::Equal, Ordering::Less) => Ordering::Less,
                (Ordering::Equal, _) => Ordering::Equal,
                (Ordering::Greater, _) => Ordering::Greater,
            })
    }
}

impl PartialOrd for SummarizedBallot {
    fn partial_cmp(&self, other: &SummarizedBallot) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    pub fn test_ballot_cmp() {
        assert_eq!(
            SummarizedBallot {
                ranking: vec![1, 2, 3],
                voter: String::new(),
            }
            .cmp(&SummarizedBallot {
                ranking: vec![1, 2, 3],
                voter: String::new(),
            }),
            Ordering::Equal
        );
        assert_eq!(
            SummarizedBallot {
                ranking: vec![2, 2, 3],
                voter: String::new(),
            }
            .cmp(&SummarizedBallot {
                ranking: vec![1, 2, 3],
                voter: String::new(),
            }),
            Ordering::Greater
        );
        assert_eq!(
            SummarizedBallot {
                ranking: vec![2, 2, 3],
                voter: String::new(),
            }
            .cmp(&SummarizedBallot {
                ranking: vec![1, 3, 3],
                voter: String::new(),
            }),
            Ordering::Greater
        );
        assert_eq!(
            SummarizedBallot {
                ranking: vec![0, 200, 3],
                voter: String::new(),
            }
            .cmp(&SummarizedBallot {
                ranking: vec![1, 2, 3],
                voter: String::new(),
            }),
            Ordering::Less
        );
    }
}

impl SummarizedBallot {
    // Required because askama for-loops always borrow values.
    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn get_by_rank(&self, rank: &i64) -> Option<i64> {
        self.ranking.get((*rank - 1) as usize).copied()
    }
}

#[derive(Serialize, Deserialize)]
pub struct SummarizedVotingResults {
    pub ranking: Vec<String>,
    pub audit_info: SummarizedAuditInfo,
}

fn get_summarized_argument(
    db: &dyn Db,
    permissions: &impl Permissions,
    argument: &argument::Argument,
) -> Result<SummarizedArgument, Error> {
    let author = account::get_other_account(db, permissions, argument.author)?;
    let sub_debates = debate::get_debates_about_argument(db, permissions, &argument)?
        .into_iter()
        .map(|debate| -> Result<_, Error> {
            let arguments = argument::get_arguments_of_debate(db, permissions, &debate)?
                .into_iter()
                .map(|argument| get_summarized_argument(db, permissions, &argument))
                .collect::<Result<Vec<_>, _>>()?;
            let ballots = ballot::get_ballots_of_debate(db, permissions, &debate)?
                .ok_or(RetrievalErrorKind::Other)?;
            let answer::YesNoAnswers { yes, no } =
                answer::get_yes_no_answers_of_debate(db, permissions, &debate)?;
            let (number_of_yes_votes, number_of_no_votes) =
                vote::two_choices::count_ballots(ballots, yes.id, no.id)
                    .map_err(|_| format_err!("Error while counting ballots"))?;
            let number_of_participants =
                participation::participants_of_debate(db, permissions, &debate)?.len();
            Ok(SummarizedSubDebate {
                creation_time: debate.creation_time,
                argument_heading: argument.heading.clone(),
                arguments,
                voting_results: YesNoVotingResults {
                    number_of_yes_votes,
                    number_of_no_votes,
                    yes: (number_of_yes_votes as f64) / (number_of_participants as f64) > 0.5,
                },
                kind: debate.kind,
            })
        })
        .collect::<Result<Vec<_>, _>>()?;
    let (debates_about_relevance, debates_about_merit) = sub_debates
        .into_iter()
        .partition::<Vec<_>, _>(|debate| debate.kind == DebateKind::RelevanceOfArgument);
    let supported_answers = argument::get_supported_answers(db, permissions, &argument)?
        .into_iter()
        .map(|answer| answer.answer)
        .collect();
    Ok(SummarizedArgument {
        creation_time: argument.creation_time,
        heading: argument.heading.clone(),
        body: argument.body.clone(),
        author: author.name,
        supported_answers,
        debates_about_relevance,
        debates_about_merit,
    })
}

fn list_all_sub_debates<'a>(
    argument: &'a SummarizedArgument,
) -> Vec<(i64, &'a SummarizedSubDebate)> {
    let mut all_sub_debates = Vec::new();
    all_sub_debates.append(
        &mut argument
            .debates_about_relevance
            .iter()
            .map(|debate| (0, debate))
            .collect(),
    );
    all_sub_debates.append(
        &mut argument
            .debates_about_merit
            .iter()
            .map(|debate| (0, debate))
            .collect(),
    );
    all_sub_debates.append(
        &mut argument
            .debates_about_relevance
            .iter()
            .flat_map(|sub_debate| {
                sub_debate.arguments.iter().flat_map(|argument| {
                    list_all_sub_debates(argument)
                        .into_iter()
                        .map(|(level, sub_debate)| (level + 1, sub_debate))
                })
            })
            .collect(),
    );
    all_sub_debates.append(
        &mut argument
            .debates_about_merit
            .iter()
            .flat_map(|sub_debate| {
                sub_debate.arguments.iter().flat_map(|argument| {
                    list_all_sub_debates(argument)
                        .into_iter()
                        .map(|(level, sub_debate)| (level + 1, sub_debate))
                })
            })
            .collect(),
    );
    all_sub_debates
}
