use crate::logic::debate::summary::*;

pub fn summarized_debate_to_json(debate: &SummarizedDebate) -> Result<String, Error> {
    serde_json::to_string(debate)
        .map_err(|_| format_err!("Unexpected error while serializing to JSON"))
}
