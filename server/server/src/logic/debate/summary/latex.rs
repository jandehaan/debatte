use crate::logic::debate::summary::*;

pub fn summarized_debate_to_latex(debate: &SummarizedDebate) -> Result<String, Error> {
    let latex = debate.render()?;
    Ok(latex)
}

#[cfg(test)]
pub mod tests {
    use super::*;

    pub fn get_summarized_test_debate() -> SummarizedDebate {
        SummarizedDebate {
            creation_time: Utc::now(),
            question: "Test Question?".to_owned(),
            details: "These are *lot* of details".to_owned(),
            collecting_answers_start: Utc::now(),
            debating_start: Utc::now(),
            voting_start: Utc::now(),
            voting_end: Utc::now(),
            locale: Locale::EnUS,
            time_zone: chrono_tz::Tz::Europe__Berlin,
            answer_ids: vec![1, 2],
            answers: vec![
                SummarizedAnswer {
                    creation_time: Utc::now(),
                    answer: "Answer 1".to_owned(),
                    author: "Harry Potter".to_owned(),
                },
                SummarizedAnswer {
                    creation_time: Utc::now(),
                    answer: "Answer 2".to_owned(),
                    author: "Hermione Granger".to_owned(),
                },
            ],
            arguments: vec![
                SummarizedArgument {
                    creation_time: Utc::now(),
                    heading: "An Argument".to_owned(),
                    body: "Body of an Argument".to_owned(),
                    supported_answers: vec!["Answer 1".to_owned()],
                    author: "Harry Potter".to_owned(),
                    debates_about_relevance: vec![SummarizedSubDebate {
                        creation_time: Utc::now(),
                        argument_heading: "An Argument".to_owned(),
                        arguments: Vec::new(),
                        voting_results: YesNoVotingResults {
                            yes: false,
                            number_of_yes_votes: 0,
                            number_of_no_votes: 1,
                        },
                        kind: DebateKind::MeritOfArgument,
                    }],
                    debates_about_merit: vec![SummarizedSubDebate {
                        creation_time: Utc::now(),
                        argument_heading: "An Argument".to_owned(),
                        arguments: Vec::new(),
                        voting_results: YesNoVotingResults {
                            yes: false,
                            number_of_yes_votes: 0,
                            number_of_no_votes: 1,
                        },
                        kind: DebateKind::RelevanceOfArgument,
                    }],
                },
                SummarizedArgument {
                    creation_time: Utc::now(),
                    heading: "Another Argument".to_owned(),
                    body: "Another body…".to_owned(),
                    supported_answers: vec!["Answer 2".to_owned()],
                    author: "Hermione Granger".to_owned(),
                    debates_about_relevance: Vec::new(),
                    debates_about_merit: Vec::new(),
                },
            ],
            voting_results: Some(SummarizedVotingResults {
                ranking: vec!["Answer 1".to_owned(), "Answer 2".to_owned()],
                audit_info: SummarizedAuditInfo {
                    ballots: vec![SummarizedBallot {
                        ranking: vec![1, 2],
                        voter: "Hermione Granger".to_owned(),
                    }],
                    pair_contest_margins: vec![((1, 1), 0), ((1, 2), 1), ((2, 1), -1), ((2, 2), 0)]
                        .into_iter()
                        .collect(),
                    graphviz: "digraph { \"1\" -> \"2\" }".to_owned(),
                    tikz: "\\begin{tikzpicture}[>=latex,line join=bevel,]
\\pgfsetlinewidth{1bp}
\\pgfsetcolor{black}
\\end{tikzpicture}"
                        .to_owned(),
                    ranks: vec![1, 2],
                },
            }),
            participants: vec!["Harry Potter".to_owned(), "Hermione Granger".to_owned()],
        }
    }
    #[test]
    fn test_to_latex() {
        let debate = get_summarized_test_debate();

        summarized_debate_to_latex(&debate).expect("Debate not converted to LaTeX");
    }
}
