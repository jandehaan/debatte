use chrono::Utc;
use failure::Error;
use uuid::Uuid;

use crate::email::templates::{self, create_email};
use crate::email::EmailSender;
use crate::error::public::*;
use crate::error::*;
use crate::extensions::SwapResult;
use crate::logic::authorization::*;
use crate::logic::tokens::random_bytes;
use crate::logic::tokens::secret_token::*;
use crate::logic::verify_email_link::*;
use db_access::db::{Db, QueryError, QueryResult};
use db_access::models::accounts::*;
use db_access::models::password_resets::*;
use db_access::models::types::Locale;

pub use db_access::models::accounts::{Account, OtherAccount};

pub fn create_regular_account(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    email: String,
    name: String,
    password: String,
    locale: Locale,
) -> Result<Account, PublicError<AccountCreationError>> {
    if !is_email_valid(&email) {
        return Err(PublicErrorKind::Public(AccountCreationError::InvalidEmail).into());
    }
    if is_email_in_use(db, &email).context(PublicErrorKind::Other)? {
        return Err(PublicErrorKind::Public(AccountCreationError::EmailInUse).into());
    }
    if !is_password_strong(&password) {
        return Err(PublicErrorKind::Public(AccountCreationError::WeakPassword).into());
    }
    if name.is_empty() {
        return Err(PublicErrorKind::Public(AccountCreationError::MissingName).into());
    }

    let salt = generate_salt();
    let account = NewAccount {
        external_id: Uuid::new_v4(),
        email,
        password_hash: Some(hash_password(&password, &salt)),
        salt: Some(salt),
        name,
        email_verified: false,
        locale,
        created: Utc::now(),
        temporary: false,
    };
    let account = db.insert_account(account).context(PublicErrorKind::Other)?;
    let auth_context = AuthContext::new(account.clone());

    let verify_email_token =
        create_verify_email_link(db, &auth_context).context(PublicErrorKind::Other)?;

    let email = create_email(&templates::verify::EmailInput {
        locale: locale.into(),
        account: &account,
        verification_link: &format!(
            "{}/verify-email/{}",
            email_sender.get_link_server(),
            verify_email_token.to_str()
        ),
    });
    email_sender
        .send(email)
        .map_err(|_| PublicErrorKind::Public(AccountCreationError::CannotSendEmail))?;

    Ok(account)
}

pub fn create_quick_account(
    db: &dyn Db,
    email: String,
    name: String,
    locale: Locale,
) -> Result<Account, PublicError<AccountCreationError>> {
    if !is_email_valid(&email) {
        return Err(PublicErrorKind::Public(AccountCreationError::InvalidEmail).into());
    }
    if is_email_in_use(db, &email).context(PublicErrorKind::Other)? {
        return Err(PublicErrorKind::Public(AccountCreationError::EmailInUse).into());
    }
    if name.is_empty() {
        return Err(PublicErrorKind::Public(AccountCreationError::MissingName).into());
    }

    let account = NewAccount {
        external_id: Uuid::new_v4(),
        email,
        salt: None,
        password_hash: None,
        name,
        email_verified: true,
        locale,
        created: Utc::now(),
        temporary: false,
    };
    let account = db.insert_account(account).context(PublicErrorKind::Other)?;
    Ok(account)
}

pub fn send_reset_password_email(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    email: String,
    locale: Locale,
) -> Result<String, PublicError<SendResetPasswordEmailError>> {
    match db.get_account_with_email(&email) {
        QueryResult::Err(QueryError::NotFound) => {
            Err(PublicErrorKind::Public(SendResetPasswordEmailError::AccountDoesNotExist).into())
        }
        QueryResult::Ok(account) => {
            if account.temporary {
                return Err(PublicErrorKind::Public(
                    SendResetPasswordEmailError::AccountDoesNotExist,
                )
                .into());
            }
            let secret_token = SecretToken::generate();
            let password_reset = NewPasswordReset {
                proof_token_hash: hash_secret_token(&secret_token),
                account_id: account.id,
                started_time: Utc::now(),
            };
            db.insert_password_reset(password_reset)
                .context(PublicErrorKind::Other)?;
            Ok(email_sender
                .send(create_email(&templates::reset_password::EmailInput {
                    locale: locale.into(),
                    account: &account,
                    reset_password_link: &format!(
                        "{}/reset-password/{}",
                        email_sender.get_link_server(),
                        secret_token.to_str()
                    ),
                }))
                .map(|_| email.to_owned())
                .context(PublicErrorKind::Other)?)
        }
        QueryResult::Err(e) => Err(e).context(PublicErrorKind::Other)?,
    }
}

pub fn change_password(
    db: &dyn Db,
    token: &str,
    new_password: &str,
) -> Result<String, PublicError<ChangePasswordError>> {
    let token = match SecretToken::try_from_str(token) {
        Some(token) => token,
        None => return Err(PublicErrorKind::Public(ChangePasswordError::MalformedToken).into()),
    };
    let hash = hash_secret_token(&token);
    let password_reset = match db.get_password_reset_with_hash(&hash) {
        Err(QueryError::NotFound) => {
            return Err(PublicErrorKind::Public(ChangePasswordError::InvalidToken).into())
        }
        Ok(p) => p,
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    };
    if password_reset.started_time >= Utc::now() - chrono::Duration::hours(1) {
        if !is_password_strong(new_password) {
            return Err(PublicErrorKind::Public(ChangePasswordError::WeakPassword).into());
        }
        let account = db
            .get_account_with_id(password_reset.account_id)
            .context(PublicErrorKind::Other)?
            .map_err(|_| format_err!("Trying to change password of deleted account"))
            .context(PublicErrorKind::Other)?;
        db.delete_password_reset(password_reset.id)
            .context(PublicErrorKind::Other)?;
        let salt = account.salt.clone().unwrap_or_else(generate_salt);
        db.update_account(Account {
            password_hash: Some(hash_password(new_password, &salt)),
            ..account
        })
        .context(PublicErrorKind::Other)?;
        Ok("success".to_owned())
    } else {
        Err(PublicErrorKind::Public(ChangePasswordError::ExpiredToken).into())
    }
}

pub fn get_account(
    db: &dyn Db,
    permissions: &impl Permissions,
    external_id: &Uuid,
) -> Result<Account, Error> {
    let account = db
        .get_account_with_external_id(external_id)?
        .map_err(|_| RetrievalErrorKind::NotFound)?;
    if permissions.may_access_account(db, &account)? {
        Ok(account)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_other_account(
    db: &dyn Db,
    permissions: &impl Permissions,
    id: i64,
) -> Result<OtherAccount, Error> {
    let other_account: OtherAccount = db
        .get_account_with_id(id)?
        .swap()
        .unwrap_or_else(OtherAccount::from);
    if permissions.may_access_other_account(db, &other_account)? {
        Ok(other_account)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_known_accounts(
    db: &dyn Db,
    auth_context: &AuthContext,
) -> Result<Vec<OtherAccount>, Error> {
    let known_accounts = db.get_accounts_of_all_debates(auth_context.account().id)?;
    let known_accounts_without_self = known_accounts
        .into_iter()
        .filter(|account| account.external_id != auth_context.account().external_id)
        .collect();
    Ok(known_accounts_without_self)
}

pub fn change_name_of_account(
    db: &dyn Db,
    auth_context: &AuthContext,
    name: String,
) -> Result<Account, Error> {
    let updated_account = Account {
        name,
        ..auth_context.account().clone()
    };
    let account = db.update_account(updated_account)?;
    Ok(account)
}

pub fn has_password(account: &Account) -> bool {
    account.password_hash.is_some()
}

pub fn set_password(
    db: &dyn Db,
    auth_context: &AuthContext,
    password: &str,
) -> Result<(), PublicError<SetPasswordError>> {
    if !is_password_strong(password) {
        return Err(PublicErrorKind::Public(SetPasswordError::WeakPassword).into());
    }
    let account = auth_context.account();
    if account.password_hash.is_none() {
        let salt = generate_salt();
        let updated_account = Account {
            password_hash: Some(hash_password(password, &salt)),
            salt: Some(salt),
            ..account.clone()
        };
        db.update_account(updated_account)
            .context(PublicErrorKind::Other)?;
        Ok(())
    } else {
        Err(PublicErrorKind::Public(SetPasswordError::AccountHasPassword).into())
    }
}

pub fn mark_as_deleted(
    db: &dyn Db,
    permissions: &impl Permissions,
    account: &Account,
    password: &str,
) -> Result<(), Error> {
    if !permissions.may_access_account(db, account)? {
        return Err(RetrievalErrorKind::Unauthorized.into());
    }
    let salt = account
        .salt
        .as_ref()
        .ok_or(RetrievalErrorKind::Unauthorized)?;
    let password_hash = account
        .password_hash
        .as_ref()
        .ok_or(RetrievalErrorKind::Unauthorized)?;
    if !verify_password(password_hash, salt, password) {
        return Err(RetrievalErrorKind::Unauthorized.into());
    }
    db.mark_account_as_deleted(account.id)?;
    db.delete_auth_tokens_of_account(account.id)?;
    Ok(())
}

pub fn change_locale(
    db: &dyn Db,
    permissions: &impl Permissions,
    account: &Account,
    new_locale: Locale,
) -> Result<(), Error> {
    if !permissions.may_access_account(db, account)? {
        return Err(RetrievalErrorKind::Unauthorized.into());
    }
    db.update_account(Account {
        locale: new_locale,
        ..account.clone()
    })?;
    Ok(())
}

pub fn create_temporary_account(db: &dyn Db, locale: Locale) -> Result<Account, Error> {
    let uuid = Uuid::new_v4();
    let account = db.insert_account(NewAccount {
        created: Utc::now(),
        email: format!("{}@eltrot.jandehaan.name", uuid),
        email_verified: false,
        external_id: uuid,
        locale,
        name: format!("Temp {}", uuid),
        password_hash: None,
        salt: None,
        temporary: true,
    })?;
    Ok(account)
}

fn is_email_in_use(db: &dyn Db, email: &str) -> QueryResult<bool> {
    match db.get_account_with_email(email) {
        Err(QueryError::NotFound) => Ok(false),
        Err(e) => Err(e),
        Ok(_) => Ok(true),
    }
}

fn hash_password(password: &str, salt: &str) -> String {
    base64::encode(&argon2rs::argon2i_simple(password, salt))
}

fn generate_salt() -> String {
    base64::encode(&random_bytes(8))
}

fn is_email_valid(email: &str) -> bool {
    email.contains('@')
}

fn is_password_strong(password: &str) -> bool {
    password.len() >= 12
}

pub fn verify_password(password_hash: &str, salt: &str, password: &str) -> bool {
    hash_password(password, salt) == password_hash
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::email::MockEmailSender;
    use db_access::db::mock::MockDb;
    use db_access::models::types::Locale;

    #[test]
    fn test_create_regular_account() {
        let db = MockDb::new();
        let email_sender = MockEmailSender;
        assert!(create_regular_account(
            &db,
            &email_sender,
            "test@example.com".to_owned(),
            "Someone with a pretty long name".to_owned(),
            "longerpassword".to_owned(),
            Locale::EnUS
        )
        .is_ok());
        assert_eq!(
            create_regular_account(
                &db,
                &email_sender,
                "test@example.com".to_owned(),
                "Someone with a pretty long name".to_owned(),
                "longerpassword".to_owned(),
                Locale::EnUS
            )
            .unwrap_err()
            .kind(),
            PublicErrorKind::Public(AccountCreationError::EmailInUse)
        );
    }

    #[test]
    fn test_create_quick_account() {
        let db = MockDb::new();
        assert!(create_quick_account(
            &db,
            "test@example.com".to_owned(),
            "Someone with a pretty long name".to_owned(),
            Locale::EnUS
        )
        .is_ok());
        assert_eq!(
            create_quick_account(
                &db,
                "test@example.com".to_owned(),
                "Someone with a pretty long name".to_owned(),
                Locale::EnUS
            )
            .unwrap_err()
            .kind(),
            PublicErrorKind::Public(AccountCreationError::EmailInUse)
        );
    }

    #[test]
    fn test_generate_salt() {
        assert_ne!(generate_salt(), generate_salt());
        assert!(generate_salt().len() > 8);
    }

    #[test]
    fn test_is_email_valid() {
        assert!(is_email_valid("someone@gmail.com"));
        assert!(is_email_valid("anyone@com"));
        assert!(!is_email_valid("not an email address"));
        assert!(!is_email_valid("gmail.com"))
    }

    #[test]
    fn test_is_password_strong() {
        assert!(is_password_strong("ß0böx&s11ab/"));
        assert!(!is_password_strong("password"));
        assert!(!is_password_strong("short"));
        assert!(is_password_strong("not_many_chars_but_very_long"));
        assert!(is_password_strong("minimal_pwd_"))
    }

    #[test]
    fn test_verify_password() {
        assert!(verify_password(
            "65bqDh278/w25/xaf3NZTqqJ0jCwHeQP2pL7MnxcmQE=",
            "salt....",
            "password"
        ));
        assert!(!verify_password(
            "55bqDh278/w25/xaf3NZTqqJ0jCwHeQP2pL7MnxcmQE=",
            "salt....",
            "password"
        ));
        assert!(!verify_password(
            "65bqDh278/w25/xaf3NZTqqJ0jCwHeQP2pL7MnxcmQE=",
            "salt....2",
            "password"
        ));
        assert!(!verify_password(
            "65bqDh278/w25/xaf3NZTqqJ0jCwHeQP2pL7MnxcmQE=",
            "salt....",
            "password1"
        ));
    }
}
