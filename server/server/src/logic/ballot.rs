use chrono::Utc;
use failure::{Error, ResultExt};
use uuid::Uuid;

use crate::error::*;
use crate::logic::argument;
use crate::logic::authorization::*;
use crate::logic::debate::*;
use db_access::db::*;
use db_access::models;

pub use db_access::models::debates::Debate;

#[derive(Debug, Clone)]
pub struct Ballot {
    pub id: Uuid,
    pub answer_ranking: Vec<RankedAnswer>,
    pub debate_id: i64,
    pub voter_id: i64,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct RankedAnswer {
    pub rank: i32,
    pub answer_id: i64,
}

pub fn get_ballots_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Option<Vec<Ballot>>, RetrievalError> {
    if !permissions
        .may_access_debate(db, debate)
        .context(RetrievalErrorKind::Other)?
    {
        Err(RetrievalErrorKind::Other.into())
    } else if implicit_debate_state(db, debate, Utc::now()).context(RetrievalErrorKind::Other)?
        == DebateState::Voting
        || top_level_debate_state(db, debate, Utc::now()).context(RetrievalErrorKind::Other)?
            == DebateState::Debating
    {
        Ok(None)
    } else {
        let raw_ballots = db
            .get_ballots_of_debate(debate.id)
            .context(RetrievalErrorKind::Other)?;
        let ballots = raw_ballots
            .iter()
            .map(|raw_ballot| ballot_from_model_ballot(db, raw_ballot))
            .collect::<Result<_, _>>()
            .context(RetrievalErrorKind::Other)?;
        Ok(Some(ballots))
    }
}

#[derive(Debug)]
pub struct PreparedBallot(pub Ballot);

pub fn get_prepared_ballot_of_debate(
    db: &dyn Db,
    auth_context: &AuthContext,
    debate: &Debate,
) -> Result<Option<PreparedBallot>, Error> {
    if auth_context.may_access_debate(db, &debate)? {
        let raw_ballot =
            match db.get_ballot_of_voter_of_debate(debate.id, auth_context.account().id) {
                Ok(raw_ballot) => Ok(Some(raw_ballot)),
                Err(QueryError::NotFound) => Ok(None),
                Err(e) => Err(e),
            }?;
        raw_ballot
            .map(|raw_ballot| {
                let ballot = ballot_from_model_ballot(db, &raw_ballot)?;
                Ok(PreparedBallot(ballot))
            })
            .transpose()
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn new_arguments(
    db: &dyn Db,
    account: &Account,
    prepared_ballot: &PreparedBallot,
) -> Result<bool, Error> {
    Ok(db
        .get_arguments_of_debate(prepared_ballot.0.debate_id)?
        .iter()
        .map(|argument| argument::considered(db, argument, account).map(|x| !x))
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .any(|x| x))
}

fn ballot_from_model_ballot(
    db: &dyn Db,
    model_ballot: &models::ballots::Ballot,
) -> Result<Ballot, Error> {
    let partial_ballots = db.get_partial_ballots_of_ballot(model_ballot.id)?;
    let answer_ranking = answer_ranking_from_partial_ballots(&partial_ballots);
    Ok(Ballot {
        id: model_ballot.external_id,
        answer_ranking,
        debate_id: model_ballot.debate,
        voter_id: model_ballot.voter,
    })
}

fn answer_ranking_from_partial_ballots(
    partial_ballots: &[models::partial_ballots::PartialBallot],
) -> Vec<RankedAnswer> {
    partial_ballots
        .iter()
        .map(|partial_ballot| RankedAnswer {
            answer_id: partial_ballot.answer,
            rank: partial_ballot.rank,
        })
        .collect()
}
