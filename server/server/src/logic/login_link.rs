use chrono::Utc;

use crate::email::templates::{self, create_email};
use crate::email::EmailSender;
use crate::error::public::*;
use crate::logic::tokens::secret_token::*;
use db_access::db::*;
use db_access::models::login_links::*;

pub fn send_login_email(
    db: &dyn Db,
    email_sender: &dyn EmailSender,
    email: &str,
) -> Result<String, PublicError<SendLoginEmailError>> {
    let account = db
        .get_account_with_email(email)
        .context(PublicErrorKind::Other)?;
    if account.password_hash.is_none() {
        let secret_token = SecretToken::generate();
        let login_link = NewLoginLink {
            proof_token_hash: hash_secret_token(&secret_token),
            account_id: account.id,
            issued_time: Utc::now(),
        };
        db.insert_login_link(login_link)
            .context(PublicErrorKind::Other)?;
        Ok(email_sender
            .send(create_email(&templates::login::EmailInput {
                locale: account.locale.into(),
                email: &account.email,
                login_link: &format!(
                    "{}/login/{}",
                    email_sender.get_link_server(),
                    secret_token.to_str()
                ),
            }))
            .map(|_| email.to_owned())
            .context(PublicErrorKind::Other)?)
    } else {
        Err(PublicErrorKind::Public(SendLoginEmailError::AccountHasPassword).into())
    }
}
