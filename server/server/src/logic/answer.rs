use chrono::Utc;
use failure::Error;
use uuid::Uuid;

use crate::error::*;
use crate::logic::authorization::*;
use crate::logic::ballot::RankedAnswer;
use crate::logic::debate::*;
use db_access::db::Db;
use db_access::models::answers::*;

pub use db_access::models::answers::Answer;
pub use db_access::models::arguments::Argument;
pub use db_access::models::debates::Debate;

pub fn get_answers_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<Vec<Answer>, Error> {
    if permissions.may_access_debate(db, debate)? {
        let answers = db.get_answers_of_debate(debate.id)?;
        Ok(answers)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

#[derive(Debug)]
pub struct YesNoAnswers {
    pub yes: Answer,
    pub no: Answer,
}

pub fn get_yes_no_answers_of_debate(
    db: &dyn Db,
    permissions: &impl Permissions,
    debate: &Debate,
) -> Result<YesNoAnswers, Error> {
    if debate.kind == DebateKind::MeritOfArgument || debate.kind == DebateKind::RelevanceOfArgument
    {
        let answers = get_answers_of_debate(db, permissions, debate)?;
        if answers.len() == 2 {
            let answer1 = answers.get(0).unwrap().clone();
            let answer2 = answers.get(1).unwrap().clone();
            let (yes, no) = match answer1.semantics {
                AnswerSemantics::Yes => Ok((answer1, answer2)),
                AnswerSemantics::No => Ok((answer2, answer1)),
                AnswerSemantics::None => Err(format_err!("Answer does not have yes/no semantics, but the debate kind would require that: {}", debate.external_id)),
            }?;
            if yes.semantics != no.semantics {
                Ok(YesNoAnswers { yes, no })
            } else {
                Err(format_err!(
                    "Both answers of this debate are yes/no: {}",
                    debate.external_id
                ))
            }
        } else {
            Err(format_err!(
                "Debate has yes/no-kind but fewer or more than 2 answers: {}",
                debate.external_id
            ))
        }
    } else {
        Err(format_err!(
            "Tried to get yes/no answers of debate that is not of a suitable kind: {}",
            debate.external_id
        ))
    }
}

pub fn get_supporting_arguments(
    db: &dyn Db,
    auth_context: &AuthContext,
    answer: &Answer,
) -> Result<Vec<Argument>, Error> {
    if auth_context.may_access_answer(db, answer)? {
        let arguments = db.get_supporting_arguments_of_answer(answer.id)?;
        Ok(arguments)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_answer_of_ranked_answer(
    db: &dyn Db,
    permissions: &impl Permissions,
    ranked_answer: &RankedAnswer,
) -> Result<Answer, Error> {
    let answer = db.get_answer_with_id(ranked_answer.answer_id)?;
    if permissions.may_access_answer(db, &answer)? {
        Ok(answer)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

pub fn get_answer_with_external_id(
    db: &dyn Db,
    auth_context: &AuthContext,
    external_id: Uuid,
) -> Result<Answer, Error> {
    let answer = db.get_answer_with_external_id(external_id)?;
    if auth_context.may_access_answer(db, &answer)? {
        Ok(answer)
    } else {
        Err(RetrievalErrorKind::Unauthorized.into())
    }
}

#[derive(Debug, GraphQLInputObject)]
pub struct AnswerInput {
    pub answer: String,
    pub debate_id: Uuid,
}

pub fn add_answer_to_debate(
    db: &dyn Db,
    auth_context: &AuthContext,
    author: &Account,
    answer: AnswerInput,
) -> Result<WithAuthContext<Answer>, Error> {
    let debate = get_debate(db, auth_context, answer.debate_id)?;

    if debate.allow_adding_more_answers
        && implicit_debate_state(db, &debate, Utc::now())? == DebateState::CollectingAnswers
    {
        let answer = NewAnswer {
            external_id: Uuid::new_v4(),
            answer: answer.answer,
            debate: debate.id,
            author: author.id,
            creation_time: Utc::now(),
            semantics: AnswerSemantics::None,
        };
        let answer = db.insert_answers(&[answer])?.remove(0);
        Ok(auth_context.with(answer))
    } else {
        Err(CreationError::Unauthorized.into())
    }
}
