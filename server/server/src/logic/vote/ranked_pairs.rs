use petgraph;
use std::collections::hash_map::*;

use super::CountingError;
use crate::logic::ballot::*;

pub struct AuditInfo {
    pub ballots: Vec<Ballot>,
    pub pair_contest_margins: Vec<((i64, i64), i64)>,
    pub complete_graph: petgraph::graph::DiGraph<i64, ()>,
}

pub fn count_ballots(ballots: &[Ballot]) -> Result<(Vec<RankedAnswer>, AuditInfo), CountingError> {
    if ballots.is_empty() {
        return Err(CountingError::NoBallots);
    }
    let mut answers = ballots
        .first()
        .unwrap()
        .answer_ranking
        .iter()
        .map(|ranked_answer| ranked_answer.answer_id)
        .collect::<Vec<_>>();
    answers.sort();
    let answers = answers;
    if answers.is_empty() {
        return Err(CountingError::NoCandidates);
    }
    let pairs: Vec<(i64, i64)> = answers
        .iter()
        .flat_map(|answer1| answers.iter().map(move |answer2| (*answer1, *answer2)))
        .collect();
    let pair_contest_margins: Vec<((i64, i64), i64)> = pairs
        .iter()
        .map(|(a, b)| {
            (
                (*a, *b),
                ballots
                    .iter()
                    .map(|ballot| {
                        if a == b {
                            0
                        } else {
                            order(
                                a,
                                b,
                                &ballot
                                    .answer_ranking
                                    .iter()
                                    .map(|ranked| ranked.answer_id)
                                    .collect::<Vec<_>>(),
                            )
                            .unwrap_or(0)
                        }
                    })
                    .sum(),
            )
        })
        .collect();
    let mut pair_contest_margins_sorted = pair_contest_margins;
    pair_contest_margins_sorted.sort_by(|(_, contest_margin_a), (_, contest_margin_b)| {
        contest_margin_b.cmp(contest_margin_a)
    });
    let pair_contest_margins_sorted = pair_contest_margins_sorted;
    let mut graph = petgraph::graph::DiGraph::new();
    let mut node_indices = HashMap::new();
    for x in answers.into_iter() {
        let node_index = graph.add_node(x);
        node_indices.insert(x, node_index);
    }
    let node_indices = node_indices;
    let pair_contest_margins = pair_contest_margins_sorted.clone();
    for ((a, b), _) in pair_contest_margins_sorted {
        let a_index = node_indices.get(&a).unwrap();
        let b_index = node_indices.get(&b).unwrap();
        let edge = graph.add_edge(*a_index, *b_index, ());
        if petgraph::algo::is_cyclic_directed(&graph) {
            graph.remove_edge(edge);
        }
    }
    let complete_graph = graph.clone();
    let mut ranking = Vec::new();
    while let Some(current_node_index) = graph.node_indices().next() {
        let mut current_node_index = current_node_index;
        while let Some(x) = graph
            .neighbors_directed(current_node_index, petgraph::Direction::Incoming)
            .next()
        {
            current_node_index = x;
        }
        let current_node_index = current_node_index;
        ranking.push(*graph.node_weight(current_node_index).unwrap());
        graph.remove_node(current_node_index);
    }
    let ranked_answers = ranking
        .into_iter()
        .zip(0..)
        .map(|(answer, rank)| RankedAnswer {
            rank,
            answer_id: answer,
        })
        .collect::<Vec<_>>();
    Ok((
        ranked_answers,
        AuditInfo {
            ballots: Vec::from(ballots),
            pair_contest_margins,
            complete_graph,
        },
    ))
}

fn order<T: PartialEq>(a: &T, b: &T, v: &[T]) -> Option<i64> {
    if !(v.contains(&a) && v.contains(&b)) {
        return None;
    }
    for x in v {
        if *x == *a {
            return Some(1);
        } else if *x == *b {
            return Some(-1);
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_order() {
        assert_eq!(order(&1, &2, &[1, 2, 3]), Some(1));
        assert_eq!(order(&1, &2, &[3, 2, 1]), Some(-1));
        assert_eq!(order(&3, &5, &[10, 4, 3, 5, 5, 6, 5, 3]), Some(1));
        assert_eq!(order(&4, &5, &[1, 2, 3]), None);
        assert_eq!(order(&1, &4, &[1, 2, 3]), None);
    }

    #[test]
    fn test_count_ballots_single_winner() {
        let ballots = to_ballots(&[(vec![1, 2, 3], 1)]);
        let ranking = count_ballots(&ballots).map(|t| t.0.first().unwrap().answer_id);
        assert_eq!(Ok(1), ranking);

        assert_eq!(
            Err(CountingError::NoBallots),
            count_ballots(&Vec::new()).map(|t| t.0)
        );

        assert_eq!(
            Err(CountingError::NoCandidates),
            count_ballots(&to_ballots(&[(Vec::new(), 1)])).map(|t| t.0)
        );

        let ballots = to_ballots(&[
            (vec![0, 1, 2, 3], 42),
            (vec![1, 2, 3, 0], 26),
            (vec![2, 3, 1, 0], 15),
            (vec![3, 2, 1, 0], 17),
        ]);
        let winner = count_ballots(&ballots).map(|t| t.0.first().unwrap().answer_id);
        assert_eq!(winner, Ok(1));

        let ballots = to_ballots(&[
            (vec![0, 1, 2, 3, 4], 7),
            (vec![4, 3, 0, 1, 2], 3),
            (vec![3, 4, 1, 2, 0], 6),
            (vec![1, 2, 0, 4, 3], 3),
            (vec![4, 2, 0, 1, 3], 5),
            (vec![3, 2, 0, 1, 4], 3),
        ]);
        let winner = count_ballots(&ballots).map(|t| t.0.first().unwrap().answer_id);
        assert_eq!(winner, Ok(0));
    }

    #[test]
    fn test_count_ballots() {
        let ballots = to_ballots(&[(vec![1, 2, 3], 1)]);
        let ranking = count_ballots(&ballots).map(|t| t.0);
        assert_eq!(Ok(vec![1, 2, 3]), ranking_to_vec(ranking));

        let ballots = to_ballots(&[
            (vec![0, 1, 2, 3], 42),
            (vec![1, 2, 3, 0], 26),
            (vec![2, 3, 1, 0], 15),
            (vec![3, 2, 1, 0], 17),
        ]);
        let ranking = count_ballots(&ballots).map(|t| t.0);
        assert_eq!(Ok(vec![1, 2, 3, 0]), ranking_to_vec(ranking));
    }

    fn ranking_to_vec<E>(ranking: Result<Vec<RankedAnswer>, E>) -> Result<Vec<i64>, E> {
        ranking.map(|ranking| ranking.into_iter().map(|answer| answer.answer_id).collect())
    }

    fn to_ballots(raw: &[(Vec<i64>, usize)]) -> Vec<Ballot> {
        raw.iter()
            .map(|(answer_ranking, n)| {
                std::iter::repeat(Ballot {
                    id: uuid::Uuid::new_v4(),
                    debate_id: 0,
                    voter_id: -1,
                    answer_ranking: answer_ranking
                        .iter()
                        .zip(0..)
                        .map(|(answer, rank)| RankedAnswer {
                            rank,
                            answer_id: *answer,
                        })
                        .collect(),
                })
                .take(*n)
            })
            .flatten()
            .collect()
    }
}
