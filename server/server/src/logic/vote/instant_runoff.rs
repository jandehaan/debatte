use std::collections::hash_map::HashMap;

use super::CountingError;
use crate::logic::ballot::*;

pub fn count_ballots(ballots: Vec<Ballot>) -> Result<Vec<RankedAnswer>, CountingError> {
    let mut ballots = ballots;
    let mut results = Vec::new();
    let mut top_prefs = count_top_preferences(&ballots);
    while !top_prefs.is_empty() {
        let fewest_top_prefs = fewest_top_preferences(&top_prefs)?;
        ballots
            .iter_mut()
            .map(|mut ballot| remove_by_shifting_preferences(&mut ballot, &fewest_top_prefs))
            .for_each(drop);
        results.push(fewest_top_prefs);
        top_prefs = count_top_preferences(&ballots);
    }
    let results = results
        .into_iter()
        .rev()
        .zip(0..)
        .map(|(answer_ids, rank)| {
            answer_ids
                .into_iter()
                .map(move |answer_id| RankedAnswer { rank, answer_id })
        })
        .flatten()
        .collect::<Vec<_>>();
    Ok(results)
}

#[cfg(test)]
mod tests {
    use uuid::Uuid;

    use super::*;

    #[test]
    fn test_count_ballots_1() {
        let ballots = vec![(0, 1, 2), (0, 1, 2), (0, 2, 1), (1, 2, 0)]
            .into_iter()
            .map(|(first, second, third)| {
                vec![
                    RankedAnswer {
                        rank: 0,
                        answer_id: first,
                    },
                    RankedAnswer {
                        rank: 1,
                        answer_id: second,
                    },
                    RankedAnswer {
                        rank: 2,
                        answer_id: third,
                    },
                ]
            })
            .map(|answer_ranking| Ballot {
                id: Uuid::new_v4(),
                answer_ranking,
                debate_id: -1,
                voter_id: -1,
            })
            .collect::<Vec<_>>();

        let ranking = count_ballots(ballots).unwrap();
        let expected_ranking = vec![
            RankedAnswer {
                rank: 0,
                answer_id: 0,
            },
            RankedAnswer {
                rank: 1,
                answer_id: 1,
            },
            RankedAnswer {
                rank: 2,
                answer_id: 2,
            },
        ];
        assert_eq!(ranking, expected_ranking);
    }

    #[test]
    fn test_count_ballots_2() {
        let ballots = vec![
            (1, 0, 2, 3),
            (0, 3, 1, 2),
            (2, 0, 1, 3),
            (1, 2, 0, 3),
            (3, 0, 1, 2),
        ]
        .into_iter()
        .map(|(first, second, third, fourth)| {
            vec![
                RankedAnswer {
                    rank: 0,
                    answer_id: first,
                },
                RankedAnswer {
                    rank: 1,
                    answer_id: second,
                },
                RankedAnswer {
                    rank: 2,
                    answer_id: third,
                },
                RankedAnswer {
                    rank: 3,
                    answer_id: fourth,
                },
            ]
        })
        .map(|answer_ranking| Ballot {
            id: Uuid::new_v4(),
            answer_ranking,
            debate_id: -1,
            voter_id: -1,
        })
        .collect::<Vec<_>>();

        let ranking = count_ballots(ballots).unwrap();
        assert_eq!(
            ranking.get(0),
            Some(&RankedAnswer {
                rank: 0,
                answer_id: 1
            })
        );
        assert_eq!(ranking.len(), 4);
        assert!(ranking.contains(&RankedAnswer {
            rank: 1,
            answer_id: 0
        }));
        assert!(ranking.contains(&RankedAnswer {
            rank: 1,
            answer_id: 2
        }));
        assert!(ranking.contains(&RankedAnswer {
            rank: 1,
            answer_id: 3
        }));
    }
}

fn count_top_preferences(ballots: &[Ballot]) -> HashMap<i64, usize> {
    let mut n_top_prefs = HashMap::new();
    ballots
        .iter()
        .max_by_key(|ballot| ballot.answer_ranking.len())
        .map(|ballot| &ballot.answer_ranking)
        .unwrap_or(&Vec::new())
        .iter()
        .map(|ranked_answer| n_top_prefs.insert(ranked_answer.answer_id, 0))
        .for_each(drop);
    let top_prefs = ballots
        .iter()
        .map(|ballot| ballot.answer_ranking.first())
        .collect::<Option<Vec<_>>>();
    match top_prefs {
        Some(top_prefs) => {
            top_prefs
                .iter()
                .map(|top_pref| {
                    let old = *n_top_prefs.get(&top_pref.answer_id).unwrap_or(&0);
                    n_top_prefs.insert(top_pref.answer_id, old + 1);
                })
                .for_each(drop);
            n_top_prefs
        }
        None => HashMap::new(),
    }
}

fn fewest_top_preferences(
    n_top_preferences: &HashMap<i64, usize>,
) -> Result<Vec<i64>, CountingError> {
    let min_n_first_prefs = n_top_preferences
        .iter()
        .map(|(_, v)| *v)
        .min()
        .ok_or(CountingError::NoCandidates)?;
    Ok(n_top_preferences
        .iter()
        .filter(|(_, v)| **v == min_n_first_prefs)
        .map(|(k, _)| *k)
        .collect())
}

fn remove_by_shifting_preferences(ballot: &mut Ballot, answer_ids: &[i64]) {
    let new_answer_ranking = ballot
        .answer_ranking
        .iter()
        .filter(|ranked_answer| !answer_ids.contains(&ranked_answer.answer_id))
        .zip(0..)
        .map(|(ranked_answer, new_rank)| RankedAnswer {
            rank: new_rank,
            answer_id: ranked_answer.answer_id,
        })
        .collect::<Vec<_>>();
    ballot.answer_ranking = new_answer_ranking;
}
