use std::convert::TryFrom;

use chrono::Utc;
use failure::Error;
use uuid::Uuid;

use crate::error::public::*;
use crate::error::*;
use crate::logic::authorization::*;
use crate::logic::ballot::*;
use crate::logic::debate::*;
use crate::logic::participation::*;
use db_access::db::*;
use db_access::models;
use db_access::models::ballots::NewBallot;
use db_access::models::partial_ballots::*;
use db_access::models::votes::*;

pub use db_access::models::debates::Debate;
pub use db_access::models::votes::Vote;

pub mod instant_runoff;
pub mod ranked_pairs;
pub mod two_choices;

#[derive(Debug, Fail, PartialEq)]
pub enum CountingError {
    #[fail(display = "no ballots")]
    NoBallots,
    #[fail(display = "no candidates")]
    NoCandidates,
}

#[derive(Debug, juniper::GraphQLInputObject)]
pub struct RankedAnswerInput {
    answer_id: Uuid,
    rank: i32,
}

#[derive(Debug, juniper::GraphQLInputObject)]
pub struct VoteInput {
    pub debate_id: Uuid,
    /// This array must contain all answer IDs.
    /// The answer with rank 0 is considered to be the first choice,
    /// the one with rank 1 the second one, and so on.
    pub answer_ranking: Vec<RankedAnswerInput>,
}

pub fn vote(
    db: &dyn Db,
    auth_context: &AuthContext,
    new_vote: &VoteInput,
) -> Result<bool, PublicError<VotingError>> {
    let voter = auth_context.account();
    let debate = db
        .get_debate_with_external_id(new_vote.debate_id)
        .context(PublicErrorKind::Other)?;

    if !participates_in(db, auth_context, &voter, &debate).context(PublicErrorKind::Other)? {
        return Err(PublicErrorKind::Public(VotingError::Unauthorized).into());
    }

    if implicit_debate_state(db, &debate, Utc::now()).context(PublicErrorKind::Other)?
        != DebateState::Voting
    {
        return Err(PublicErrorKind::Public(VotingError::Unauthorized).into());
    }

    match db.get_vote_of_voter_in_debate(voter.id, debate.id) {
        Ok(_) => return Err(PublicErrorKind::Public(VotingError::AlreadyVoted).into()),
        Err(QueryError::NotFound) => (),
        Err(e) => Err(e).context(PublicErrorKind::Other)?,
    };

    if !is_answer_ranking_valid(db, &debate, &new_vote.answer_ranking)
        .context(PublicErrorKind::Other)?
    {
        return Err(PublicErrorKind::Public(VotingError::InvalidData).into());
    }

    let ballot = NewBallot {
        external_id: Uuid::new_v4(),
        debate: debate.id,
        voter: voter.id,
        prepared_time: None,
    };
    let ballot = db.insert_ballot(ballot).context(PublicErrorKind::Other)?;

    let vote = NewVote {
        external_id: Uuid::new_v4(),
        voter: voter.id,
        debate: debate.id,
    };
    db.insert_vote(vote).context(PublicErrorKind::Other)?;

    insert_answer_ranking_partial_ballots(db, ballot.id, &new_vote.answer_ranking)
        .context(PublicErrorKind::Other)?;

    Ok(true)
}

pub fn vote_cast(db: &dyn Db, auth_context: &AuthContext, debate: &Debate) -> Result<bool, Error> {
    match db.get_vote_of_voter_in_debate(auth_context.account().id, debate.id) {
        Ok(_) => Ok(true),
        Err(QueryError::NotFound) => Ok(false),
        Err(other) => Err(other.into()),
    }
}

pub fn prepare_vote(
    db: &dyn Db,
    auth_context: &AuthContext,
    new_vote: &VoteInput,
) -> Result<PreparedBallot, Error> {
    let voter = auth_context.account();
    let debate = db.get_debate_with_external_id(new_vote.debate_id)?;

    if !auth_context.may_access_debate(db, &debate)? {
        return Err(CreationError::Unauthorized.into());
    }

    if implicit_debate_state(db, &debate, Utc::now())? != DebateState::Combined {
        return Err(CreationError::Unauthorized.into());
    }

    if !is_answer_ranking_valid(db, &debate, &new_vote.answer_ranking)? {
        return Err(CreationError::InvalidData.into());
    }

    let (ballot, partial_ballots) = match db.get_ballot_of_voter_of_debate(debate.id, voter.id) {
        Ok(existing_ballot) => {
            db.delete_partial_ballots_of_ballot(existing_ballot.id)?;
            let partial_ballots = insert_answer_ranking_partial_ballots(
                db,
                existing_ballot.id,
                &new_vote.answer_ranking,
            )?;
            let ballot = db.update_ballot(models::ballots::Ballot {
                prepared_time: Some(Utc::now()),
                ..existing_ballot
            })?;
            Ok((ballot, partial_ballots))
        }
        Err(QueryError::NotFound) => {
            let ballot = NewBallot {
                external_id: Uuid::new_v4(),
                debate: debate.id,
                voter: voter.id,
                prepared_time: Some(Utc::now()),
            };
            let ballot = db.insert_ballot(ballot)?;
            let partial_ballots =
                insert_answer_ranking_partial_ballots(db, ballot.id, &new_vote.answer_ranking)?;
            Ok((ballot, partial_ballots))
        }
        Err(e) => Err(e),
    }?;

    let answer_ranking = partial_ballots
        .iter()
        .map(|partial_ballot| RankedAnswer {
            rank: partial_ballot.rank,
            answer_id: partial_ballot.answer,
        })
        .collect();
    Ok(PreparedBallot(crate::logic::ballot::Ballot {
        id: ballot.external_id,
        answer_ranking,
        debate_id: ballot.debate,
        voter_id: voter.id,
    }))
}

fn insert_answer_ranking_partial_ballots(
    db: &dyn Db,
    ballot_id: i64,
    answer_ranking: &[RankedAnswerInput],
) -> Result<Vec<PartialBallot>, Error> {
    answer_ranking
        .iter()
        .map(|ranked_answer| {
            let answer = db.get_answer_with_external_id(ranked_answer.answer_id)?;
            let partial_ballot = NewPartialBallot {
                answer: answer.id,
                rank: ranked_answer.rank,
                ballot: ballot_id,
            };
            let partial_ballot = db.insert_partial_ballot(partial_ballot)?;
            Ok(partial_ballot)
        })
        .collect::<Result<Vec<PartialBallot>, Error>>()
}

fn is_answer_ranking_valid(
    db: &dyn Db,
    debate: &Debate,
    answer_ranking: &[RankedAnswerInput],
) -> QueryResult<bool> {
    let l = i32::try_from(answer_ranking.len())
        .map_err(|_| QueryError::Other("More answers than maximum value of i32".to_owned()))?;
    let answers = db.get_answers_of_debate(debate.id)?;
    if l == i32::try_from(answers.len())
        .map_err(|_| QueryError::Other("More answers than maximum value of i32".to_owned()))?
    {
        let complete_ranking = answer_ranking
            .iter()
            .zip(0..l - 1)
            .map(|(ranked_answer, expected_rank)| ranked_answer.rank == expected_rank)
            .all(|b| b);

        if !complete_ranking {
            return Ok(false);
        }

        let mut all_answer_ids = answer_ranking
            .iter()
            .map(|ranked_answer| ranked_answer.answer_id)
            .collect::<Vec<_>>();
        all_answer_ids.sort();
        all_answer_ids.dedup();
        let all_answers_unique = all_answer_ids.len() == answer_ranking.len();

        if !all_answers_unique {
            return Ok(false);
        }
        let mut all_answer_ids_of_debate = answers
            .iter()
            .map(|answer| answer.external_id)
            .collect::<Vec<_>>();
        all_answer_ids_of_debate.sort();
        let all_answers_belong_to_debate = all_answer_ids == all_answer_ids_of_debate;

        Ok(all_answers_belong_to_debate)
    } else {
        Ok(false)
    }
}
