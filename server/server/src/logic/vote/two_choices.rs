use crate::logic::ballot::*;

pub fn count_ballots(
    ballots: Vec<Ballot>,
    first_choice: i64,
    second_choice: i64,
) -> Result<(usize, usize), ()> {
    let ballots_valid = ballots.iter().all(|ballot| {
        if ballot.answer_ranking.len() != 2 {
            return false;
        }
        let first_pref = ballot.answer_ranking.get(0).unwrap().answer_id;
        let second_pref = ballot.answer_ranking.get(1).unwrap().answer_id;
        (first_pref == first_choice && second_pref == second_choice)
            || (second_pref == first_choice && first_pref == second_choice)
    });
    if ballots_valid {
        let n_first_choice = ballots
            .iter()
            .filter(|ballot| ballot.answer_ranking.first().unwrap().answer_id == first_choice)
            .count();
        let n_second_choice = ballots
            .iter()
            .filter(|ballot| ballot.answer_ranking.first().unwrap().answer_id == second_choice)
            .count();
        Ok((n_first_choice, n_second_choice))
    } else {
        Err(())
    }
}

#[cfg(test)]
mod tests {
    use uuid::Uuid;

    use super::*;

    #[test]
    fn test_count_ballots() {
        let ballots = vec![(0, 1), (0, 1), (0, 1), (1, 0)]
            .into_iter()
            .map(|(first, second)| {
                vec![
                    RankedAnswer {
                        rank: 0,
                        answer_id: first,
                    },
                    RankedAnswer {
                        rank: 1,
                        answer_id: second,
                    },
                ]
            })
            .map(|answer_ranking| Ballot {
                id: Uuid::new_v4(),
                answer_ranking,
                debate_id: -1,
                voter_id: -1,
            })
            .collect::<Vec<_>>();
        let (n_0, n_1) = count_ballots(ballots, 0, 1).unwrap();
        assert_eq!(n_0, 3);
        assert_eq!(n_1, 1);
    }
}
