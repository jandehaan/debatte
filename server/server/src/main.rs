extern crate libeltrot;

fn main() {
    match libeltrot::main() {
        Ok(()) => (),
        Err(message) => {
            eprintln!("{}", message);
            std::process::exit(1);
        }
    }
}
