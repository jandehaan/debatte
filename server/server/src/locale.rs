use serde::{Deserialize, Serialize};

use db_access::models;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Locale {
    DeDE,
    EnUS,
}

impl From<models::types::Locale> for Locale {
    fn from(locale: models::types::Locale) -> Locale {
        match locale {
            models::types::Locale::DeDE => Locale::DeDE,
            models::types::Locale::EnUS => Locale::EnUS,
        }
    }
}

impl Locale {
    // This function is not complex, it’s basically just a table.
    #[allow(clippy::cognitive_complexity)]
    pub fn get(self, phrase: Phrase) -> &'static str {
        match phrase {
            Phrase::Answers => match self {
                Locale::EnUS => "Answers",
                Locale::DeDE => "Antworten",
            },
            Phrase::Arguments => match self {
                Locale::EnUS => "Arguments",
                Locale::DeDE => "Argumente",
            },
            Phrase::Supports => match self {
                Locale::EnUS => "Supports:",
                Locale::DeDE => "Spricht für:",
            },
            Phrase::UnconsideredLackOfMerit => match self {
                Locale::EnUS => "This argument was not considered because of a lack of merit, see section",
                Locale::DeDE => "Dieses Argument wurde nicht berücksichtigt, weil es für sachlich nicht richtig befunden wurde, siehe Abschnitt",
            },
            Phrase::UnconsideredLackOfRelevance => match self {
                Locale::EnUS => "This argument was not considered because of a lack of relevance, see section",
                Locale::DeDE => "Dieses Argument wurde nicht berücksichtigt, weil es für irrelevant befunden wurde, siehe Abschnitt",
            },
            Phrase::VotingResults => match self {
                Locale::EnUS => "Voting Results",
                Locale::DeDE => "Abstimmungsergebnisse",
            },
            Phrase::Participants => match self {
                Locale::EnUS => "Participants",
                Locale::DeDE => "Teilnehmer",
            },
            Phrase::Schedule => match self {
                Locale::EnUS => "Schedule",
                Locale::DeDE => "Zeitplan",
            },
            Phrase::StartCollectingAnswers => match self {
                Locale::EnUS => "Start collecting answers",
                Locale::DeDE => "Antworten sammeln",
            },
            Phrase::StartDiscussing => match self {
                Locale::EnUS => "Start discussing",
                Locale::DeDE => "Diskutieren",
            },
            Phrase::StartVoting => match self {
                Locale::EnUS => "Start voting",
                Locale::DeDE => "Abstimmen",
            },
            Phrase::FinishVoting => match self {
                Locale::EnUS => "Finish voting",
                Locale::DeDE => "Ende der Abstimmung",
            },
            Phrase::AnswerWithMostVotes => match self {
                Locale::EnUS => "The answer with the most votes was",
                Locale::DeDE => "Die Antwort mit den meisten Stimmen war",
            },
            Phrase::Summary => match self {
                Locale::EnUS => "Summary",
                Locale::DeDE => "Zusammenfassung",
            },
            Phrase::Of => match self {
                Locale::EnUS => "of",
                Locale::DeDE => "von",
            },
            Phrase::NoVotingResults => match self {
                Locale::EnUS => "Because there were no answers or no ballots, there are no voting results.",
                Locale::DeDE => "Weil es keine Antworten oder keine Stimmen gab, gibt es keine Ergebnisse."
            },
            Phrase::TallyingReport => match self {
                Locale::EnUS => "Tallying Report",
                Locale::DeDE => "Auszählungsbericht"
            },
            Phrase::VotingMethodExplanation => match self {
                Locale::EnUS => "Voting Method",
                Locale::DeDE => "Wahlsystem"
            },
            Phrase::ActualVotingMethodExplanation => match self {
                Locale::EnUS => "The voting method used is the ranked-pairs method. The winner is chosen as follows: First, the number of ballots where one candidate is ranked better than another one is counted for all pairs of candidates. The difference between the number of ballots who ranked candidate 1 ahead of candidate 2 and the number of ballots where 2 is ranked ahead of 1 is calculated and written down in a table. Then, the pair with the largest margin is locked in, i.e. this ranking will definitely be preserved in the final result, by drawing an arrow between the two in a diagram. This is repeated for all pairs of candidates, starting with the clearest winners, unless that would lead to a cycle in the diagram (which would mean that 1 is preferred to 2 is preferred to 1, and so on.). Finally, the winner is chosen as the candidate from which you can trace a path to all other candidates in the diagram.",
                Locale::DeDE => "Das Wahlsystem ist die sogenannte ranked-pairs-Methode. Der Wahlsieger wird wie folgt ermittelt: Zuerst wird für alle Paare von Kandidaten die Zahl der Stimmzettel, auf denen ein Kandidat besser als ein anderer eingestuft wurde, gezählt. Die Differenz zwischen der Zahl der Stimmzettel, die Kandidat 1 besser als Kandidat 2 bewerten und denen, die 2 gegenüber 1 bevorzugen, wird in eine Tabelle eingetragen. Anschließend wird die Anordnung des Paares mit der größten Differenz festgesetzt, indem ein Pfeil zwischen den beiden Kandidaten im Diagramm eingezeichnet wird, sofern dadurch kein Kreis im Diagramm ensteht (das würde bedeuten, dass 1 gegenüber 2, 2 gegenüber 1, 1 gegenüber 2, usw. bevorzugt wird). Schließlich wird die Antwort, von der es im Diagramm einen Weg zu allen anderen gibt, ausgewählt."
            },
            Phrase::PairContestMargins => match self {
                Locale::EnUS => "Pair Contest Margins",
                Locale::DeDE => "Differenzen der paarweisen Präferenzen"
            },
            Phrase::Graph => match self {
                Locale::EnUS => "Diagram",
                Locale::DeDE => "Diagramm"
            },
            Phrase::Ballots => match self {
                Locale::EnUS => "Ballots",
                Locale::DeDE => "Stimmzettel"
            },
            Phrase::DebateAboutMerit => match self {
                Locale::EnUS => "Debate About Merit",
                Locale::DeDE => "Diskussion der sachlichen Richtigkeit"
            },
            Phrase::DebateAboutRelevance => match self {
                Locale::EnUS => "Debate About Relevance",
                Locale::DeDE => "Diskussion der Relevanz"
            },
            Phrase::Participant => match self {
                Locale::EnUS => "Participant",
                Locale::DeDE => "Teilnehmer"
            },
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Phrase {
    Answers,
    VotingResults,
    Arguments,
    Supports,
    UnconsideredLackOfMerit,
    UnconsideredLackOfRelevance,
    Participants,
    Schedule,
    StartCollectingAnswers,
    StartDiscussing,
    StartVoting,
    FinishVoting,
    AnswerWithMostVotes,
    Summary,
    Of,
    NoVotingResults,
    TallyingReport,
    VotingMethodExplanation,
    ActualVotingMethodExplanation,
    PairContestMargins,
    Graph,
    Ballots,
    DebateAboutMerit,
    DebateAboutRelevance,
    Participant,
}
