macro_rules! graphql_list {
    ($name:ident, $item:ty) => {
        #[derive(Debug)]
        pub struct $name {
            pub id: String,
            pub list: Vec<$item>,
        }

        impl juniper::GraphQLType for $name {
            type Context = Context;
            type TypeInfo = ();

            fn name(_: &()) -> Option<&'static str> {
                Some(stringify!($name))
            }

            fn concrete_type_name(&self, _: &Context, _: &()) -> String {
                stringify!($name).to_owned()
            }

            fn meta<'r>(_: &(), registry: &mut juniper::Registry<'r>) -> juniper::meta::MetaType<'r>
            where
                juniper::DefaultScalarValue: 'r,
            {
                let fields = &[
                    registry.field::<Option<&str>>("id", &()),
                    registry.field::<Option<&Vec<$item>>>("list", &()),
                ];

                registry.build_object_type::<$name>(&(), fields).into_meta()
            }

            fn resolve_field(
                &self,
                info: &(),
                field_name: &str,
                _: &juniper::Arguments,
                executor: &juniper::Executor<Context>,
            ) -> juniper::ExecutionResult {
                match field_name {
                    "id" => executor.resolve_with_ctx(info, &self.id),
                    "list" => executor.resolve_with_ctx(info, &self.list),
                    _ => panic!(
                        "Field {} not found on type {}",
                        field_name,
                        stringify!($name)
                    ),
                }
            }
        }
    };
}
