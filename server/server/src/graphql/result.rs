macro_rules! graphql_result {
    ($name:ident, $ok:ty, $err:ty) => {
        #[derive(Debug)]
        pub struct $name(Result<$ok, $err>);

        impl juniper::GraphQLType for $name {
            type Context = Context;
            type TypeInfo = ();

            fn name(_: &()) -> Option<&'static str> {
                Some(stringify!($name))
            }

            fn concrete_type_name(&self, _: &Context, _: &()) -> String {
                stringify!($name).to_owned()
            }

            fn meta<'r>(_: &(), registry: &mut juniper::Registry<'r>) -> juniper::meta::MetaType<'r>
            where
                juniper::DefaultScalarValue: 'r,
            {
                let fields = &[
                    registry.field::<Option<&$ok>>("ok", &()),
                    registry.field::<Option<&$err>>("err", &()),
                ];

                registry.build_object_type::<$name>(&(), fields).into_meta()
            }

            fn resolve_field(
                &self,
                info: &(),
                field_name: &str,
                _: &juniper::Arguments,
                executor: &juniper::Executor<Context>,
            ) -> juniper::ExecutionResult {
                match field_name {
                    "ok" => executor.resolve_with_ctx(info, &self.0.as_ref().ok()),
                    "err" => executor.resolve_with_ctx(info, &self.0.as_ref().err()),
                    _ => panic!(
                        "Field {} not found on type {}",
                        field_name,
                        stringify!($name)
                    ),
                }
            }
        }

        impl From<Result<$ok, $err>> for $name {
            fn from(r: Result<$ok, $err>) -> $name {
                $name(r)
            }
        }
    };
}
