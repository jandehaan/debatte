use chrono::*;
use juniper::FieldResult;
use uuid::Uuid;

use crate::error::public::*;
use crate::graphql::context::*;
use crate::graphql::query::enums::*;
use crate::graphql::query::prepared_ballot::*;
use crate::logic::answer::*;
use crate::logic::argument::*;
use crate::logic::authorization::*;
use crate::logic::ballot::*;
use crate::logic::debate::summary::*;
use crate::logic::debate::*;
use crate::logic::participation::*;
use crate::logic::public_invite_link::*;
use crate::logic::vote::*;
use crate::markdown::truncate_markdown;

#[derive(Debug, juniper::GraphQLObject)]
pub struct VoteCast {
    pub id: Uuid,
    pub vote_cast: bool,
}

graphql_list!(ParticipantsList, WithAuthContext<OtherAccount>);
graphql_list!(ArgumentsList, WithAuthContext<Argument>);
graphql_list!(ConsideredArgumentsList, WithAuthContext<Argument>);
graphql_list!(UnconsideredArgumentsList, WithAuthContext<Argument>);
graphql_list!(AnswersList, WithAuthContext<Answer>);
graphql_list!(BallotsList, WithAuthContext<Ballot>);
graphql_list!(VotesList, WithAuthContext<Vote>);
graphql_list!(VotingResultsList, WithAuthContext<RankedAnswer>);
graphql_list!(SubDebatesList, WithAuthContext<Debate>);

pub fn get_arguments(
    debate: &Debate,
    auth_context: &AuthContext,
    context: &Context,
) -> FieldResult<ArgumentsList> {
    let arguments = get_arguments_of_debate(context.db.as_ref(), auth_context, debate)?;
    let auth_arguments = arguments
        .into_iter()
        .map(|argument| auth_context.with(argument))
        .collect();
    Ok(ArgumentsList {
        id: format!("arguments-{}", debate.external_id),
        list: auth_arguments,
    })
}

pub fn get_answers(
    debate: &Debate,
    auth_context: &AuthContext,
    context: &Context,
) -> FieldResult<AnswersList> {
    let answers = get_answers_of_debate(context.db.as_ref(), auth_context, debate)?;
    let auth_answers = answers
        .into_iter()
        .map(|answer| auth_context.with(answer))
        .collect();
    Ok(AnswersList {
        id: format!("answers-{}", debate.external_id),
        list: auth_answers,
    })
}

pub fn get_sub_debates(
    context: &Context,
    auth_context: &AuthContext,
    debate: &Debate,
) -> FieldResult<SubDebatesList> {
    let sub_debates = get_sub_debates_of_debate(context.db.as_ref(), auth_context, debate)?
        .into_iter()
        .map(|sub_debate| auth_context.with(sub_debate))
        .collect();
    Ok(SubDebatesList {
        id: format!("sub-debates-{}", debate.external_id),
        list: sub_debates,
    })
}

pub fn get_cached_prepared_ballot(
    context: &Context,
    auth_context: &AuthContext,
    debate: &Debate,
) -> FieldResult<CachedPreparedBallot> {
    let prepared_ballot = get_prepared_ballot_of_debate(context.db.as_ref(), auth_context, debate)?
        .map(|prepared_ballot| auth_context.with(prepared_ballot));
    Ok(CachedPreparedBallot {
        id: format!("prepared-ballot-{}", debate.external_id),
        prepared_ballot,
    })
}

pub struct ReviewRequired {
    debate_id: Uuid,
    review_required: bool,
}

#[juniper::object(Context = Context)]
impl ReviewRequired {
    fn id() -> String {
        format!("{}", self.debate_id)
    }

    fn review_required() -> bool {
        self.review_required
    }
}

pub fn get_review_required(
    context: &Context,
    auth_context: &AuthContext,
    debate: &Debate,
) -> FieldResult<ReviewRequired> {
    match get_cached_prepared_ballot(context, auth_context, debate)?.prepared_ballot {
        Some(cached_prepared_ballot_with) => {
            let prepared_ballot = cached_prepared_ballot_with.inner;
            let new_arguments = new_arguments(
                context.db.as_ref(),
                &auth_context.account(),
                &prepared_ballot,
            )?;
            Ok(ReviewRequired {
                debate_id: debate.external_id,
                review_required: new_arguments,
            })
        }
        None => Ok(ReviewRequired {
            debate_id: debate.external_id,
            review_required: false,
        }),
    }
}

graphql_result!(SummaryLinksResult, SummaryLinks, GetSummaryLinksError);

#[juniper::object(Context = Context, name = "Debate")]
impl WithAuthContext<Debate> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn question() -> &String {
        &self.inner.question
    }

    fn details(context: &Context) -> FieldResult<String> {
        let details = get_debate_details(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(details)
    }

    fn short_details(context: &Context) -> FieldResult<String> {
        let details = get_debate_details(context.db.as_ref(), &self.auth_context, &self.inner)?;
        let short_details = truncate_markdown(140, &details);
        let short_details = if short_details.len() < details.len() {
            short_details + "…"
        } else {
            short_details
        };
        Ok(short_details)
    }

    fn state(context: &Context) -> FieldResult<GQLDebateState> {
        let state = implicit_debate_state(context.db.as_ref(), &self.inner, Utc::now())?;
        Ok(state.into())
    }

    fn allow_adding_more_answers() -> bool {
        self.inner.allow_adding_more_answers
    }

    fn collecting_answers_start(context: &Context) -> FieldResult<DateTime<Utc>> {
        match self.inner.collecting_answers_start {
            Some(t) => Ok(t),
            None => Ok(
                get_top_level_debate_recursively(context.db.as_ref(), self.inner.id)?
                    .debating_start,
            ),
        }
    }

    fn debating_start(context: &Context) -> FieldResult<DateTime<Utc>> {
        match self.inner.debating_start {
            Some(t) => Ok(t),
            None => Ok(
                get_top_level_debate_recursively(context.db.as_ref(), self.inner.id)?
                    .debating_start,
            ),
        }
    }

    fn voting_start(context: &Context) -> FieldResult<DateTime<Utc>> {
        match self.inner.voting_start {
            Some(t) => Ok(t),
            None => Ok(
                get_top_level_debate_recursively(context.db.as_ref(), self.inner.id)?
                    .debating_start,
            ),
        }
    }

    fn voting_end(context: &Context) -> FieldResult<DateTime<Utc>> {
        match self.inner.voting_end {
            Some(t) => Ok(t),
            None => Ok(
                get_top_level_debate_recursively(context.db.as_ref(), self.inner.id)?.voting_start,
            ),
        }
    }

    fn concerned_argument(context: &Context) -> FieldResult<Option<WithAuthContext<Argument>>> {
        let argument =
            get_concerned_argument_of_debate(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(argument)
    }

    fn kind() -> GQLDebateKind {
        self.inner.kind.into()
    }

    fn arguments(context: &Context) -> FieldResult<ArgumentsList> {
        let arguments = get_arguments(&self.inner, &self.auth_context, context)?;
        Ok(arguments)
    }

    fn considered_arguments(context: &Context) -> FieldResult<ConsideredArgumentsList> {
        let considered_arguments = get_considered_arguments_of_debate(
            context.db.as_ref(),
            &self.auth_context,
            &self.inner,
        )?;
        let considered_arguments = considered_arguments
            .into_iter()
            .map(|argument| self.auth_context.with(argument))
            .collect();
        Ok(ConsideredArgumentsList {
            id: format!("considered-arguments-{}", self.inner.external_id),
            list: considered_arguments,
        })
    }

    fn unconsidered_arguments(context: &Context) -> FieldResult<UnconsideredArgumentsList> {
        let unconsidered_arguments = get_unconsidered_arguments_of_debate(
            context.db.as_ref(),
            &self.auth_context,
            &self.inner,
        )?;
        let unconsidered_arguments = unconsidered_arguments
            .into_iter()
            .map(|argument| self.auth_context.with(argument))
            .collect();
        Ok(UnconsideredArgumentsList {
            id: format!("unconsidered-arguments-{}", self.inner.external_id),
            list: unconsidered_arguments,
        })
    }

    fn participants(context: &Context) -> FieldResult<ParticipantsList> {
        let participants =
            participants_of_debate(context.db.as_ref(), &self.auth_context, &self.inner)?
                .into_iter()
                .map(|account| self.auth_context.with(account))
                .collect();
        Ok(ParticipantsList {
            id: format!("participants-{}", self.inner.external_id),
            list: participants,
        })
    }

    fn answers(context: &Context) -> FieldResult<AnswersList> {
        let answers = get_answers(&self.inner, &self.auth_context, context)?;
        Ok(answers)
    }

    fn ballots(context: &Context) -> FieldResult<Option<BallotsList>> {
        let ballots =
            match get_ballots_of_debate(context.db.as_ref(), &self.auth_context, &self.inner)? {
                Some(ballots) => ballots,
                None => return Ok(None),
            };
        let auth_ballots = ballots
            .into_iter()
            .map(|ballot| self.auth_context.with(ballot))
            .collect();
        Ok(Some(BallotsList {
            id: format!("ballots-{}", self.inner.external_id),
            list: auth_ballots,
        }))
    }

    fn vote_cast(context: &Context) -> FieldResult<VoteCast> {
        let vote_cast = vote_cast(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(VoteCast {
            id: self.inner.external_id,
            vote_cast,
        })
    }

    fn voting_results(context: &Context) -> FieldResult<Option<VotingResultsList>> {
        let ballots =
            match get_ballots_of_debate(context.db.as_ref(), &self.auth_context, &self.inner) {
                Ok(Some(ballots)) => ballots,
                Ok(None) => return Ok(None),
                Err(e) => return Err(e.into()),
            };
        let ranked_answers = match ranked_pairs::count_ballots(&ballots) {
            Ok(ranked_answers) => ranked_answers,
            Err(CountingError::NoBallots) => return Ok(None),
            Err(CountingError::NoCandidates) => return Ok(None),
        };
        let auth_ranked_answers = ranked_answers
            .0
            .into_iter()
            .map(|ranked_answer| self.auth_context.with(ranked_answer))
            .collect();
        Ok(Some(VotingResultsList {
            id: format!("voting-results-{}", self.inner.external_id),
            list: auth_ranked_answers,
        }))
    }

    fn prepared_vote(context: &Context) -> FieldResult<CachedPreparedBallot> {
        let prepared_vote = get_cached_prepared_ballot(context, &self.auth_context, &self.inner)?;
        Ok(prepared_vote)
    }

    fn sub_debates(context: &Context) -> FieldResult<SubDebatesList> {
        let sub_debates = get_sub_debates(context, &self.auth_context, &self.inner)?;
        Ok(sub_debates)
    }

    fn has_sub_debates(context: &Context) -> FieldResult<bool> {
        let sub_debates =
            get_sub_debates_of_debate(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(!sub_debates.is_empty())
    }

    fn last_change(context: &Context) -> FieldResult<DateTime<Utc>> {
        let last_change =
            get_last_participation_time(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(last_change)
    }

    fn review_required(context: &Context) -> FieldResult<ReviewRequired> {
        let review_required = get_review_required(context, &self.auth_context, &self.inner)?;
        Ok(review_required)
    }

    fn public_invite_link(context: &Context) -> Option<String> {
        get_public_invite_link_of_debate(context.email_sender.get_link_server(), &self.inner)
    }

    fn summary_links(context: &Context) -> FieldResult<SummaryLinksResult> {
        get_summary_links(
            context.db.as_ref(),
            &context.email_sender.get_api_server(),
            &self.inner,
        )
        .into_field_result()
        .map(|r| r.into())
    }
}
