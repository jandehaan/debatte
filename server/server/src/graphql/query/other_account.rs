use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::account::*;
use crate::logic::authorization::*;

#[juniper::object(Context = Context)]
impl WithAuthContext<OtherAccount> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn name() -> &str {
        &self.inner.name
    }
}
