use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::account::*;
use crate::logic::authorization::*;
use crate::logic::vote::*;

#[juniper::object(Context = Context, name = "Vote")]
impl WithAuthContext<Vote> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn voter(context: &Context) -> FieldResult<WithAuthContext<OtherAccount>> {
        let voter = get_other_account(context.db.as_ref(), &self.auth_context, self.inner.voter)?;
        Ok(self.auth_context.with(voter))
    }
}
