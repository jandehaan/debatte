use db_access::models::types::*;

#[derive(Debug, Clone, Copy, juniper::GraphQLEnum)]
#[graphql(name = "DebateState")]
pub enum GQLDebateState {
    CollectingAnswers,
    Debating,
    Voting,
    Finished,
    Combined,
    Waiting,
}

impl From<DebateState> for GQLDebateState {
    fn from(x: DebateState) -> GQLDebateState {
        match x {
            DebateState::CollectingAnswers => GQLDebateState::CollectingAnswers,
            DebateState::Debating => GQLDebateState::Debating,
            DebateState::Voting => GQLDebateState::Voting,
            DebateState::Finished => GQLDebateState::Finished,
            DebateState::Combined => GQLDebateState::Combined,
            DebateState::Waiting => GQLDebateState::Waiting,
        }
    }
}

#[derive(Debug, Clone, Copy, juniper::GraphQLEnum)]
#[graphql(name = "DebateKind")]
pub enum GQLDebateKind {
    MeritOfArgument,
    RelevanceOfArgument,
    Other,
}

impl From<DebateKind> for GQLDebateKind {
    fn from(x: DebateKind) -> GQLDebateKind {
        match x {
            DebateKind::MeritOfArgument => GQLDebateKind::MeritOfArgument,
            DebateKind::RelevanceOfArgument => GQLDebateKind::RelevanceOfArgument,
            DebateKind::Other => GQLDebateKind::Other,
        }
    }
}

impl From<GQLDebateKind> for DebateKind {
    fn from(x: GQLDebateKind) -> DebateKind {
        match x {
            GQLDebateKind::MeritOfArgument => DebateKind::MeritOfArgument,
            GQLDebateKind::RelevanceOfArgument => DebateKind::RelevanceOfArgument,
            GQLDebateKind::Other => DebateKind::Other,
        }
    }
}

#[derive(Debug, Clone, Copy, juniper::GraphQLEnum)]
#[graphql(name = "AnswerSemantics")]
pub enum GQLAnswerSemantics {
    Yes,
    No,
    None,
}

impl From<AnswerSemantics> for GQLAnswerSemantics {
    fn from(x: AnswerSemantics) -> GQLAnswerSemantics {
        match x {
            AnswerSemantics::Yes => GQLAnswerSemantics::Yes,
            AnswerSemantics::No => GQLAnswerSemantics::No,
            AnswerSemantics::None => GQLAnswerSemantics::None,
        }
    }
}

#[derive(Debug, Clone, Copy, juniper::GraphQLEnum)]
#[graphql(name = "Locale")]
pub enum GQLLocale {
    EnUS,
    DeDE,
}

impl From<Locale> for GQLLocale {
    fn from(x: Locale) -> GQLLocale {
        match x {
            Locale::EnUS => GQLLocale::EnUS,
            Locale::DeDE => GQLLocale::DeDE,
        }
    }
}

impl From<GQLLocale> for Locale {
    fn from(x: GQLLocale) -> Locale {
        match x {
            GQLLocale::EnUS => Locale::EnUS,
            GQLLocale::DeDE => Locale::DeDE,
        }
    }
}
