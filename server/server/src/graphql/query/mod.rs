pub mod account;
pub mod answer;
pub mod argument;
pub mod authenticated_query;
pub mod ballot;
pub mod debate;
pub mod enums;
pub mod invitation;
pub mod other_account;
pub mod prepared_ballot;
pub mod ranked_answer;
pub mod vote;

use juniper::FieldResult;

use crate::error::public::*;
use crate::graphql::context::*;
use crate::graphql::query::authenticated_query::*;
use crate::logic::authorization::*;
use crate::logic::tokens::auth_token::*;

#[derive(Debug)]
pub struct Query;

graphql_result!(AuthenticatedQueryResult, AuthenticatedQuery, TokenAuthError);
graphql_result!(IsAuthTokenValidResult, bool, TokenAuthError);

#[juniper::object(Context = Context)]
impl Query {
    fn api_version() -> &str {
        "1.0"
    }

    fn authenticated_query(
        context: &Context,
        token: String,
    ) -> FieldResult<AuthenticatedQueryResult> {
        Ok(AuthContext::from_token(context.db.as_ref(), &token)
            .map(|auth_context| AuthenticatedQuery { auth_context })
            .into_field_result()?
            .into())
    }

    fn is_auth_token_valid(
        context: &Context,
        token: String,
    ) -> FieldResult<IsAuthTokenValidResult> {
        Ok(OpaqueAuthToken::try_from_str(&token[..])
            .ok_or_else(|| PublicErrorKind::Public(TokenAuthError::MalformedToken).into())
            .and_then(|auth_token| {
                match authenticate_with_token(context.db.as_ref(), &auth_token) {
                    Ok(_) => Ok(true),
                    Err(e) => match e.kind() {
                        PublicErrorKind::Public(TokenAuthError::InvalidToken) => Ok(false),
                        PublicErrorKind::Public(TokenAuthError::UnknownAccount) => Ok(false),
                        _ => Err(e),
                    },
                }
            })
            .into_field_result()?
            .into())
    }
}
