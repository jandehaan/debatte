use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::account::*;
use crate::logic::argument::*;
use crate::logic::authorization::*;
use crate::logic::debate::*;

graphql_list!(SupportedAnswersList, WithAuthContext<Answer>);
graphql_list!(DebatesAboutList, WithAuthContext<Debate>);

pub fn get_debates_about(
    context: &Context,
    auth_context: &AuthContext,
    argument: &Argument,
) -> FieldResult<DebatesAboutList> {
    let debates_about = get_debates_about_argument(context.db.as_ref(), auth_context, argument)?
        .into_iter()
        .map(|debate| auth_context.with(debate))
        .collect();
    Ok(DebatesAboutList {
        id: format!("debates_about-{}", argument.external_id),
        list: debates_about,
    })
}

#[derive(Debug)]
struct GQLArgumentWeight(ArgumentWeight);

#[juniper::object(Context = Context, name = "ArgumentWeight")]
impl GQLArgumentWeight {
    fn infinite() -> bool {
        match self.0 {
            ArgumentWeight::Infinite => true,
            ArgumentWeight::Regular(_) => false,
        }
    }

    fn regular_weight() -> Option<i32> {
        match self.0 {
            ArgumentWeight::Infinite => None,
            ArgumentWeight::Regular(weight) => Some(weight),
        }
    }
}

pub struct ArgumentConsidered {
    considered: bool,
    argument_id: Uuid,
}

#[juniper::object(Context = Context)]
impl ArgumentConsidered {
    fn considered() -> bool {
        self.considered
    }

    fn id() -> String {
        format!("{}", self.argument_id)
    }
}

pub fn get_argument_considered(
    context: &Context,
    argument: &Argument,
    auth_context: &AuthContext,
) -> FieldResult<ArgumentConsidered> {
    let considered = considered(context.db.as_ref(), argument, auth_context.account())?;
    Ok(ArgumentConsidered {
        considered,
        argument_id: argument.external_id,
    })
}

#[juniper::object(Context = Context, name = "Argument")]
impl WithAuthContext<Argument> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn heading() -> &String {
        &self.inner.heading
    }

    fn body() -> &String {
        &self.inner.body
    }

    fn author(context: &Context) -> FieldResult<WithAuthContext<OtherAccount>> {
        let author_account =
            get_other_account(context.db.as_ref(), &self.auth_context, self.inner.author)?;
        Ok(self.auth_context.with(author_account))
    }

    fn supported_answers(context: &Context) -> FieldResult<SupportedAnswersList> {
        let answers = get_supported_answers(context.db.as_ref(), &self.auth_context, &self.inner)?;
        let auth_answers = answers
            .into_iter()
            .map(|answer| self.auth_context.with(answer))
            .collect();
        Ok(SupportedAnswersList {
            id: format!("supported_answers-{}", self.inner.external_id),
            list: auth_answers,
        })
    }

    fn debate(context: &Context) -> FieldResult<WithAuthContext<Debate>> {
        let debate = get_debate_of_argument(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(debate)
    }

    fn debates_about(context: &Context) -> FieldResult<DebatesAboutList> {
        let debates_about = get_debates_about(context, &self.auth_context, &self.inner)?;
        Ok(debates_about)
    }

    fn weight(context: &Context) -> FieldResult<Option<GQLArgumentWeight>> {
        let weight = get_argument_weight(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(weight.map(GQLArgumentWeight))
    }

    fn considered(context: &Context) -> FieldResult<ArgumentConsidered> {
        let argument_considered =
            get_argument_considered(context, &self.inner, &self.auth_context)?;
        Ok(argument_considered)
    }
}
