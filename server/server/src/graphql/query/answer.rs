use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::account::*;
use crate::logic::answer::*;
use crate::logic::authorization::*;

graphql_list!(SupportingArgumentsList, WithAuthContext<Argument>);

#[juniper::object(Context = Context, name = "Answer")]
impl WithAuthContext<Answer> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn answer() -> &str {
        &self.inner.answer
    }

    fn author(context: &Context) -> FieldResult<WithAuthContext<OtherAccount>> {
        let author = get_other_account(context.db.as_ref(), &self.auth_context, self.inner.author)?;
        Ok(self.auth_context.with(author))
    }

    fn supporting_arguments(context: &Context) -> FieldResult<SupportingArgumentsList> {
        let arguments =
            get_supporting_arguments(context.db.as_ref(), &self.auth_context, &self.inner)?;
        let auth_arguments = arguments
            .into_iter()
            .map(|argument| self.auth_context.with(argument))
            .collect();
        Ok(SupportingArgumentsList {
            id: format!("supporting_arguments-{}", self.inner.external_id),
            list: auth_arguments,
        })
    }
}
