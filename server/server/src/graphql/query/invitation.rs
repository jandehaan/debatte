use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::authorization::*;
use crate::logic::invitation::*;

#[juniper::object(Context = Context, name = "Invitation")]
impl WithAuthContext<Invitation> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn debate_id(context: &Context) -> FieldResult<Uuid> {
        let debate_id = get_debate_id(context.db.as_ref(), &self.inner)?;
        Ok(debate_id)
    }

    fn debate_question(context: &Context) -> FieldResult<String> {
        let question = get_debate_question(context.db.as_ref(), &self.inner)?;
        Ok(question)
    }

    fn debate_details(context: &Context) -> FieldResult<String> {
        let details = get_debate_details(context.db.as_ref(), &self.inner)?;
        Ok(details)
    }

    fn invited_by(context: &Context) -> FieldResult<WithAuthContext<OtherAccount>> {
        let invited_by = get_invited_by(context.db.as_ref(), &self.inner)?;
        Ok(self.auth_context.with(invited_by))
    }
}
