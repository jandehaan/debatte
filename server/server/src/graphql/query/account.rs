use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::Context;
use crate::logic::account::*;
use crate::logic::authorization::*;
use crate::logic::invitation::*;

use super::enums::GQLLocale;

graphql_list!(InvitationsList, WithAuthContext<Invitation>);

pub fn get_invitations(
    context: &Context,
    auth_context: &AuthContext,
) -> FieldResult<InvitationsList> {
    let invitations = get_invitations_of_invitee(context.db.as_ref(), auth_context)?;
    Ok(InvitationsList {
        id: "invitations-list".to_owned(),
        list: invitations,
    })
}

#[juniper::object(Context = Context, name = "Account")]
impl WithAuthContext<Account> {
    fn id() -> Uuid {
        self.inner.external_id
    }

    fn email() -> &str {
        &self.inner.email
    }

    fn name() -> &str {
        &self.inner.name
    }

    fn invitations(context: &Context) -> FieldResult<InvitationsList> {
        get_invitations(context, &self.auth_context)
    }

    fn has_password() -> AccountHasPassword {
        get_account_has_password(&self.inner)
    }

    fn locale() -> GQLLocale {
        GQLLocale::from(self.inner.locale)
    }
}

pub fn get_account_has_password(account: &Account) -> AccountHasPassword {
    AccountHasPassword {
        has_password: has_password(&account),
        account_id: account.external_id,
    }
}

#[derive(Debug)]
pub struct AccountHasPassword {
    pub has_password: bool,
    pub account_id: Uuid,
}

#[juniper::object(Context = Context)]
impl AccountHasPassword {
    fn has_password() -> bool {
        self.has_password
    }

    fn id() -> String {
        format!("has_password-{}", self.account_id)
    }
}
