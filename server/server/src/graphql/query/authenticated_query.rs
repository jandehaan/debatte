use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::account::*;
use crate::logic::argument::*;
use crate::logic::authorization::*;
use crate::logic::debate::*;

#[derive(Debug)]
pub struct AuthenticatedQuery {
    pub auth_context: AuthContext,
}

graphql_list!(TopLevelDebatesList, WithAuthContext<Debate>);
graphql_list!(KnownAccountsList, WithAuthContext<OtherAccount>);

pub fn get_top_level_debates(
    auth_context: &AuthContext,
    context: &Context,
) -> FieldResult<TopLevelDebatesList> {
    let debates = get_top_level_debates_of_account(
        context.db.as_ref(),
        &auth_context,
        auth_context.account(),
    )?;
    Ok(TopLevelDebatesList {
        id: "top-level-debates".to_owned(),
        list: debates,
    })
}

#[juniper::object(Context = Context)]
impl AuthenticatedQuery {
    fn account(context: &Context) -> FieldResult<WithAuthContext<Account>> {
        let account = get_account(
            context.db.as_ref(),
            &self.auth_context,
            &self.auth_context.account().external_id,
        )?;
        Ok(self.auth_context.with(account))
    }

    fn debate(context: &Context, debate_id: Uuid) -> FieldResult<WithAuthContext<Debate>> {
        let debate = get_debate(context.db.as_ref(), &self.auth_context, debate_id)?;
        Ok(self.auth_context.with(debate))
    }

    fn top_level_debates(context: &Context) -> FieldResult<TopLevelDebatesList> {
        let top_level_debates = get_top_level_debates(&self.auth_context, context)?;
        Ok(top_level_debates)
    }

    fn known_accounts(context: &Context) -> FieldResult<KnownAccountsList> {
        let known_accounts = get_known_accounts(context.db.as_ref(), &self.auth_context)?
            .into_iter()
            .map(|account| self.auth_context.with(account))
            .collect();
        Ok(KnownAccountsList {
            id: "known_accounts".to_owned(),
            list: known_accounts,
        })
    }

    fn argument(context: &Context, argument_id: Uuid) -> FieldResult<WithAuthContext<Argument>> {
        let argument =
            get_argument_with_external_id(context.db.as_ref(), &self.auth_context, argument_id)?;
        Ok(argument)
    }
}
