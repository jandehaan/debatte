use juniper::FieldResult;

use crate::graphql::context::*;
use crate::logic::answer::*;
use crate::logic::authorization::*;
use crate::logic::ballot::*;

#[juniper::object(Context = Context, name = "RankedAnswer")]
impl WithAuthContext<RankedAnswer> {
    fn rank() -> i32 {
        self.inner.rank
    }

    fn answer(context: &Context) -> FieldResult<WithAuthContext<Answer>> {
        let answer =
            get_answer_of_ranked_answer(context.db.as_ref(), &self.auth_context, &self.inner)?;
        Ok(self.auth_context.with(answer))
    }
}
