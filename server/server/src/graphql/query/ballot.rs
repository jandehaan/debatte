use juniper::FieldResult;
use uuid::Uuid;

use crate::graphql::context::*;
use crate::logic::account::*;
use crate::logic::authorization::*;
use crate::logic::ballot::*;

#[juniper::object(Context = Context, name = "Ballot")]
impl WithAuthContext<Ballot> {
    fn id() -> Uuid {
        self.inner.id
    }

    fn answer_ranking() -> Vec<WithAuthContext<RankedAnswer>> {
        self.inner
            .answer_ranking
            .iter()
            .map(|ranked_answer| self.auth_context.with(ranked_answer.clone()))
            .collect()
    }

    fn voter(context: &Context) -> FieldResult<WithAuthContext<OtherAccount>> {
        let voter =
            get_other_account(context.db.as_ref(), &self.auth_context, self.inner.voter_id)?;
        Ok(self.auth_context.with(voter))
    }
}
