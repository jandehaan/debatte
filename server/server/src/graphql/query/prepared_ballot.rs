use crate::graphql::context::*;
use crate::logic::authorization::*;
use crate::logic::ballot::*;

#[juniper::object(Context = Context, name = "PreparedBallot")]
impl WithAuthContext<PreparedBallot> {
    fn ballot() -> WithAuthContext<Ballot> {
        self.auth_context.with(self.inner.0.clone())
    }
}

#[derive(Debug)]
pub struct CachedPreparedBallot {
    pub id: String,
    pub prepared_ballot: Option<WithAuthContext<PreparedBallot>>,
}

#[juniper::object(Context = Context)]
impl CachedPreparedBallot {
    fn id() -> &str {
        &self.id
    }

    fn prepared_ballot() -> &Option<WithAuthContext<PreparedBallot>> {
        &self.prepared_ballot
    }
}
