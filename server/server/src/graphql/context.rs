use crate::email::EmailSender;
use db_access::db::Db;

pub struct Context {
    pub db: Box<dyn Db>,
    pub email_sender: Box<dyn EmailSender>,
}

impl juniper::Context for Context {}
