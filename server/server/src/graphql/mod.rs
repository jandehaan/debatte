#[macro_use]
pub mod result;
#[macro_use]
pub mod list;

pub mod context;
pub mod mutation;
pub mod query;

pub type Schema = juniper::RootNode<'static, query::Query, mutation::Mutation>;
