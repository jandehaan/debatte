use juniper::FieldResult;
use query::enums::GQLLocale;
use uuid::Uuid;

use crate::error::public::*;
use crate::graphql::context::*;
use crate::graphql::query;
use crate::graphql::query::account::*;
use crate::graphql::query::argument::*;
use crate::graphql::query::authenticated_query::*;
use crate::graphql::query::debate::*;
use crate::graphql::query::prepared_ballot::*;
use crate::logic::account::*;
use crate::logic::answer::*;
use crate::logic::argument::*;
use crate::logic::authorization::*;
use crate::logic::debate::creation::*;
use crate::logic::debate::get_debate;
use crate::logic::debate::*;
use crate::logic::invitation::*;
use crate::logic::tokens::auth_token::*;
use crate::logic::vote::*;

#[derive(Debug)]
pub struct AuthenticatedMutation {
    pub auth_context: AuthContext,
}

#[derive(Debug)]
struct AddArgumentChangeSet {
    arguments_list: query::debate::ArgumentsList,
}

#[juniper::object(Context = Context)]
impl AddArgumentChangeSet {
    fn arguments_list(context: &Context) -> &query::debate::ArgumentsList {
        &self.arguments_list
    }
}

#[derive(Debug)]
struct AddAnswerChangeSet {
    answers_list: query::debate::AnswersList,
}

#[juniper::object(Context = Context)]
impl AddAnswerChangeSet {
    fn answers_list(context: &Context) -> &query::debate::AnswersList {
        &self.answers_list
    }
}

#[derive(Debug)]
struct CreateSubDebateChangeSet {
    sub_debates_list: Option<SubDebatesList>,
    debates_about_list: Option<DebatesAboutList>,
}

#[juniper::object(Context = Context)]
impl CreateSubDebateChangeSet {
    fn sub_debates_list(context: &Context) -> &Option<SubDebatesList> {
        &self.sub_debates_list
    }

    fn debates_about_list(context: &Context) -> &Option<DebatesAboutList> {
        &self.debates_about_list
    }
}

impl CreateSubDebateChangeSet {
    fn new(
        context: &Context,
        auth_context: &AuthContext,
        created_debate: &Debate,
    ) -> FieldResult<CreateSubDebateChangeSet> {
        let concerned_argument =
            get_concerned_argument_of_debate(context.db.as_ref(), auth_context, created_debate)?;
        let sub_debates = concerned_argument
            .as_ref()
            .map(|concerned_argument| {
                let super_debate = get_debate_of_argument(
                    context.db.as_ref(),
                    auth_context,
                    &concerned_argument.inner,
                )?
                .inner;
                get_sub_debates(context, auth_context, &super_debate)
            })
            .transpose()?;
        let debates_about = concerned_argument
            .as_ref()
            .map(|concerned_argument| {
                get_debates_about(context, auth_context, &concerned_argument.inner)
            })
            .transpose()?;
        Ok(CreateSubDebateChangeSet {
            sub_debates_list: sub_debates,
            debates_about_list: debates_about,
        })
    }
}

struct AcceptInvitationChangeSet {
    invitations: InvitationsList,
    top_level_debates: TopLevelDebatesList,
}

#[juniper::object(Context = Context)]
impl AcceptInvitationChangeSet {
    fn invitations(context: &Context) -> &InvitationsList {
        &self.invitations
    }

    fn top_level_debates(context: &Context) -> &TopLevelDebatesList {
        &self.top_level_debates
    }
}

impl AcceptInvitationChangeSet {
    fn new(
        context: &Context,
        auth_context: &AuthContext,
    ) -> FieldResult<AcceptInvitationChangeSet> {
        let top_level_debates = get_top_level_debates(auth_context, context)?;
        let invitations = get_invitations(context, auth_context)?;
        Ok(AcceptInvitationChangeSet {
            top_level_debates,
            invitations,
        })
    }
}

graphql_result!(CastVoteResult, VoteCast, VotingError);

struct PrepareVoteChangeset {
    cached_prepared_ballot: CachedPreparedBallot,
    arguments_considered: Vec<ArgumentConsidered>,
    review_required: ReviewRequired,
}

#[juniper::object(Context = Context)]
impl PrepareVoteChangeset {
    fn cached_prepared_ballot() -> &CachedPreparedBallot {
        &self.cached_prepared_ballot
    }

    fn arguments_considered() -> &Vec<ArgumentConsidered> {
        &self.arguments_considered
    }

    fn review_required() -> &ReviewRequired {
        &self.review_required
    }
}

graphql_result!(SetPasswordResult, AccountHasPassword, SetPasswordError);

#[juniper::object(Context = Context)]
impl AuthenticatedMutation {
    fn create_top_level_debate(
        context: &Context,
        debate: TopLevelDebateInput,
    ) -> FieldResult<TopLevelDebatesList> {
        create_top_level_debate(
            context.db.as_ref(),
            context.email_sender.as_ref(),
            &self.auth_context,
            debate,
        )?;
        let top_level_debates = get_top_level_debates(&self.auth_context, context)?;
        Ok(top_level_debates)
    }

    fn create_special_sub_debate(
        context: &Context,
        debate: SpecialSubDebateInput,
    ) -> FieldResult<CreateSubDebateChangeSet> {
        let created_debate =
            create_special_sub_debate(context.db.as_ref(), &self.auth_context, debate)?;
        let change_set =
            CreateSubDebateChangeSet::new(context, &self.auth_context, &created_debate)?;
        Ok(change_set)
    }

    fn add_argument(
        context: &Context,
        argument: ArgumentInput,
    ) -> FieldResult<AddArgumentChangeSet> {
        let added_argument = add_argument(context.db.as_ref(), &self.auth_context, &argument)?;
        let debate = get_debate(context.db.as_ref(), &self.auth_context, argument.debate_id)?;
        let arguments_list =
            query::debate::get_arguments(&debate, &added_argument.auth_context, context)?;
        Ok(AddArgumentChangeSet { arguments_list })
    }

    fn add_answer(context: &Context, answer: AnswerInput) -> FieldResult<AddAnswerChangeSet> {
        let debate = get_debate(context.db.as_ref(), &self.auth_context, answer.debate_id)?;
        let added_answer = add_answer_to_debate(
            context.db.as_ref(),
            &self.auth_context,
            &self.auth_context.account(),
            answer,
        )?;
        let answers_list =
            query::debate::get_answers(&debate, &added_answer.auth_context, context)?;
        Ok(AddAnswerChangeSet { answers_list })
    }

    fn cast_vote(context: &Context, new_vote: VoteInput) -> FieldResult<CastVoteResult> {
        Ok(vote(context.db.as_ref(), &self.auth_context, &new_vote)
            .into_field_result()?
            .map(|vote_cast| VoteCast {
                id: new_vote.debate_id,
                vote_cast,
            })
            .into())
    }

    fn prepare_vote(context: &Context, new_vote: VoteInput) -> FieldResult<PrepareVoteChangeset> {
        let prepared_ballot = prepare_vote(context.db.as_ref(), &self.auth_context, &new_vote)?;
        let debate = get_debate(context.db.as_ref(), &self.auth_context, new_vote.debate_id)?;
        let cached_prepared_ballot =
            get_cached_prepared_ballot(context, &self.auth_context, &debate)?;
        let arguments_considered =
            get_arguments_of_debate(context.db.as_ref(), &self.auth_context, &debate)?
                .iter()
                .map(|argument| get_argument_considered(context, argument, &self.auth_context))
                .collect::<Result<Vec<_>, _>>()?;
        let review_required = get_review_required(context, &self.auth_context, &debate)?;
        Ok(PrepareVoteChangeset {
            cached_prepared_ballot,
            arguments_considered,
            review_required,
        })
    }

    fn change_name(context: &Context, new_name: String) -> FieldResult<WithAuthContext<Account>> {
        let account = change_name_of_account(context.db.as_ref(), &self.auth_context, new_name)?;
        Ok(self.auth_context.with(account))
    }

    fn log_out(context: &Context) -> LogOutResult {
        log_out(context.db.as_ref(), &self.auth_context)
    }

    fn set_argument_weights(
        context: &Context,
        weights: Vec<ArgumentWeightInput>,
    ) -> FieldResult<Vec<WithAuthContext<Argument>>> {
        let arguments = save_argument_weights(context.db.as_ref(), &self.auth_context, &weights)?
            .into_iter()
            .map(|(argument, _)| self.auth_context.with(argument))
            .collect();
        Ok(arguments)
    }

    fn decline_invitation(context: &Context, id: Uuid) -> FieldResult<InvitationsList> {
        decline_invitation(context.db.as_ref(), &self.auth_context, id)?;
        get_invitations(context, &self.auth_context)
    }

    fn accept_invitation(context: &Context, id: Uuid) -> FieldResult<AcceptInvitationChangeSet> {
        accept_invitation(context.db.as_ref(), &self.auth_context, id)?;
        AcceptInvitationChangeSet::new(context, &self.auth_context)
    }

    fn set_password(context: &Context, password: String) -> FieldResult<SetPasswordResult> {
        Ok(
            set_password(context.db.as_ref(), &self.auth_context, &password)
                .into_field_result()?
                .map(|_| AccountHasPassword {
                    has_password: true,
                    ..get_account_has_password(self.auth_context.account())
                })
                .into(),
        )
    }

    fn delete_account(context: &Context, password: String) -> FieldResult<String> {
        mark_as_deleted(
            context.db.as_ref(),
            &self.auth_context,
            &self.auth_context.account(),
            &password,
        )?;
        Ok(String::new())
    }

    fn change_account_locale(context: &Context, new_locale: GQLLocale) -> FieldResult<GQLLocale> {
        change_locale(
            context.db.as_ref(),
            &self.auth_context,
            self.auth_context.account(),
            new_locale.into(),
        )?;
        Ok(new_locale)
    }
}
