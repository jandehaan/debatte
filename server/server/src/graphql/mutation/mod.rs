pub mod authenticated_mutation;

use juniper::FieldResult;

use crate::error::public::*;
use crate::graphql::context::*;
use crate::graphql::mutation::authenticated_mutation::*;
use crate::graphql::query::enums::*;
use crate::logic::account::*;
use crate::logic::authorization::*;
use crate::logic::invite_link::*;
use crate::logic::login_link::*;
use crate::logic::public_invite_link::*;
use crate::logic::tokens::auth_token::*;
use crate::logic::verify_email_link::*;
use db_access::models::{accounts::*, types::Locale};

#[derive(Debug)]
pub struct Mutation;

graphql_result!(
    AuthenticatedMutationResult,
    AuthenticatedMutation,
    TokenAuthError
);
graphql_result!(LoginResult, String, LoginError);
graphql_result!(
    CreateAccountResult,
    WithAuthContext<Account>,
    AccountCreationError
);
graphql_result!(
    VerifyEmailResult,
    WithAuthContext<OtherAccount>,
    VerifyEmailError
);
graphql_result!(ResendVerificationEmailResult, String, ResendEmailError);
graphql_result!(SendLoginEmailResult, String, SendLoginEmailError);
graphql_result!(
    SendResetPasswordEmailResult,
    String,
    SendResetPasswordEmailError
);
graphql_result!(ChangePasswordResult, String, ChangePasswordError);
graphql_result!(UseInviteLinkResult, InviteeInfo, UseInviteLinkError);
graphql_result!(
    ChangeInviteLinkEmailResult,
    String,
    ChangeInviteLinkEmailError
);
graphql_result!(LoginWithLinkResult, String, TokenAuthError);
graphql_result!(
    UsePublicInviteLinkResult,
    UsePublicInviteLinkAction,
    UsePublicInviteLinkError
);

#[juniper::object(Context = Context)]
impl Mutation {
    /// This field is used to perform a login using a user’s email and password.
    /// It returns an authentication token that can be used to access other fields.
    fn login(context: &Context, email: String, password: String) -> FieldResult<LoginResult> {
        login_with_password(context.db.as_ref(), &email, &password)
            .map(|token| token.to_str())
            .into_field_result()
            .map(|r| r.into())
    }

    fn login_with_link(context: &Context, token: String) -> FieldResult<LoginWithLinkResult> {
        Ok(login_with_link(context.db.as_ref(), &token)
            .into_field_result()?
            .map(|token| token.to_str())
            .into())
    }

    fn send_login_email(context: &Context, email: String) -> FieldResult<SendLoginEmailResult> {
        Ok(
            send_login_email(context.db.as_ref(), context.email_sender.as_ref(), &email)
                .into_field_result()?
                .into(),
        )
    }

    /// Creates a new account.
    /// If a password is provided, the account will be password-protected,
    /// otherwise, the user will be sent an email every time they want to log in.
    ///
    /// The password has to be 12 bytes long when UTF-8 encoded.
    fn create_account(
        context: &Context,
        email: String,
        name: String,
        password: String,
        locale: GQLLocale,
    ) -> FieldResult<CreateAccountResult> {
        Ok(create_regular_account(
            context.db.as_ref(),
            context.email_sender.as_ref(),
            email,
            name,
            password,
            locale.into(),
        )
        .into_field_result()?
        .map(|account| AuthContext::no_account().with(account))
        .into())
    }

    /// Marks an email as verified by checking whether a token sent to the user as a link is valid.
    fn verify_email(context: &Context, token: String) -> FieldResult<VerifyEmailResult> {
        Ok(verify_email(context.db.as_ref(), &token)
            .into_field_result()?
            .map(|account| AuthContext::no_account().with(account))
            .into())
    }

    fn authenticated_mutation(
        context: &Context,
        token: String,
    ) -> FieldResult<AuthenticatedMutationResult> {
        Ok(AuthContext::from_token(context.db.as_ref(), &token)
            .into_field_result()?
            .map(|auth_context| AuthenticatedMutation { auth_context })
            .into())
    }

    fn resend_verification_email(
        context: &Context,
        email: String,
        password: String,
    ) -> FieldResult<ResendVerificationEmailResult> {
        Ok(
            login_once_with_password(context.db.as_ref(), &email, &password)
                .map_err(|e| {
                    e.map_inner(|e| match e {
                        LoginError::InvalidEmailOrPassword => {
                            Some(ResendEmailError::InvalidEmailOrPassword)
                        }
                        _ => None,
                    })
                })
                .and_then(|auth_context| {
                    resend_verification_email(
                        context.db.as_ref(),
                        context.email_sender.as_ref(),
                        &auth_context,
                    )
                    .map(|s| s.to_owned())
                })
                .into_field_result()?
                .into(),
        )
    }

    fn send_reset_password_email(
        context: &Context,
        email: String,
        locale: GQLLocale,
    ) -> FieldResult<SendResetPasswordEmailResult> {
        Ok(send_reset_password_email(
            context.db.as_ref(),
            context.email_sender.as_ref(),
            email,
            locale.into(),
        )
        .into_field_result()?
        .into())
    }

    fn change_password(
        context: &Context,
        token: String,
        new_password: String,
    ) -> FieldResult<ChangePasswordResult> {
        Ok(change_password(context.db.as_ref(), &token, &new_password)
            .into_field_result()?
            .into())
    }

    fn use_invite_link(
        context: &Context,
        token: String,
        name: String,
        locale: GQLLocale,
    ) -> FieldResult<UseInviteLinkResult> {
        Ok(
            use_invite_link(context.db.as_ref(), &token, name, locale.into())
                .into_field_result()?
                .into(),
        )
    }

    fn change_invite_link_email(
        context: &Context,
        token: String,
        new_email: String,
        locale: GQLLocale,
    ) -> FieldResult<ChangeInviteLinkEmailResult> {
        Ok(change_invite_link_email(
            context.db.as_ref(),
            context.email_sender.as_ref(),
            &token,
            new_email,
            locale.into(),
        )
        .into_field_result()?
        .into())
    }

    fn use_public_invite_link(
        context: &Context,
        token: String,
        email: String,
        locale: GQLLocale,
    ) -> FieldResult<UsePublicInviteLinkResult> {
        Ok(use_public_invite_link(
            context.db.as_ref(),
            context.email_sender.as_ref(),
            &token,
            &email,
            locale.into(),
        )
        .into_field_result()?
        .into())
    }

    fn create_temporary_account(context: &Context, locale: GQLLocale) -> FieldResult<String> {
        let account = create_temporary_account(context.db.as_ref(), Locale::from(locale))?;
        let token = login_to_temporary_account(context.db.as_ref(), account.external_id)?;
        Ok(token.to_str())
    }
}
