use failure::{Backtrace, Context, Fail};
use std::fmt;

#[derive(Debug)]
pub struct RetrievalError {
    inner: Context<RetrievalErrorKind>,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum RetrievalErrorKind {
    #[fail(display = "not found")]
    NotFound,
    #[fail(display = "unauthorized")]
    Unauthorized,
    #[fail(display = "unauthorized")]
    Other,
}

impl Fail for RetrievalError {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for RetrievalError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl RetrievalError {
    pub fn kind(&self) -> RetrievalErrorKind {
        *self.inner.get_context()
    }
}

impl From<RetrievalErrorKind> for RetrievalError {
    fn from(kind: RetrievalErrorKind) -> RetrievalError {
        RetrievalError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<RetrievalErrorKind>> for RetrievalError {
    fn from(inner: Context<RetrievalErrorKind>) -> RetrievalError {
        RetrievalError { inner }
    }
}
