mod creation;
mod retrieval;

pub mod public;

pub use creation::CreationError;
pub use failure::{Error, ResultExt};
pub use retrieval::{RetrievalError, RetrievalErrorKind};
