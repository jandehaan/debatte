use failure::{Backtrace, Context, Fail};
use std::fmt;

pub use failure::ResultExt;

#[derive(Debug)]
pub struct PublicError<E: Fail> {
    inner: Context<PublicErrorKind<E>>,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum PublicErrorKind<E: Fail> {
    #[fail(display = "{}", _0)]
    Public(E),
    #[fail(display = "an error occurred")]
    Other,
}

impl<E: Fail> Fail for PublicError<E> {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl<E: Fail> fmt::Display for PublicError<E> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl<E: Fail + Copy> PublicError<E> {
    pub fn kind(&self) -> PublicErrorKind<E> {
        *self.inner.get_context()
    }

    pub fn map_inner<E2: Fail, F: FnOnce(E) -> Option<E2>>(self, f: F) -> PublicError<E2> {
        match self.inner.get_context() {
            PublicErrorKind::Public(e) => match f(*e) {
                Some(e2) => PublicError {
                    inner: self.inner.map(|_| PublicErrorKind::Public(e2)),
                },
                None => format_err!("making inner error non-public")
                    .context(PublicErrorKind::Other)
                    .into(),
            },
            PublicErrorKind::Other => PublicError {
                inner: self.inner.map(|_| PublicErrorKind::Other),
            },
        }
    }
}

pub trait IntoFieldResultExt<T, E: Fail>
where
    Self: Sized,
{
    fn into_field_result(self) -> juniper::FieldResult<Result<T, E>>;
}

impl<T, E: Fail + Copy> IntoFieldResultExt<T, E> for Result<T, PublicError<E>> {
    fn into_field_result(self) -> juniper::FieldResult<Result<T, E>> {
        match self {
            Ok(x) => Ok(Ok(x)),
            Err(public_e) => match public_e.kind() {
                PublicErrorKind::Public(e) => Ok(Err(e)),
                PublicErrorKind::Other => {
                    eprintln!("{:?}", public_e);
                    Err(format!("{}", public_e).into())
                }
            },
        }
    }
}

impl<E: Fail> From<PublicErrorKind<E>> for PublicError<E> {
    fn from(kind: PublicErrorKind<E>) -> PublicError<E> {
        PublicError {
            inner: Context::new(kind),
        }
    }
}

impl<E: Fail> From<Context<PublicErrorKind<E>>> for PublicError<E> {
    fn from(inner: Context<PublicErrorKind<E>>) -> PublicError<E> {
        PublicError { inner }
    }
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum ResendEmailError {
    #[fail(display = "rate-limited")]
    RateLimited,
    #[fail(display = "invalid email or password")]
    InvalidEmailOrPassword,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum LoginError {
    #[fail(display = "invalid email or password")]
    InvalidEmailOrPassword,
    #[fail(display = "quickly created account")]
    QuicklyCreatedAccount,
    #[fail(display = "email not verified")]
    EmailNotVerified,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum AccountCreationError {
    #[fail(display = "invalid email")]
    InvalidEmail,
    #[fail(display = "email in use")]
    EmailInUse,
    #[fail(display = "weak password")]
    WeakPassword,
    #[fail(display = "missing name")]
    MissingName,
    #[fail(display = "cannot send email")]
    CannotSendEmail,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum VerifyEmailError {
    #[fail(display = "malformed token")]
    MalformedToken,
    #[fail(display = "invalid token")]
    InvalidToken,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum SendLoginEmailError {
    #[fail(display = "account does not exist")]
    AccountDoesNotExist,
    #[fail(display = "account has password")]
    AccountHasPassword,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum TokenAuthError {
    #[fail(display = "malformed token")]
    MalformedToken,
    #[fail(display = "invalid token")]
    InvalidToken,
    #[fail(display = "unknown account")]
    UnknownAccount,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum SendEmailError {
    #[fail(display = "email address invalid")]
    InvalidEmail,
    #[fail(display = "error while sending email")]
    Other,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum SendResetPasswordEmailError {
    #[fail(display = "account does not exist")]
    AccountDoesNotExist,
}

#[derive(Debug, juniper::GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum ChangePasswordError {
    #[fail(display = "malformed token")]
    MalformedToken,
    #[fail(display = "invalid token")]
    InvalidToken,
    #[fail(display = "expired token")]
    ExpiredToken,
    #[fail(display = "weak password")]
    WeakPassword,
}

#[derive(Debug, GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum UseInviteLinkError {
    #[fail(display = "invalid token")]
    InvalidToken,
    #[fail(display = "malformed token")]
    MalformedToken,
    #[fail(display = "email in use")]
    EmailInUse,
    #[fail(display = "cannot send email")]
    CannotSendEmail,
    #[fail(display = "invalid email")]
    InvalidEmail,
    #[fail(display = "missing name")]
    MissingName,
}

#[derive(Debug, GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum ChangeInviteLinkEmailError {
    #[fail(display = "invalid token")]
    InvalidToken,
    #[fail(display = "malformed token")]
    MalformedToken,
    #[fail(display = "cannot send email")]
    CannotSendEmail,
    #[fail(display = "invalid email")]
    InvalidEmail,
}

#[derive(Debug, GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum VotingError {
    #[fail(display = "already voted")]
    AlreadyVoted,
    #[fail(display = "unauthorized")]
    Unauthorized,
    #[fail(display = "invalid data")]
    InvalidData,
}

#[derive(Debug, GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum UsePublicInviteLinkError {
    #[fail(display = "invalid token")]
    InvalidToken,
    #[fail(display = "cannot send email")]
    CannotSendEmail,
    #[fail(display = "invalid email")]
    InvalidEmail,
    #[fail(display = "debate has already started")]
    DebateHasAlreadyStarted,
}

#[derive(Debug, GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum SetPasswordError {
    #[fail(display = "weak password")]
    WeakPassword,
    #[fail(display = "account has password")]
    AccountHasPassword,
}

#[derive(Debug, GraphQLEnum, Fail, Clone, Copy, PartialEq, Eq)]
pub enum GetSummaryLinksError {
    #[fail(display = "not ready yet")]
    NotReadyYet,
    #[fail(display = "debate in progress")]
    DebateInProgress,
}
