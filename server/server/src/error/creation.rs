use failure::Fail;

#[derive(Debug, Fail)]
pub enum CreationError {
    #[fail(display = "unauthorized")]
    Unauthorized,
    #[fail(display = "invalid input data")]
    InvalidData,
}
