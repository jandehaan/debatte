// For error-chain
#![recursion_limit = "1024"]

pub mod background_tasks;
pub mod email;
pub mod error;
pub mod extensions;
pub mod graphql;
pub mod locale;
pub mod logic;
pub mod markdown;
pub mod tex;

extern crate actix_cors;
extern crate actix_rt;
extern crate actix_web;
extern crate argon2rs;
extern crate askama;
extern crate askama_escape;
extern crate base64;
extern crate chrono;
extern crate chrono_tz;
extern crate duct;
extern crate env_logger;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate juniper;
extern crate lettre;
extern crate mime;
extern crate pulldown_cmark;
extern crate rand;
extern crate serde;
extern crate serde_json;
extern crate sha2;
extern crate tectonic;

use actix_cors::Cors;
use actix_web::cookie::Cookie;
use actix_web::dev::HttpResponseBuilder;
use actix_web::{
    http::header, http::StatusCode, middleware, web, App, HttpMessage, HttpResponse, HttpServer,
};
use juniper::http::{graphiql::graphiql_source, GraphQLRequest};
use serde::Deserialize;
use std::sync::Arc;
use uuid::Uuid;

use crate::email::*;
use crate::graphql::context::Context;
use crate::graphql::*;
use db_access::db::connection::*;
use db_access::db::Db;

#[actix_rt::main]
pub async fn main() -> Result<(), String> {
    std::env::set_var("RUST_LOG", "info");
    dotenv::dotenv().map_err(|_| "Could not load .env")?;
    env_logger::init();
    let database_url = std::env::var("DATABASE_URL")
        .map_err(|_| "Environment variable DATABASE_URL is not set".to_owned())?;
    let port =
        std::env::var("PORT").map_err(|_| "Environment variable PORT is not set".to_owned())?;
    let static_server_address = std::env::var("STATIC_SERVER_ADDRESS")
        .map_err(|_| "Environment variable STATIC_SERVER_ADDRESS is not set".to_owned())?;
    let develop = std::env::var("DEVELOP")
        .map_err(|_| "Environment variable DEVELOP is not set".to_owned())?
        == "true";
    let smtp_server_host = std::env::var("SMTP_SERVER_HOST")
        .map_err(|_| "Environment variable SMTP_SERVER_HOST is not set".to_owned())?;
    let smtp_server_port = std::env::var("SMTP_SERVER_PORT")
        .map_err(|_| "Environment variable SMTP_SERVER_PORT is not set".to_owned())?;
    let from_email_host = std::env::var("FROM_EMAIL_HOST")
        .map_err(|_| "Environment variable FROM_EMAIL_HOST is not set".to_owned())?;
    let bind_address = format!("localhost:{}", port);
    let api_server = std::env::var("API_SERVER")
        .map_err(|_| "Environment variable API_SERVER is not set".to_owned())?;

    let pool = ConnectionPool::new(&database_url)?;
    let email_sender = SmtpEmailSender {
        smtp_server_host,
        port: smtp_server_port,
        from_host: from_email_host,
        link_server: static_server_address.clone(),
        api_server,
    };
    let schema = Arc::new(graphql::Schema::new(
        graphql::query::Query,
        graphql::mutation::Mutation,
    ));

    let background_db_conn: Box<dyn Db> = Box::new(pool.get()?);
    let background_email_sender: Box<dyn EmailSender> = Box::new(email_sender.clone());
    actix_rt::spawn(poll_background_tasks(
        background_db_conn,
        background_email_sender,
    ));
    let bind_address_with_scheme = "http://".to_owned() + &bind_address;
    HttpServer::new(move || {
        let app = App::new()
            .data(pool.clone())
            .data(email_sender.clone())
            .data(schema.clone())
            .wrap(middleware::Logger::default())
            .wrap({
                let cors = Cors::new().allowed_origin(&static_server_address);
                let cors = if develop {
                    cors.allowed_origin(&bind_address_with_scheme)
                } else {
                    cors
                };
                cors.allowed_methods(vec!["GET", "POST"])
                    .allowed_headers(vec![
                        header::CONTENT_TYPE,
                        header::AUTHORIZATION,
                        header::COOKIE,
                        header::SET_COOKIE,
                    ])
                    .supports_credentials()
                    .max_age(3600)
                    .finish()
            })
            .service(web::resource("/graphql").route(web::post().to(graphql)))
            // Should use query parameters: /graphql/?=...
            .service(web::resource("/graphql").route(web::get().to(graphql)))
            .service(web::resource("/set-cookie").route(web::post().to(set_cookie)))
            .service(web::resource("/summary/pdf").route(web::get().to(get_pdf_summary)))
            .service(web::resource("/summary/json").route(web::get().to(get_json_summary)));

        if develop {
            app.service(web::resource("/graphiql").route(web::get().to(graphiql)))
        } else {
            app
        }
    })
    .bind(&bind_address)
    .map_err(|_| format!("Could not bind to {}", bind_address))?
    .run()
    .await
    .map_err(|_| "Cannot start server".to_owned())
}

fn graphiql() -> HttpResponse {
    let html = graphiql_source("/graphql");
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

async fn graphql(
    pool: web::Data<ConnectionPool>,
    email_sender: web::Data<SmtpEmailSender>,
    st: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> Result<HttpResponse, actix_web::Error> {
    web::block(move || -> Result<String, ()> {
        let db_conn = pool.get().map_err(|e| {
            eprintln!("{}", e);
        })?;
        let context = Context {
            db: Box::new(db_conn),
            email_sender: Box::new((**email_sender).clone()),
        };
        let res = data.execute(&st, &context);
        Ok(serde_json::to_string(&res).map_err(|_| ())?)
    })
    .await
    .map_err(actix_web::Error::from)
    .and_then(|user| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(user))
    })
}

#[derive(Debug, Deserialize)]
struct SetCookieData {
    auth_token: String,
}

const AUTH_COOKIE_NAME: &str = "auth_token";

async fn set_cookie(data: web::Json<SetCookieData>) -> HttpResponse {
    let develop = std::env::var("DEVELOP")
        .map(|value| value == "true")
        .unwrap_or(false);
    let cookie = Cookie::build(AUTH_COOKIE_NAME, data.auth_token.clone()).http_only(true);
    let cookie = if !develop {
        cookie.secure(true).finish()
    } else {
        cookie.finish()
    };
    HttpResponseBuilder::new(StatusCode::OK)
        .cookie(cookie)
        .finish()
}

#[derive(Debug, Deserialize)]
struct GetSummaryQuery {
    debate: String,
}

use crate::error::public::*;
use crate::logic::authorization::*;
use crate::logic::debate::*;
use db_access::models::debate_summaries::*;

async fn get_pdf_summary(
    pool: web::Data<ConnectionPool>,
    query: web::Query<GetSummaryQuery>,
    request: web::HttpRequest,
) -> HttpResponse {
    match get_summary(pool, query, request) {
        Ok((summary, filename)) => HttpResponseBuilder::new(StatusCode::OK)
            .set_header("Content-Type", "application/pdf")
            .set_header(
                "Content-Disposition",
                format!("attachment; filename=\"{}\"", filename),
            )
            .body(summary.pdf),
        Err(status) => HttpResponseBuilder::new(status).finish(),
    }
}

async fn get_json_summary(
    pool: web::Data<ConnectionPool>,
    query: web::Query<GetSummaryQuery>,
    request: web::HttpRequest,
) -> HttpResponse {
    match get_summary(pool, query, request) {
        Ok((summary, _)) => HttpResponseBuilder::new(StatusCode::OK)
            .set_header("Content-Type", "application/json")
            .body(summary.json),
        Err(status) => HttpResponseBuilder::new(status).finish(),
    }
}

fn get_summary(
    pool: web::Data<ConnectionPool>,
    query: web::Query<GetSummaryQuery>,
    request: web::HttpRequest,
) -> Result<(DebateSummary, String), StatusCode> {
    let debate_id = Uuid::parse_str(&query.debate).map_err(|_| StatusCode::BAD_REQUEST)?;
    let db: Box<dyn Db> = Box::new(pool.get().map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?);
    let auth_context = AuthContext::from_token(
        db.as_ref(),
        request
            .cookie(AUTH_COOKIE_NAME)
            .ok_or(StatusCode::BAD_REQUEST)?
            .value(),
    )
    .map_err(|e| match e.kind() {
        PublicErrorKind::Public(TokenAuthError::InvalidToken) => StatusCode::UNAUTHORIZED,
        PublicErrorKind::Public(_) => StatusCode::BAD_REQUEST,
        PublicErrorKind::Other => StatusCode::INTERNAL_SERVER_ERROR,
    })?;
    let debate = get_debate(db.as_ref(), &auth_context, debate_id)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    let summary = db
        .get_debate_summary_of_debate(debate.id)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    Ok((
        summary,
        format!(
            "{}: {}.pdf",
            locale::Locale::from(debate.locale).get(locale::Phrase::Summary),
            debate.question
        ),
    ))
}

async fn poll_background_tasks(db: Box<dyn Db>, email_sender: Box<dyn EmailSender>) {
    loop {
        std::thread::sleep(std::time::Duration::from_secs(1));
        match background_tasks::perform(db.as_ref(), email_sender.as_ref()) {
            Ok(_) => (),
            Err(e) => eprintln!("Error while performing background tasks: {:?}", e),
        }
    }
}
