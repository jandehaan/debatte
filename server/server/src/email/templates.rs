use askama::Template;

use crate::email::*;
use crate::locale::*;

#[derive(Debug, GraphQLInputObject)]
pub struct EmailInvitee {
    pub name: String,
    pub email: String,
}

pub trait EmailInput<'a> {
    fn to(&self) -> &str;
    fn pdf_attachment(&self) -> Option<(String, &[u8])> {
        None
    }
    type Subject;
    type Body;
}

pub trait EmailTemplate<'a, T: EmailInput<'a>>: Template {
    fn new(input: &'a T) -> Self;
}

pub fn create_email<'a, T: 'a + EmailInput<'a>>(input: &'a T) -> Email
where
    T::Subject: EmailTemplate<'a, T>,
    T::Body: EmailTemplate<'a, T>,
{
    Email {
        to: input.to().to_owned(),
        subject: T::Subject::new(input).render().unwrap(),
        body: T::Body::new(input).render().unwrap(),
        pdf_attachment: input
            .pdf_attachment()
            .map(|(name, bytes)| (name, Vec::from(bytes))),
    }
}

macro_rules! template_types {
    ($subject_template:expr, $body_template:expr) => {
        #[derive(Debug, Template)]
        #[template(path = $subject_template)]
        pub struct EmailSubjectInput<'a> {
            input: &'a EmailInput<'a>,
        }
        impl<'a> super::EmailTemplate<'a, EmailInput<'a>> for EmailSubjectInput<'a> {
            fn new(input: &'a EmailInput<'a>) -> EmailSubjectInput<'a> {
                EmailSubjectInput { input }
            }
        }
        #[derive(Debug, Template)]
        #[template(path = $body_template)]
        pub struct EmailBodyInput<'a> {
            input: &'a EmailInput<'a>,
        }
        impl<'a> super::EmailTemplate<'a, EmailInput<'a>> for EmailBodyInput<'a> {
            fn new(input: &'a EmailInput<'a>) -> EmailBodyInput<'a> {
                EmailBodyInput { input }
            }
        }
    };
}

mod filters {
    pub fn optional(x: &Option<String>) -> Result<String, askama::Error> {
        Ok(x.clone().unwrap_or_default())
    }
}

pub mod invitation {
    use super::filters;
    use super::{Locale, Template};
    use crate::logic::debate::Debate;

    template_types!("email/invitation.subject.txt", "email/invitation.txt");

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub invitee_name: &'a str,
        pub invitee_email: &'a str,
        pub debate: &'a Debate,
        pub inviting_user_name: &'a str,
        pub invite_link: &'a str,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.invitee_email
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}

pub mod verify {
    use super::{Locale, Template};
    use crate::logic::account::Account;

    template_types!("email/verify.subject.txt", "email/verify.txt");

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub account: &'a Account,
        pub verification_link: &'a str,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.account.email
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}

pub mod invitation_for_account {
    use super::filters;
    use super::{Locale, Template};
    use crate::logic::account::Account;
    use crate::logic::debate::Debate;

    template_types!(
        "email/invitation_for_account.subject.txt",
        "email/invitation_for_account.txt"
    );

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub account: &'a Account,
        pub debate: &'a Debate,
        pub link: &'a str,
        pub inviting_user_name: &'a str,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.account.email
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}

pub mod invitation_public {
    use super::filters;
    use super::{Locale, Template};
    use crate::logic::debate::Debate;

    template_types!(
        "email/invitation_public.subject.txt",
        "email/invitation_public.txt"
    );

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub debate: &'a Debate,
        pub invite_link: &'a str,
        pub invitee_email: &'a str,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.invitee_email
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}

pub mod login {
    use super::{Locale, Template};

    template_types!("email/login.subject.txt", "email/login.txt");

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub email: &'a str,
        pub login_link: &'a str,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.email
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}

pub mod reset_password {
    use super::{Locale, Template};
    use crate::logic::account::Account;

    template_types!(
        "email/reset_password.subject.txt",
        "email/reset_password.txt"
    );

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub account: &'a Account,
        pub reset_password_link: &'a str,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.account.email
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}

pub mod summary {
    use super::{Locale, Template};
    use crate::locale::Phrase;
    use crate::logic::account::Account;
    use crate::logic::debate::Debate;

    template_types!("email/summary.subject.txt", "email/summary.txt");

    #[derive(Debug)]
    pub struct EmailInput<'a> {
        pub locale: Locale,
        pub account: &'a Account,
        pub debate: &'a Debate,
        pub pdf_summary: &'a [u8],
        pub top_answer: Option<&'a str>,
    }

    impl<'a> super::EmailInput<'a> for EmailInput<'a> {
        fn to(&self) -> &str {
            &self.account.email
        }

        fn pdf_attachment(&self) -> Option<(String, &[u8])> {
            Some((
                format!(
                    "{}: {}",
                    self.locale.get(Phrase::Summary),
                    self.debate.question
                ),
                self.pdf_summary,
            ))
        }

        type Subject = EmailSubjectInput<'a>;
        type Body = EmailBodyInput<'a>;
    }
}
