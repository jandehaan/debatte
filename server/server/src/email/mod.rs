pub mod templates;

#[cfg(debug_assertions)]
use lettre::ClientSecurity;
#[cfg(not(debug_assertions))]
use lettre::ClientSecurity;
use lettre::{SmtpClient, Transport};

use crate::error::public::*;
use crate::error::*;

#[derive(Debug)]
pub struct Email {
    pub to: String,
    pub subject: String,
    pub body: String,
    pub pdf_attachment: Option<(String, Vec<u8>)>,
}

pub trait EmailSender {
    fn send(&self, email: Email) -> Result<(), PublicError<SendEmailError>>;
    fn get_link_server(&self) -> &str;
    fn get_api_server(&self) -> &str;
}

#[derive(Debug, Clone)]
pub struct SmtpEmailSender {
    pub smtp_server_host: String,
    pub port: String,
    pub from_host: String,
    pub link_server: String,
    pub api_server: String,
}

impl EmailSender for SmtpEmailSender {
    fn send(&self, email: Email) -> Result<(), PublicError<SendEmailError>> {
        self.send_from_local_part("eltrot", email)
    }

    fn get_link_server(&self) -> &str {
        &self.link_server
    }

    fn get_api_server(&self) -> &str {
        &self.api_server
    }
}

impl SmtpEmailSender {
    fn send_from_local_part(
        &self,
        local_part: &str,
        email_input: Email,
    ) -> Result<(), PublicError<SendEmailError>> {
        let email = lettre_email::Email::builder()
            .to(email_input.to)
            .from(format!("{}@{}", local_part, self.from_host))
            .subject(email_input.subject)
            .text(email_input.body);
        let email = match email_input.pdf_attachment {
            Some(pdf_attachment) => email
                .attachment(&pdf_attachment.1, &pdf_attachment.0, &mime::APPLICATION_PDF)
                .context(PublicErrorKind::Other)?,
            None => email,
        };
        let email = email
            .build()
            .map_err(|_| PublicErrorKind::Public(SendEmailError::InvalidEmail))?;
        let domain = self.smtp_server_host.clone() + ":" + &self.port;
        let mut smtp_client = new_smtp_client(&domain)
            .context(PublicErrorKind::Other)?
            .transport();
        smtp_client
            .send(email.into())
            .context(PublicErrorKind::Other)?;
        Ok(())
    }
}

#[cfg(not(debug_assertions))]
fn new_smtp_client(domain: &str) -> Result<SmtpClient, Error> {
    Ok(SmtpClient::new(&domain, ClientSecurity::None)?)
}

#[cfg(debug_assertions)]
fn new_smtp_client(domain: &str) -> Result<SmtpClient, Error> {
    Ok(SmtpClient::new(&domain, ClientSecurity::None)?)
}

#[derive(Debug, Clone)]
pub struct MockEmailSender;

impl EmailSender for MockEmailSender {
    fn send(&self, email: Email) -> Result<(), PublicError<SendEmailError>> {
        self.send_from_local_part("eltrot", email)
    }

    fn get_link_server(&self) -> &str {
        "http://example.com"
    }

    fn get_api_server(&self) -> &str {
        "http://example.com"
    }
}

impl MockEmailSender {
    fn send_from_local_part(
        &self,
        local_part: &str,
        email: Email,
    ) -> Result<(), PublicError<SendEmailError>> {
        println!(
            "\
Send email:

From: {}@mockemail.example.com
To: {}
Subject: {}

{}
\
        ",
            local_part, email.to, email.subject, email.body
        );
        Ok(())
    }
}
