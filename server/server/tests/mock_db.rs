mod db;

use db_access::db::mock::*;
use db_access::db::Db;

use db::*;

fn get_mock_db() -> Box<dyn Db> {
    let mock_db = MockDb::new();
    Box::new(mock_db)
}

#[test]
fn accounts_crud() {
    let mock_db = get_mock_db();
    accounts_crud_with_db(mock_db.as_ref());
}

#[test]
fn get_accounts_of_all_debates() {
    let mock_db = get_mock_db();
    get_accounts_of_all_debates_with_db(mock_db.as_ref())
}

#[test]
fn get_answers_of_debate() {
    let mock_db = get_mock_db();
    get_answers_of_debate_with_db(mock_db.as_ref())
}
