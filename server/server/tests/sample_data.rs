use db_access::db::Db;
use db_access::models;
use libeltrot::email::*;
use libeltrot::logic;

pub fn insert_sample_data(db: &dyn Db) -> Result<(), String> {
    let mock_email_sender = MockEmailSender;

    let names = vec![
        "Max Mustermann",
        "Felicia Vorlagenfrau",
        "Humberto Huss",
        "April Ammerman",
        "Gertie Guerra",
        "Violet Villines",
        "Cyrstal Carillo",
        "Susannah Sparano",
        "Magdalen Mcatee",
        "Marhta Mulero",
        "Tomoko Truby",
        "Tiffanie Torpey",
        "Dallas Dunkle",
        "Ivelisse Inge",
        "Tiara Tapley",
        "Ty Tawil",
        "Johnna Jo",
        "Treena Tyra",
        "Faye Frisina",
        "Lavenia Lent",
        "Isela Ishida",
        "Ruthie Robidoux",
    ];

    let accounts = names
        .iter()
        .map(|name| insert_name(db, name))
        .collect::<Result<Vec<_>, _>>()
        .map_err(|e| format!("{:?}", e))?;

    let auth_token = logic::tokens::auth_token::login_with_password(
        db,
        "maxmustermann@example.com",
        "longerpassword",
    )
    .map_err(|e| format!("{:?}", e))?;
    let auth_context =
        logic::authorization::AuthContext::from_token(db, &auth_token.to_str()).unwrap();

    let debate_input = logic::debate::creation::TopLevelDebateInput {
        known_invitees: accounts
            .into_iter()
            .filter(|account| account.name != "Max Mustermann")
            .map(|account| logic::debate::creation::OtherAccountInput {
                id: account.external_id,
            })
            .collect(),
        first_answers: vec![
            logic::debate::creation::FirstAnswerInput {
                answer: "Abiertour".to_owned(),
            },
            logic::debate::creation::FirstAnswerInput {
                answer: "SemipermeABIle Membran – nur die besten kommen durch".to_owned(),
            },
        ],
        question: "Was soll unser Abimotto sein?".to_owned(),
        details: "".to_owned(),
        timetable: logic::debate::creation::DebateTimetable {
            collecting_answers_start: chrono::DateTime::parse_from_rfc3339(
                "2019-06-24T17:00:00+02:00",
            )
            .map_err(|e| format!("{:?}", e))?
            .with_timezone(&chrono::Utc),
            debating_start: chrono::DateTime::parse_from_rfc3339("2019-06-30T17:00:00+02:00")
                .map_err(|e| format!("{:?}", e))?
                .with_timezone(&chrono::Utc),
            voting_start: chrono::DateTime::parse_from_rfc3339("2019-07-02T17:00:00+02:00")
                .map_err(|e| format!("{:?}", e))?
                .with_timezone(&chrono::Utc),
            voting_end: chrono::DateTime::parse_from_rfc3339("2019-07-04T17:00:00+02:00")
                .map_err(|e| format!("{:?}", e))?
                .with_timezone(&chrono::Utc),
        },
        email_invitees: Vec::new(),
        locale: libeltrot::graphql::query::enums::GQLLocale::EnUS,
        time_zone: "Europe/Berlin".to_owned(),
        create_public_invite_link: false,
    };
    let _debate = logic::debate::creation::create_top_level_debate(
        db,
        &mock_email_sender,
        &auth_context,
        debate_input,
    )
    .map_err(|e| format!("{:?}", e))?;

    Ok(())
}

fn insert_name(db: &dyn Db, name: &str) -> Result<models::accounts::Account, String> {
    let email = name
        .to_lowercase()
        .chars()
        .filter(|c| *c != ' ')
        .collect::<String>()
        + "@example.com";

    println!("{}", email);
    let email_sender = MockEmailSender;
    let account = logic::account::create_regular_account(
        db,
        &email_sender,
        email,
        name.to_owned(),
        "longerpassword".to_owned(),
        models::types::Locale::EnUS,
    )
    .map_err(|e| format!("{:?}", e))?;
    Ok(account)
}
