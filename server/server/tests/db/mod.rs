use chrono::prelude::*;
use uuid::Uuid;

use db_access::db::Db;
use db_access::models::accounts::*;
use db_access::models::answers::*;
use db_access::models::debates::*;
use db_access::models::participations::*;
use db_access::models::types::*;

#[derive(Debug)]
pub struct ExampleValues;

impl ExampleValues {
    pub fn new_account_1() -> NewAccount {
        NewAccount {
            external_id: Uuid::new_v4(),
            email: "some_mail@example.com".to_owned(),
            salt: Some("this_is_salt1".to_owned()),
            password_hash: Some("this_is_a_hash1".to_owned()),
            name: "ASome Name 1".to_owned(),
            email_verified: false,
            locale: Locale::EnUS,
            created: Utc::now(),
            temporary: false,
        }
    }

    pub fn new_account_2() -> NewAccount {
        NewAccount {
            external_id: Uuid::new_v4(),
            email: "another_mail@example.com".to_owned(),
            salt: Some("this_is_salt2".to_owned()),
            password_hash: Some("this_is_a_hash2".to_owned()),
            name: "BSome Name 2".to_owned(),
            email_verified: false,
            locale: Locale::EnUS,
            created: Utc::now(),
            temporary: false,
        }
    }

    pub fn new_account_3() -> NewAccount {
        NewAccount {
            external_id: Uuid::new_v4(),
            email: "a_different_mail@example.com".to_owned(),
            salt: Some("this_is_salt3".to_owned()),
            password_hash: Some("this_is_a_hash3".to_owned()),
            name: "CSome Name 3".to_owned(),
            locale: Locale::EnUS,
            email_verified: false,
            created: Utc::now(),
            temporary: false,
        }
    }

    pub fn new_debate() -> NewDebate {
        NewDebate {
            external_id: Uuid::new_v4(),
            question: "Question?".to_owned(),
            details: Some("".to_owned()),
            allow_adding_more_answers: false,
            collecting_answers_start: Some(Utc::now()),
            debating_start: Some(Utc::now()),
            voting_start: Some(Utc::now()),
            voting_end: Some(Utc::now()),
            concerned_argument: None,
            kind: DebateKind::Other,
            creation_time: Utc::now(),
            locale: Locale::EnUS,
            time_zone: "Europe/Berlin".to_owned(),
            public_invite_link_token: None,
        }
    }
}

pub fn accounts_crud_with_db(db: &dyn Db) {
    let new_account = ExampleValues::new_account_1();
    let account = db.insert_account(new_account.clone()).unwrap();

    assert_eq!(account.external_id, new_account.external_id);
    assert_eq!(account.email, new_account.email);
    assert_eq!(account.salt, new_account.salt);
    assert_eq!(account.password_hash, new_account.password_hash);
    assert_eq!(account.name, new_account.name);

    let account_with_id = db.get_account_with_id(account.id).unwrap().unwrap();
    assert_eq!(account.external_id, account_with_id.external_id);

    let account_with_email = db.get_account_with_email(&new_account.email).unwrap();
    assert_eq!(account.id, account_with_email.id);

    let account_with_external_id = db
        .get_account_with_external_id(&new_account.external_id)
        .unwrap()
        .unwrap();
    assert_eq!(account.id, account_with_external_id.id);
}

pub fn get_accounts_of_all_debates_with_db(db: &dyn Db) {
    let main_account = db.insert_account(ExampleValues::new_account_1()).unwrap();
    let known_account = db.insert_account(ExampleValues::new_account_2()).unwrap();
    let _unknown_account = db.insert_account(ExampleValues::new_account_3()).unwrap();

    let debate = db.insert_debate(&ExampleValues::new_debate()).unwrap();

    let main_account_participation = NewParticipation {
        debate: debate.id,
        participant: main_account.id,
    };
    let known_account_participation = NewParticipation {
        debate: debate.id,
        participant: known_account.id,
    };
    db.insert_participations(&[main_account_participation, known_account_participation])
        .unwrap();

    let mut known_accounts = db.get_accounts_of_all_debates(main_account.id).unwrap();
    known_accounts.sort_by_key(|account| account.name.clone());
    println!("{:?}", known_accounts);
    assert_eq!(known_accounts.len(), 2);
    assert_eq!(
        known_accounts.get(0).unwrap().external_id,
        main_account.external_id
    );
    assert_eq!(
        known_accounts.get(1).unwrap().external_id,
        known_account.external_id
    );
}

pub fn get_answers_of_debate_with_db(db: &dyn Db) {
    let debate_1 = db.insert_debate(&ExampleValues::new_debate()).unwrap();
    let debate_2 = db.insert_debate(&ExampleValues::new_debate()).unwrap();
    let account = db.insert_account(ExampleValues::new_account_1()).unwrap();
    let new_answer_1 = NewAnswer {
        external_id: Uuid::new_v4(),
        answer: "an answer".to_owned(),
        debate: debate_1.id,
        author: account.id,
        creation_time: Utc::now(),
        semantics: AnswerSemantics::None,
    };
    let new_answer_2 = NewAnswer {
        external_id: Uuid::new_v4(),
        answer: "another answer".to_owned(),
        debate: debate_2.id,
        author: account.id,
        creation_time: Utc::now(),
        semantics: AnswerSemantics::None,
    };
    db.insert_answers(&[new_answer_1.clone(), new_answer_2])
        .unwrap();

    let answers_of_debate_1 = db.get_answers_of_debate(debate_1.id).unwrap();
    assert_eq!(answers_of_debate_1.len(), 1);
    assert_eq!(
        answers_of_debate_1.get(0).unwrap().external_id,
        new_answer_1.external_id
    );
}
