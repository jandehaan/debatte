extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate serial_test_derive;

mod db;

use std::env;

use dotenv::dotenv;
use serial_test_derive::serial;

use db_access::db::connection::*;
use db_access::db::Db;

use db::*;

embed_migrations!("../db_access/migrations");

fn establish_connection() -> DbConn {
    dotenv().ok();

    let db_url = env::var("DATABASE_TEST_URL").expect("DATABASE_URL must be set");
    let pool = ConnectionPool::new(&db_url).expect("Could not create connection pool");
    let pg_conn = pool.get().expect("Could not get connection from pool");
    embedded_migrations::run_with_output(pg_conn.inner(), &mut std::io::stdout())
        .expect("Error while running migrations");
    pg_conn
}

fn get_db() -> Box<dyn Db> {
    let conn = establish_connection();
    clean_sql_db(&conn);
    Box::new(conn)
}

#[serial]
#[test]
fn accounts_crud() {
    let db = get_db();
    accounts_crud_with_db(db.as_ref());
}

#[serial]
#[test]
fn get_accounts_of_all_debates() {
    let db = get_db();
    get_accounts_of_all_debates_with_db(db.as_ref());
}

#[serial]
#[test]
fn get_answers_of_debate() {
    let db = get_db();
    get_answers_of_debate_with_db(db.as_ref());
}
