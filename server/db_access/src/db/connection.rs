use chrono::{Duration, Utc};
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, PooledConnection};
use std::sync::Arc;
use uuid::Uuid;

use crate::db::{Db, QueryError, QueryResult};
use crate::models::accounts::*;
use crate::models::answers::*;
use crate::models::argument_weights::*;
use crate::models::arguments::*;
use crate::models::auth_tokens::*;
use crate::models::ballots::*;
use crate::models::debate_summaries::*;
use crate::models::debates::*;
use crate::models::invitations::*;
use crate::models::invite_links::*;
use crate::models::login_links::*;
use crate::models::partial_ballots::*;
use crate::models::participations::*;
use crate::models::password_resets::*;
use crate::models::supports::*;
use crate::models::verify_email_links::*;
use crate::models::votes::*;
use crate::schema;

pub struct DbConn(PooledConnection<ConnectionManager<diesel::PgConnection>>);

impl DbConn {
    pub fn inner(&self) -> &PooledConnection<ConnectionManager<diesel::PgConnection>> {
        &self.0
    }
}

#[derive(Clone)]
pub struct ConnectionPool(
    Arc<diesel::r2d2::Pool<diesel::r2d2::ConnectionManager<diesel::pg::PgConnection>>>,
);

impl ConnectionPool {
    pub fn new(database_url: &str) -> Result<ConnectionPool, String> {
        let manager = diesel::r2d2::ConnectionManager::<diesel::PgConnection>::new(database_url);
        let pool = Arc::new(
            diesel::r2d2::Pool::new(manager).map_err(|_| "Could not create connection pool")?,
        );
        Ok(ConnectionPool(pool))
    }

    pub fn get(&self) -> Result<DbConn, String> {
        Ok(DbConn(self.0.get().map_err(|e| format!("{:?}", e))?))
    }
}

pub fn clean_sql_db(conn: &DbConn) {
    vec![
        r#"delete from "answers""#,
        r#"delete from "participations""#,
        r#"delete from "accounts""#,
        r#"delete from "debates""#,
    ]
    .iter()
    .map(|s| diesel::sql_query(s.to_owned()))
    .map(|q| q.execute(&conn.0).expect("Could not clean DB"))
    .for_each(drop);
}

impl Db for DbConn {
    fn insert_account(&self, account: NewAccount) -> QueryResult<Account> {
        let account = vec![NewRawAccount::from(account)]
            .insert_into(schema::accounts::table)
            .get_result::<RawAccount>(&self.0)?;
        Ok(Account::try_from(account).unwrap())
    }

    fn update_account(&self, account: Account) -> QueryResult<Account> {
        let account = RawAccount::from(account);
        let account = diesel::update(&account)
            .set(&account)
            .get_result::<RawAccount>(&self.0)?;
        Ok(Account::try_from(account).unwrap())
    }

    fn get_account_with_email(&self, email: &str) -> QueryResult<Account> {
        let account = schema::accounts::table
            .filter(schema::accounts::email.eq(email))
            .get_result::<RawAccount>(&self.0)?;
        Account::try_from(account).map_err(|_| QueryError::NotFound)
    }

    fn get_account_with_external_id(
        &self,
        external_id: &Uuid,
    ) -> QueryResult<Result<Account, OtherAccount>> {
        let account = schema::accounts::table
            .filter(schema::accounts::external_id.eq(external_id))
            .get_result::<RawAccount>(&self.0)?;
        Ok(Account::try_from(account).map_err(OtherAccount::from))
    }

    fn get_account_with_id(&self, id: i64) -> QueryResult<Result<Account, OtherAccount>> {
        let account = schema::accounts::table
            .filter(schema::accounts::id.eq(id))
            .get_result::<RawAccount>(&self.0)?;
        Ok(Account::try_from(account).map_err(OtherAccount::from))
    }

    fn get_accounts_of_all_debates(&self, account_id: i64) -> QueryResult<Vec<OtherAccount>> {
        // A raw SQL query is needed here because Diesel does not support self joins.
        // String interpolation is OK in this case because the interpolated string will only ever be an integer.
        let query = diesel::sql_query(format!(
            r#"
        select distinct *
            from
                "accounts",
                (select "participations"."participant"
                    from
                        "participations",
                        (select "participations"."debate"
                            from "participations"
                            where "participations"."participant" = {})
                        as "own_debates"
                    where "participations"."debate" = "own_debates"."debate")
                as "known_participants"
            where "accounts"."id" = "known_participants"."participant"        
        "#,
            account_id
        ));
        let accounts = query.get_results(&self.0)?;
        let other_accounts = accounts
            .into_iter()
            .map(|account: RawAccount| OtherAccount {
                external_id: account.external_id,
                name: account.name,
            })
            .collect::<Vec<OtherAccount>>();
        Ok(other_accounts)
    }

    fn get_answers_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Answer>> {
        let answers = schema::answers::table
            .filter(schema::answers::debate.eq(debate_id))
            .select(schema::answers::all_columns)
            .get_results(&self.0)?;
        Ok(answers)
    }

    fn get_supporting_arguments_of_answer(&self, answer_id: i64) -> QueryResult<Vec<Argument>> {
        let arguments = schema::supports::table
            .filter(schema::supports::answer.eq(answer_id))
            .inner_join(schema::arguments::table)
            .filter(schema::supports::argument.eq(schema::arguments::id))
            .select(schema::arguments::all_columns)
            .get_results(&self.0)?;
        Ok(arguments)
    }

    fn get_answer_with_external_id(&self, external_id: Uuid) -> QueryResult<Answer> {
        let answer = schema::answers::table
            .filter(schema::answers::external_id.eq(external_id))
            .get_result::<Answer>(&self.0)?;
        Ok(answer)
    }

    fn get_answer_with_id(&self, answer_id: i64) -> QueryResult<Answer> {
        let answer = schema::answers::table
            .filter(schema::answers::id.eq(answer_id))
            .get_result(&self.0)?;
        Ok(answer)
    }

    fn insert_answers(&self, answers: &[NewAnswer]) -> QueryResult<Vec<Answer>> {
        let answers = diesel::insert_into(schema::answers::table)
            .values(answers)
            .get_results(&self.0)?;
        Ok(answers)
    }

    fn get_argument_with_id(&self, id: i64) -> QueryResult<Argument> {
        let argument = schema::arguments::table
            .filter(schema::arguments::id.eq(id))
            .get_result::<Argument>(&self.0)?;
        Ok(argument)
    }

    fn get_arguments_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Argument>> {
        let arguments = schema::arguments::table
            .inner_join(schema::debates::table)
            .filter(schema::arguments::debate.eq(debate_id))
            .select(schema::arguments::all_columns)
            .get_results(&self.0)?;
        Ok(arguments)
    }

    fn get_supported_answers_of_argument(&self, argument_id: i64) -> QueryResult<Vec<Answer>> {
        let answers = schema::supports::table
            .filter(schema::supports::argument.eq(argument_id))
            .inner_join(schema::answers::table)
            .filter(schema::supports::answer.eq(schema::answers::id))
            .select(schema::answers::all_columns)
            .get_results(&self.0)?;
        Ok(answers)
    }

    fn insert_supports(&self, supports: &[NewSupport]) -> QueryResult<Vec<Support>> {
        let supports = diesel::insert_into(schema::supports::table)
            .values(supports)
            .get_results(&self.0)?;
        Ok(supports)
    }

    fn get_debate_of_argument(&self, argument_id: i64) -> QueryResult<Debate> {
        let debate = schema::arguments::table
            .filter(schema::arguments::id.eq(argument_id))
            .inner_join(schema::debates::table)
            .select(schema::debates::all_columns)
            .get_result(&self.0)?;
        Ok(debate)
    }

    fn get_argument_with_external_id(&self, external_id: Uuid) -> QueryResult<Argument> {
        let argument = schema::arguments::table
            .filter(schema::arguments::external_id.eq(external_id))
            .get_result::<Argument>(&self.0)?;
        Ok(argument)
    }

    fn insert_argument(&self, argument: &NewArgument) -> QueryResult<Argument> {
        let argument = vec![argument]
            .insert_into(schema::arguments::table)
            .get_result::<Argument>(&self.0)?;
        Ok(argument)
    }

    fn insert_auth_token(&self, auth_token: &NewAuthToken) -> QueryResult<AuthToken> {
        let auth_token = vec![auth_token]
            .insert_into(schema::auth_tokens::table)
            .get_result::<AuthToken>(&self.0)?;
        Ok(auth_token)
    }

    fn get_auth_token_with_hash(&self, auth_token_hash: &str) -> QueryResult<AuthToken> {
        let auth_token = schema::auth_tokens::table
            .filter(schema::auth_tokens::auth_token_hash.eq(auth_token_hash))
            .get_result::<AuthToken>(&self.0)?;
        Ok(auth_token)
    }

    fn delete_auth_tokens_of_account(&self, account_id: i64) -> QueryResult<()> {
        diesel::delete(
            schema::auth_tokens::table.filter(schema::auth_tokens::account_id.eq(account_id)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn get_ballots_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Ballot>> {
        let ballots = schema::ballots::table
            .filter(schema::ballots::debate.eq(debate_id))
            .get_results(&self.0)?;
        Ok(ballots)
    }

    fn get_ballot_of_voter_of_debate(&self, debate_id: i64, voter_id: i64) -> QueryResult<Ballot> {
        let ballot = schema::ballots::table
            .filter(
                schema::ballots::debate
                    .eq(debate_id)
                    .and(schema::ballots::voter.eq(voter_id)),
            )
            .get_result(&self.0)?;
        Ok(ballot)
    }

    fn insert_ballot(&self, ballot: NewBallot) -> QueryResult<Ballot> {
        let ballot = vec![ballot]
            .insert_into(schema::ballots::table)
            .get_result(&self.0)?;
        Ok(ballot)
    }

    fn update_ballot(&self, ballot: Ballot) -> QueryResult<Ballot> {
        let ballot = diesel::update(&ballot).set(&ballot).get_result(&self.0)?;
        Ok(ballot)
    }

    fn get_debate_with_id(&self, id: i64) -> QueryResult<Debate> {
        let debate = schema::debates::table
            .filter(schema::debates::id.eq(id))
            .get_result::<Debate>(&self.0)?;
        Ok(debate)
    }

    fn get_debate_with_external_id(&self, external_id: Uuid) -> QueryResult<Debate> {
        let debate = schema::debates::table
            .filter(schema::debates::external_id.eq(external_id))
            .get_result::<Debate>(&self.0)?;
        Ok(debate)
    }

    fn get_top_level_debates_of_participant(
        &self,
        participant_id: i64,
    ) -> QueryResult<Vec<Debate>> {
        let debates = schema::participations::table
            .filter(schema::participations::participant.eq(participant_id))
            .inner_join(schema::debates::table)
            .filter(schema::debates::id.eq(schema::participations::debate))
            .filter(schema::debates::concerned_argument.is_null())
            .select(schema::debates::all_columns)
            .get_results(&self.0)?;
        Ok(debates)
    }

    fn get_debates_about_argument(&self, argument_id: i64) -> QueryResult<Vec<Debate>> {
        let debates = schema::debates::table
            .filter(schema::debates::concerned_argument.eq(argument_id))
            .get_results(&self.0)?;
        Ok(debates)
    }

    fn get_sub_debates_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Debate>> {
        let debates = schema::arguments::table
            .filter(schema::arguments::debate.eq(debate_id))
            .inner_join(
                schema::debates::table.on(schema::arguments::id
                    .nullable()
                    .eq(schema::debates::concerned_argument)),
            )
            .select(schema::debates::all_columns)
            .get_results(&self.0)?;
        Ok(debates)
    }

    fn insert_debate(&self, debate: &NewDebate) -> QueryResult<Debate> {
        let debate = diesel::insert_into(schema::debates::table)
            .values(debate)
            .get_result::<Debate>(&self.0)?;
        Ok(debate)
    }

    fn get_old_finished_debates(&self, finished_since: Duration) -> QueryResult<Vec<Debate>> {
        let debates = schema::debates::table
            .filter(schema::debates::voting_end.le(Utc::now() - finished_since))
            .get_results(&self.0)?;
        Ok(debates)
    }

    fn delete_debate(&self, debate_id: i64) -> QueryResult<()> {
        diesel::delete(schema::debates::table.filter(schema::debates::id.eq(debate_id)))
            .execute(&self.0)?;
        Ok(())
    }

    fn get_partial_ballots_of_ballot(&self, ballot_id: i64) -> QueryResult<Vec<PartialBallot>> {
        let partial_ballots = schema::partial_ballots::table
            .filter(schema::partial_ballots::ballot.eq(ballot_id))
            .get_results(&self.0)?;
        Ok(partial_ballots)
    }

    fn insert_partial_ballot(
        &self,
        partial_ballot: NewPartialBallot,
    ) -> QueryResult<PartialBallot> {
        let partial_ballot = vec![partial_ballot]
            .insert_into(schema::partial_ballots::table)
            .get_result(&self.0)?;
        Ok(partial_ballot)
    }

    fn delete_partial_ballots_of_ballot(&self, ballot_id: i64) -> QueryResult<()> {
        diesel::delete(
            schema::partial_ballots::table.filter(schema::partial_ballots::ballot.eq(ballot_id)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn get_participation(
        &self,
        participant_account_id: i64,
        debate_id: i64,
    ) -> QueryResult<Participation> {
        let participation = schema::participations::table
            .filter(schema::participations::debate.eq(debate_id))
            .filter(schema::participations::participant.eq(participant_account_id))
            .get_result::<Participation>(&self.0)?;
        Ok(participation)
    }

    fn get_participants_of_debate(
        &self,
        debate_id: i64,
    ) -> QueryResult<Vec<Result<Account, OtherAccount>>> {
        let participants = schema::participations::table
            .filter(schema::participations::debate.eq(debate_id))
            .inner_join(schema::accounts::table)
            .filter(schema::accounts::id.eq(schema::participations::participant))
            .select(schema::accounts::all_columns)
            .get_results(&self.0)?;
        Ok(participants
            .into_iter()
            .map(|raw_account: RawAccount| {
                Account::try_from(raw_account).map_err(OtherAccount::from)
            })
            .collect())
    }

    fn insert_participations(
        &self,
        participations: &[NewParticipation],
    ) -> QueryResult<Vec<Participation>> {
        let participations = diesel::insert_into(schema::participations::table)
            .values(participations)
            .get_results(&self.0)?;
        Ok(participations)
    }

    fn get_votes_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Vote>> {
        let votes = schema::votes::table
            .filter(schema::votes::debate.eq(debate_id))
            .get_results(&self.0)?;
        Ok(votes)
    }

    fn get_vote_of_voter_in_debate(&self, voter_id: i64, debate_id: i64) -> QueryResult<Vote> {
        let vote = schema::votes::table
            .filter(
                schema::votes::voter
                    .eq(voter_id)
                    .and(schema::votes::debate.eq(debate_id)),
            )
            .get_result(&self.0)?;
        Ok(vote)
    }

    fn insert_vote(&self, vote: NewVote) -> QueryResult<Vote> {
        let vote = vec![vote]
            .insert_into(schema::votes::table)
            .get_result(&self.0)?;
        Ok(vote)
    }

    fn insert_verify_email_link(
        &self,
        verify_email_link: &NewVerifyEmailLink,
    ) -> QueryResult<VerifyEmailLink> {
        let verify_email_link = diesel::insert_into(schema::verify_email_links::table)
            .values(verify_email_link)
            .get_result(&self.0)?;
        Ok(verify_email_link)
    }

    fn get_verify_email_links_of_account(
        &self,
        account_id: i64,
    ) -> QueryResult<Vec<VerifyEmailLink>> {
        let verify_email_links = schema::verify_email_links::table
            .filter(schema::verify_email_links::account_id.eq(account_id))
            .get_results(&self.0)?;
        Ok(verify_email_links)
    }

    fn get_verify_email_link_with_hash(
        &self,
        verify_email_link_hash: &str,
    ) -> QueryResult<VerifyEmailLink> {
        let verify_email_link = schema::verify_email_links::table
            .filter(schema::verify_email_links::proof_token_hash.eq(verify_email_link_hash))
            .get_result(&self.0)?;
        Ok(verify_email_link)
    }

    fn delete_verify_email_link(&self, verify_email_link_id: i64) -> QueryResult<()> {
        diesel::delete(
            schema::verify_email_links::table
                .filter(schema::verify_email_links::id.eq(verify_email_link_id)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn get_argument_weight(
        &self,
        account_id: i64,
        argument_id: i64,
    ) -> QueryResult<ArgumentWeight> {
        let argument_weight = schema::argument_weights::table
            .filter(
                schema::argument_weights::account
                    .eq(account_id)
                    .and(schema::argument_weights::argument.eq(argument_id)),
            )
            .get_result(&self.0)?;
        Ok(argument_weight)
    }

    fn insert_argument_weight(
        &self,
        argument_weight: NewArgumentWeight,
    ) -> QueryResult<ArgumentWeight> {
        let argument_weight = diesel::insert_into(schema::argument_weights::table)
            .values(vec![argument_weight])
            .get_result(&self.0)?;
        Ok(argument_weight)
    }

    fn update_argument_weight(
        &self,
        argument_weight: &ArgumentWeight,
    ) -> QueryResult<ArgumentWeight> {
        let argument_weight = diesel::update(argument_weight)
            .set(argument_weight)
            .get_result(&self.0)?;
        Ok(argument_weight)
    }

    fn get_login_link_with_hash(&self, login_token_hash: &str) -> QueryResult<LoginLink> {
        let login_link = schema::login_links::table
            .filter(schema::login_links::proof_token_hash.eq(login_token_hash))
            .get_result(&self.0)?;
        Ok(login_link)
    }

    fn insert_login_link(&self, login_link: NewLoginLink) -> QueryResult<LoginLink> {
        let login_link = diesel::insert_into(schema::login_links::table)
            .values(vec![login_link])
            .get_result(&self.0)?;
        Ok(login_link)
    }

    fn delete_login_link(&self, id: i64) -> QueryResult<()> {
        diesel::delete(schema::login_links::table.filter(schema::login_links::id.eq(id)))
            .execute(&self.0)?;
        Ok(())
    }

    fn get_password_reset_with_hash(
        &self,
        password_reset_token_hash: &str,
    ) -> QueryResult<PasswordReset> {
        let password_reset = schema::password_resets::table
            .filter(schema::password_resets::proof_token_hash.eq(password_reset_token_hash))
            .get_result(&self.0)?;
        Ok(password_reset)
    }

    fn insert_password_reset(
        &self,
        password_reset: NewPasswordReset,
    ) -> QueryResult<PasswordReset> {
        let password_reset = diesel::insert_into(schema::password_resets::table)
            .values(vec![password_reset])
            .get_result(&self.0)?;
        Ok(password_reset)
    }

    fn delete_password_reset(&self, id: i64) -> QueryResult<()> {
        diesel::delete(schema::password_resets::table.filter(schema::password_resets::id.eq(id)))
            .execute(&self.0)?;
        Ok(())
    }

    fn insert_invitation(&self, invitation: NewInvitation) -> QueryResult<Invitation> {
        let invitation = diesel::insert_into(schema::invitations::table)
            .values(vec![invitation])
            .get_result(&self.0)?;
        Ok(invitation)
    }

    fn get_invitations_of_invitee(&self, invitee_id: i64) -> QueryResult<Vec<Invitation>> {
        let invitations = schema::invitations::table
            .filter(schema::invitations::invitee.eq(invitee_id))
            .get_results(&self.0)?;
        Ok(invitations)
    }

    fn get_invitation_with_external_id(&self, external_id: Uuid) -> QueryResult<Invitation> {
        let invitation = schema::invitations::table
            .filter(schema::invitations::external_id.eq(external_id))
            .get_result::<Invitation>(&self.0)?;
        Ok(invitation)
    }

    fn delete_invitation(&self, id: i64) -> QueryResult<()> {
        diesel::delete(schema::invitations::table.filter(schema::invitations::id.eq(id)))
            .execute(&self.0)?;
        Ok(())
    }

    fn get_invite_link_with_hash(&self, invite_link_token_hash: &str) -> QueryResult<InviteLink> {
        let invite_link = schema::invite_links::table
            .filter(schema::invite_links::proof_token_hash.eq(invite_link_token_hash))
            .get_result(&self.0)?;
        Ok(invite_link)
    }

    fn insert_invite_link(&self, invite_link: NewInviteLink) -> QueryResult<InviteLink> {
        let invite_link = diesel::insert_into(schema::invite_links::table)
            .values(vec![invite_link])
            .get_result(&self.0)?;
        Ok(invite_link)
    }

    fn delete_invite_link(&self, id: i64) -> QueryResult<()> {
        diesel::delete(schema::invite_links::table.filter(schema::invite_links::id.eq(id)))
            .execute(&self.0)?;
        Ok(())
    }

    fn get_debate_with_public_invite_link_token(
        &self,
        public_invite_link_token: &str,
    ) -> QueryResult<Debate> {
        let debate = schema::debates::table
            .filter(schema::debates::public_invite_link_token.eq(public_invite_link_token))
            .get_result(&self.0)?;
        Ok(debate)
    }

    fn insert_debate_summary(&self, debate_summary: DebateSummary) -> QueryResult<DebateSummary> {
        let debate_summary = diesel::insert_into(schema::debate_summaries::table)
            .values(vec![debate_summary])
            .get_result(&self.0)?;
        Ok(debate_summary)
    }

    fn get_debate_summary_of_debate(&self, debate_id: i64) -> QueryResult<DebateSummary> {
        let debate_summary = schema::debate_summaries::table
            .filter(schema::debate_summaries::debate.eq(debate_id))
            .get_result(&self.0)?;
        Ok(debate_summary)
    }

    fn delete_debate_summary(&self, debate_id: i64) -> QueryResult<()> {
        diesel::delete(
            schema::debate_summaries::table.filter(schema::debate_summaries::debate.eq(debate_id)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn get_debates_missing_summary(&self) -> QueryResult<Vec<Debate>> {
        let debates = schema::debates::table
            .filter(schema::debates::voting_end.le(Utc::now()))
            .left_outer_join(schema::debate_summaries::table)
            .filter(schema::debate_summaries::debate.is_null())
            .select(schema::debates::all_columns)
            .filter(schema::debates::concerned_argument.is_null())
            .get_results(&self.0)?;
        Ok(debates)
    }

    fn delete_old_auth_tokens(&self, age: Duration) -> QueryResult<()> {
        diesel::delete(
            schema::auth_tokens::table
                .filter(schema::auth_tokens::issued_time.le(Utc::now() - age)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn delete_old_login_links(&self, age: Duration) -> QueryResult<()> {
        diesel::delete(
            schema::login_links::table
                .filter(schema::login_links::issued_time.le(Utc::now() - age)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn delete_old_password_resets(&self, age: Duration) -> QueryResult<()> {
        diesel::delete(
            schema::password_resets::table
                .filter(schema::password_resets::started_time.le(Utc::now() - age)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn delete_old_verify_email_links(&self, age: Duration) -> QueryResult<()> {
        diesel::delete(
            schema::verify_email_links::table
                .filter(schema::verify_email_links::issued_time.le(Utc::now() - age)),
        )
        .execute(&self.0)?;
        Ok(())
    }

    fn mark_account_as_deleted(&self, account_id: i64) -> QueryResult<()> {
        diesel::update(schema::accounts::table.filter(schema::accounts::id.eq(account_id)))
            .set((
                schema::accounts::email.eq(Option::<String>::None),
                schema::accounts::password_hash.eq(Option::<String>::None),
                schema::accounts::salt.eq(Option::<String>::None),
            ))
            .execute(&self.0)?;
        Ok(())
    }

    fn delete_accounts_ready_for_deletion(&self) -> QueryResult<()> {
        let ids_to_delete = schema::accounts::table
            .filter(schema::accounts::email.is_null())
            .left_outer_join(schema::participations::table)
            .filter(schema::participations::participant.is_null())
            .select(schema::accounts::id)
            .get_results::<i64>(&self.0)?;
        for account_id in ids_to_delete.iter() {
            diesel::delete(schema::accounts::table.filter(schema::accounts::id.eq(account_id)))
                .execute(&self.0)?;
        }
        Ok(())
    }

    fn delete_old_temporary_accounts(&self, age: Duration) -> QueryResult<()> {
        let account_ids_to_delete = schema::accounts::table
            .filter(schema::accounts::temporary.eq(true))
            .filter(schema::accounts::created.le(Utc::now() - age))
            .select(schema::accounts::id)
            .get_results::<i64>(&self.0)?;
        let debate_ids_to_delete = schema::debates::table
            .left_outer_join(schema::participations::table)
            .filter(schema::participations::participant.eq_any(account_ids_to_delete.clone()))
            .select(schema::debates::id)
            .get_results::<i64>(&self.0)?;
        for debate_id in debate_ids_to_delete.iter() {
            diesel::delete(schema::debates::table.filter(schema::debates::id.eq(debate_id)))
                .execute(&self.0)?;
        }
        for account_id in account_ids_to_delete.iter() {
            diesel::delete(schema::accounts::table.filter(schema::accounts::id.eq(account_id)))
                .execute(&self.0)?;
        }
        Ok(())
    }
}
