use chrono::Duration;
use std::cell::RefCell;
use std::convert::TryInto;
use std::num::TryFromIntError;
use uuid::Uuid;

use crate::db::{Db, QueryError, QueryResult};
use crate::models::accounts::*;
use crate::models::answers::*;
use crate::models::argument_weights::*;
use crate::models::arguments::*;
use crate::models::auth_tokens::*;
use crate::models::ballots::*;
use crate::models::debate_summaries::*;
use crate::models::debates::*;
use crate::models::invitations::*;
use crate::models::invite_links::*;
use crate::models::login_links::*;
use crate::models::partial_ballots::*;
use crate::models::participations::*;
use crate::models::password_resets::*;
use crate::models::supports::*;
use crate::models::verify_email_links::*;
use crate::models::votes::*;

#[derive(Debug)]
pub struct MockDb {
    accounts: RefCell<Vec<RawAccount>>,
    answers: RefCell<Vec<Answer>>,
    arguments: RefCell<Vec<Argument>>,
    auth_tokens: RefCell<Vec<AuthToken>>,
    ballots: RefCell<Vec<Ballot>>,
    debates: RefCell<Vec<Debate>>,
    partial_ballots: RefCell<Vec<PartialBallot>>,
    participations: RefCell<Vec<Participation>>,
    supports: RefCell<Vec<Support>>,
    votes: RefCell<Vec<Vote>>,
    verify_email_links: RefCell<Vec<VerifyEmailLink>>,
    argument_weights: RefCell<Vec<ArgumentWeight>>,
    login_links: RefCell<Vec<LoginLink>>,
    password_resets: RefCell<Vec<PasswordReset>>,
    invitations: RefCell<Vec<Invitation>>,
    invite_links: RefCell<Vec<InviteLink>>,
    debate_summaries: RefCell<Vec<DebateSummary>>,
    id_counter: RefCell<i64>,
}

impl Default for MockDb {
    fn default() -> Self {
        MockDb::new()
    }
}

impl MockDb {
    pub fn new() -> MockDb {
        MockDb {
            accounts: RefCell::new(Vec::new()),
            answers: RefCell::new(Vec::new()),
            arguments: RefCell::new(Vec::new()),
            auth_tokens: RefCell::new(Vec::new()),
            ballots: RefCell::new(Vec::new()),
            debates: RefCell::new(Vec::new()),
            partial_ballots: RefCell::new(Vec::new()),
            participations: RefCell::new(Vec::new()),
            supports: RefCell::new(Vec::new()),
            votes: RefCell::new(Vec::new()),
            verify_email_links: RefCell::new(Vec::new()),
            argument_weights: RefCell::new(Vec::new()),
            login_links: RefCell::new(Vec::new()),
            password_resets: RefCell::new(Vec::new()),
            invitations: RefCell::new(Vec::new()),
            invite_links: RefCell::new(Vec::new()),
            debate_summaries: RefCell::new(Vec::new()),
            id_counter: RefCell::new(0),
        }
    }

    fn get_new_id(&self) -> i64 {
        *self.get_many_ids(1).get(0).unwrap()
    }

    fn get_many_ids(&self, n: i64) -> Vec<i64> {
        let mut id_counter = self.id_counter.borrow_mut();
        let first = *id_counter + 1;
        let last = *id_counter + n;
        *id_counter = last;
        (first..last + 1).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_many_ids() {
        let mock_db = MockDb::new();
        let three_ids = mock_db.get_many_ids(3);
        assert_eq!(three_ids.len(), 3);
    }

    #[test]
    fn get_new_id() {
        let mock_db = MockDb::new();
        let _id = mock_db.get_new_id();
    }
}

impl Db for MockDb {
    fn insert_account(&self, account: NewAccount) -> QueryResult<Account> {
        let id = self.get_new_id();
        let account = NewRawAccount::from(account).with_id(id);
        self.accounts.borrow_mut().push(account.clone());
        Ok(Account::try_from(account).unwrap())
    }

    fn update_account(&self, account: Account) -> QueryResult<Account> {
        let index = self
            .accounts
            .borrow()
            .iter()
            .position(|a| a.id == account.id)
            .ok_or(QueryError::NotFound)?;
        *self.accounts.borrow_mut().get_mut(index).unwrap() = RawAccount::from(account.clone());
        Ok(account)
    }

    fn get_account_with_email(&self, email: &str) -> QueryResult<Account> {
        let accounts = self.accounts.borrow();
        let account = accounts
            .iter()
            .find(|account| account.email.as_ref().map(|s| s == email).unwrap_or(false))
            .ok_or(QueryError::NotFound)?
            .clone();
        Ok(Account::try_from(account).unwrap())
    }

    fn get_account_with_external_id(
        &self,
        external_id: &Uuid,
    ) -> QueryResult<Result<Account, OtherAccount>> {
        let accounts = self.accounts.borrow();
        let account = accounts
            .iter()
            .find(|account| account.external_id == *external_id)
            .ok_or(QueryError::NotFound)?
            .clone();
        Ok(Account::try_from(account).map_err(OtherAccount::from))
    }

    fn get_account_with_id(&self, id: i64) -> QueryResult<Result<Account, OtherAccount>> {
        let accounts = self.accounts.borrow();
        let account = accounts
            .iter()
            .find(|account| account.id == id)
            .ok_or(QueryError::NotFound)?
            .clone();
        Ok(Account::try_from(account).map_err(OtherAccount::from))
    }

    fn get_accounts_of_all_debates(&self, account_id: i64) -> QueryResult<Vec<OtherAccount>> {
        let participations = self.participations.borrow();
        let accounts = self.accounts.borrow();
        let mut accounts = participations
            .iter()
            // Get debates that the user participates in:
            .filter(|participation| participation.participant == account_id)
            .map(|participation| participation.debate)
            // Get all participations of these debates:
            .map(|debate_id| {
                participations
                    .iter()
                    .filter(move |participation| participation.debate == debate_id)
            })
            // Multiple people participate in each debate,
            // so they are all put into one vector:
            .fold(Vec::new(), |mut acc, iter| {
                acc.append(&mut iter.collect());
                acc
            })
            .iter()
            .map(|participation| participation.participant)
            // Get full accounts of the found account IDs:
            .map(|account_id| {
                accounts
                    .iter()
                    .find(|account| account.id == account_id)
                    .unwrap()
            })
            // The full accounts are turned into `OtherAccount` structs:
            .map(|account| OtherAccount {
                external_id: account.external_id,
                name: account.name.clone(),
            })
            .collect::<Vec<OtherAccount>>();
        // Because people may participate in multiple debates,
        // duplicates are removed:
        accounts.sort_by_key(|other_account| other_account.external_id);
        accounts.dedup_by_key(|other_account| other_account.external_id);
        Ok(accounts)
    }

    fn get_answers_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Answer>> {
        let answers = self.answers.borrow();
        let answers = answers
            .iter()
            .filter(|answer| answer.debate == debate_id)
            .cloned()
            .collect::<Vec<Answer>>();
        Ok(answers)
    }
    fn get_supporting_arguments_of_answer(&self, answer_id: i64) -> QueryResult<Vec<Argument>> {
        let arguments = self.arguments.borrow();
        let supports = self.supports.borrow();
        let arguments = supports
            .iter()
            .filter(|support| support.answer == answer_id)
            .map(|support| {
                arguments
                    .iter()
                    .find(|argument| argument.id == support.argument)
                    .unwrap()
                    .clone()
            })
            .collect::<Vec<Argument>>();
        Ok(arguments)
    }

    fn get_answer_with_external_id(&self, external_id: Uuid) -> QueryResult<Answer> {
        let answers = self.answers.borrow();
        let answer = answers
            .iter()
            .find(|answer| answer.external_id == external_id)
            .ok_or(QueryError::NotFound)?;
        Ok(answer.clone())
    }

    fn get_answer_with_id(&self, answer_id: i64) -> QueryResult<Answer> {
        let answers = self.answers.borrow();
        let answer = answers
            .iter()
            .find(|answer| answer.id == answer_id)
            .unwrap()
            .clone();
        Ok(answer)
    }

    fn insert_answers(&self, answers: &[NewAnswer]) -> QueryResult<Vec<Answer>> {
        let l: i64 = answers.len().try_into()?;
        let ids = self.get_many_ids(l);
        let answers = answers
            .iter()
            .zip(ids)
            .map(|(answer, id)| answer.clone().with_id(id))
            .collect::<Vec<Answer>>();
        self.answers.borrow_mut().append(&mut answers.clone());
        Ok(answers)
    }

    fn get_argument_with_id(&self, id: i64) -> QueryResult<Argument> {
        let arguments = self.arguments.borrow();
        let argument = arguments
            .iter()
            .find(|argument| argument.id == id)
            .ok_or(QueryError::NotFound)?;
        Ok(argument.clone())
    }

    fn get_arguments_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Argument>> {
        let arguments = self.arguments.borrow();
        let arguments = arguments
            .iter()
            .filter(|argument| argument.debate == debate_id)
            .cloned()
            .collect::<Vec<Argument>>();
        Ok(arguments)
    }

    fn get_supported_answers_of_argument(&self, argument_id: i64) -> QueryResult<Vec<Answer>> {
        let supports = self.supports.borrow();
        let answers = self.answers.borrow();
        let answers = supports
            .iter()
            .filter(|support| support.argument == argument_id)
            .map(|support| {
                answers
                    .iter()
                    .filter(move |answer| answer.id == support.answer)
            })
            .fold(Vec::new(), |mut acc, iter| {
                acc.append(&mut iter.cloned().collect());
                acc
            });
        Ok(answers)
    }

    fn insert_supports(&self, supports: &[NewSupport]) -> QueryResult<Vec<Support>> {
        let l: i64 = supports.len().try_into()?;
        let ids = self.get_many_ids(l);
        let supports = supports
            .iter()
            .zip(ids)
            .map(|(support, id)| support.clone().with_id(id))
            .collect::<Vec<Support>>();
        self.supports.borrow_mut().append(&mut supports.clone());
        Ok(supports)
    }

    fn get_debate_of_argument(&self, argument_id: i64) -> QueryResult<Debate> {
        let debates = self.debates.borrow();
        let debate_id = self.get_argument_with_id(argument_id)?.debate;
        let debate = debates
            .iter()
            .find(|debate| debate.id == debate_id)
            .unwrap()
            .clone();
        Ok(debate)
    }

    fn get_argument_with_external_id(&self, external_id: Uuid) -> QueryResult<Argument> {
        let arguments = self.arguments.borrow();
        let argument = arguments
            .iter()
            .find(|argument| argument.external_id == external_id)
            .ok_or(QueryError::NotFound)?;
        Ok(argument.clone())
    }

    fn insert_argument(&self, argument: &NewArgument) -> QueryResult<Argument> {
        let id = self.get_new_id();
        let argument = argument.clone().with_id(id);
        self.arguments.borrow_mut().push(argument.clone());
        Ok(argument)
    }

    fn insert_auth_token(&self, auth_token: &NewAuthToken) -> QueryResult<AuthToken> {
        let id = self.get_new_id();
        let auth_token = auth_token.clone().with_id(id);
        self.auth_tokens.borrow_mut().push(auth_token.clone());
        Ok(auth_token)
    }

    fn get_auth_token_with_hash(&self, auth_token_hash: &str) -> QueryResult<AuthToken> {
        let auth_tokens = self.auth_tokens.borrow();
        let auth_token = auth_tokens
            .iter()
            .find(|auth_token| auth_token.auth_token_hash == auth_token_hash)
            .unwrap()
            .clone();
        Ok(auth_token)
    }

    fn delete_auth_tokens_of_account(&self, account_id: i64) -> QueryResult<()> {
        let index = self
            .auth_tokens
            .borrow()
            .iter()
            .position(|auth_token| auth_token.account_id == account_id);
        index.map(|index| self.auth_tokens.borrow_mut().remove(index));
        Ok(())
    }

    fn get_ballots_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Ballot>> {
        let ballots = self.ballots.borrow();
        let ballots = ballots
            .iter()
            .filter(|ballot| ballot.debate == debate_id)
            .cloned()
            .collect::<Vec<Ballot>>();
        Ok(ballots)
    }

    fn get_ballot_of_voter_of_debate(&self, debate_id: i64, voter_id: i64) -> QueryResult<Ballot> {
        let ballots = self.ballots.borrow();
        let ballot = ballots
            .iter()
            .find(|ballot| ballot.debate == debate_id && ballot.voter == voter_id)
            .ok_or(QueryError::NotFound)?
            .clone();
        Ok(ballot)
    }

    fn insert_ballot(&self, ballot: NewBallot) -> QueryResult<Ballot> {
        let id = self.get_new_id();
        let ballot = ballot.with_id(id);
        self.ballots.borrow_mut().push(ballot.clone());
        Ok(ballot)
    }

    fn update_ballot(&self, ballot: Ballot) -> QueryResult<Ballot> {
        let index = self
            .ballots
            .borrow()
            .iter()
            .position(|a| a.id == ballot.id)
            .ok_or(QueryError::NotFound)?;
        *self.ballots.borrow_mut().get_mut(index).unwrap() = ballot.clone();
        Ok(ballot)
    }

    fn get_debate_with_id(&self, id: i64) -> QueryResult<Debate> {
        let debates = self.debates.borrow();
        let debate = debates
            .iter()
            .find(|debate| debate.id == id)
            .ok_or(QueryError::NotFound)?;
        Ok(debate.clone())
    }

    fn get_debate_with_external_id(&self, external_id: Uuid) -> QueryResult<Debate> {
        let debates = self.debates.borrow();
        let debate = debates
            .iter()
            .find(|debate| debate.external_id == external_id)
            .ok_or(QueryError::NotFound)?;
        Ok(debate.clone())
    }

    fn get_top_level_debates_of_participant(
        &self,
        participant_id: i64,
    ) -> QueryResult<Vec<Debate>> {
        let participations = self.participations.borrow();
        let debates = self.debates.borrow();
        let debates = participations
            .iter()
            .filter(|participation| participation.participant == participant_id)
            .map(|participation| {
                debates
                    .iter()
                    .filter(move |debate| debate.id == participation.id)
                    .find(|debate| debate.concerned_argument.is_none())
                    .unwrap()
                    .clone()
            })
            .collect::<Vec<Debate>>();
        Ok(debates)
    }

    fn get_debates_about_argument(&self, argument_id: i64) -> QueryResult<Vec<Debate>> {
        let debates = self.debates.borrow();
        let debates = debates
            .iter()
            .filter(|debate| debate.concerned_argument == Some(argument_id))
            .cloned()
            .collect::<Vec<Debate>>();
        Ok(debates)
    }

    fn get_sub_debates_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Debate>> {
        let debates = self.debates.borrow();
        let arguments = self.arguments.borrow();
        let debates = arguments
            .iter()
            .filter(|argument| argument.debate == debate_id)
            .map(|argument| {
                debates
                    .iter()
                    .filter(move |debate| debate.concerned_argument == Some(argument.id))
            })
            .flatten()
            .cloned()
            .collect::<Vec<Debate>>();
        Ok(debates)
    }

    fn insert_debate(&self, debate: &NewDebate) -> QueryResult<Debate> {
        let id = self.get_new_id();
        let debate = debate.clone().with_id(id);
        self.debates.borrow_mut().push(debate.clone());
        Ok(debate)
    }

    fn get_old_finished_debates(&self, _: Duration) -> QueryResult<Vec<Debate>> {
        unimplemented!()
    }

    fn delete_debate(&self, _: i64) -> QueryResult<()> {
        unimplemented!()
    }

    fn get_partial_ballots_of_ballot(&self, ballot_id: i64) -> QueryResult<Vec<PartialBallot>> {
        let partial_ballots = self.partial_ballots.borrow();
        let partial_ballots = partial_ballots
            .iter()
            .filter(|partial_ballot| partial_ballot.ballot == ballot_id)
            .cloned()
            .collect::<Vec<PartialBallot>>();
        Ok(partial_ballots)
    }

    fn insert_partial_ballot(
        &self,
        partial_ballot: NewPartialBallot,
    ) -> QueryResult<PartialBallot> {
        let id = self.get_new_id();
        let partial_ballot = partial_ballot.with_id(id);
        self.partial_ballots
            .borrow_mut()
            .push(partial_ballot.clone());
        Ok(partial_ballot)
    }

    fn delete_partial_ballots_of_ballot(&self, ballot_id: i64) -> QueryResult<()> {
        let index = self
            .partial_ballots
            .borrow()
            .iter()
            .position(|partial_ballot| partial_ballot.ballot == ballot_id)
            .ok_or(QueryError::NotFound)?;
        self.partial_ballots.borrow_mut().remove(index);
        Ok(())
    }

    fn get_participation(
        &self,
        participant_account_id: i64,
        debate_id: i64,
    ) -> QueryResult<Participation> {
        let participations = self.participations.borrow();
        let participation = participations
            .iter()
            .find(|participation| {
                participation.debate == debate_id
                    && participation.participant == participant_account_id
            })
            .unwrap()
            .clone();
        Ok(participation)
    }

    fn get_participants_of_debate(
        &self,
        debate_id: i64,
    ) -> QueryResult<Vec<Result<Account, OtherAccount>>> {
        let participations = self.participations.borrow();
        let accounts = self.accounts.borrow();
        let participants = participations
            .iter()
            .filter(|participation| participation.debate == debate_id)
            .map(|participation| {
                accounts
                    .iter()
                    .find(|account| account.id == participation.participant)
                    .unwrap()
                    .clone()
            })
            .map(|account| Account::try_from(account).map_err(OtherAccount::from))
            .collect();
        Ok(participants)
    }

    fn insert_participations(
        &self,
        participations: &[NewParticipation],
    ) -> QueryResult<Vec<Participation>> {
        let l: i64 = participations.len().try_into()?;
        let ids = self.get_many_ids(l);
        let participations = participations
            .iter()
            .zip(ids)
            .map(|(participation, id)| participation.clone().with_id(id))
            .collect::<Vec<Participation>>();
        self.participations
            .borrow_mut()
            .append(&mut participations.clone());
        Ok(participations)
    }

    fn get_votes_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Vote>> {
        let votes = self.votes.borrow();
        let votes = votes
            .iter()
            .filter(|vote| vote.debate == debate_id)
            .cloned()
            .collect::<Vec<Vote>>();
        Ok(votes)
    }

    fn get_vote_of_voter_in_debate(&self, voter_id: i64, debate_id: i64) -> QueryResult<Vote> {
        let votes = self.votes.borrow();
        let vote = votes
            .iter()
            .find(|vote| vote.voter == voter_id && vote.debate == debate_id)
            .ok_or(QueryError::NotFound)?;
        Ok(vote.clone())
    }

    fn insert_vote(&self, vote: NewVote) -> QueryResult<Vote> {
        let id = self.get_new_id();
        let vote = vote.with_id(id);
        self.votes.borrow_mut().push(vote.clone());
        Ok(vote)
    }

    fn insert_verify_email_link(
        &self,
        verify_email_link: &NewVerifyEmailLink,
    ) -> QueryResult<VerifyEmailLink> {
        let id = self.get_new_id();
        let verify_email_link = verify_email_link.clone().with_id(id);
        self.verify_email_links
            .borrow_mut()
            .push(verify_email_link.clone());
        Ok(verify_email_link)
    }

    fn get_verify_email_links_of_account(
        &self,
        account_id: i64,
    ) -> QueryResult<Vec<VerifyEmailLink>> {
        let verify_email_links = self.verify_email_links.borrow();
        let verify_email_links = verify_email_links
            .iter()
            .filter(|verify_email_link| verify_email_link.account_id == account_id)
            .cloned()
            .collect();
        Ok(verify_email_links)
    }
    fn get_verify_email_link_with_hash(
        &self,
        verify_email_link_hash: &str,
    ) -> QueryResult<VerifyEmailLink> {
        let verify_email_links = self.verify_email_links.borrow();
        let verify_email_link = verify_email_links
            .iter()
            .find(|verify_email_link| verify_email_link.proof_token_hash == verify_email_link_hash)
            .unwrap()
            .clone();
        Ok(verify_email_link)
    }

    fn delete_verify_email_link(&self, verify_email_link_id: i64) -> QueryResult<()> {
        let index = self
            .verify_email_links
            .borrow()
            .iter()
            .position(|verify_email_link| verify_email_link.id == verify_email_link_id)
            .ok_or(QueryError::NotFound)?;
        self.verify_email_links.borrow_mut().remove(index);
        Ok(())
    }

    fn get_argument_weight(
        &self,
        account_id: i64,
        argument_id: i64,
    ) -> QueryResult<ArgumentWeight> {
        let argument_weights = self.argument_weights.borrow();
        let argument_weight = argument_weights
            .iter()
            .find(|argument_weight| {
                argument_weight.account == account_id && argument_weight.argument == argument_id
            })
            .ok_or(QueryError::NotFound)?
            .clone();
        Ok(argument_weight)
    }

    fn insert_argument_weight(
        &self,
        argument_weight: NewArgumentWeight,
    ) -> QueryResult<ArgumentWeight> {
        let id = self.get_new_id();
        let argument_weight = argument_weight.with_id(id);
        self.argument_weights
            .borrow_mut()
            .push(argument_weight.clone());
        Ok(argument_weight)
    }

    fn update_argument_weight(
        &self,
        argument_weight: &ArgumentWeight,
    ) -> QueryResult<ArgumentWeight> {
        let index = self
            .argument_weights
            .borrow()
            .iter()
            .position(|a| a.id == argument_weight.id)
            .ok_or(QueryError::NotFound)?;
        *self.argument_weights.borrow_mut().get_mut(index).unwrap() = argument_weight.clone();
        Ok(argument_weight.clone())
    }

    fn get_login_link_with_hash(&self, login_token_hash: &str) -> QueryResult<LoginLink> {
        let login_links = self.login_links.borrow();
        let login_link = login_links
            .iter()
            .find(|login_link| login_link.proof_token_hash == login_token_hash)
            .unwrap()
            .clone();
        Ok(login_link)
    }

    fn insert_login_link(&self, login_link: NewLoginLink) -> QueryResult<LoginLink> {
        let id = self.get_new_id();
        let login_link = login_link.with_id(id);
        self.login_links.borrow_mut().push(login_link.clone());
        Ok(login_link)
    }

    fn delete_login_link(&self, id: i64) -> QueryResult<()> {
        let index = self
            .login_links
            .borrow()
            .iter()
            .position(|login_link| login_link.id == id)
            .ok_or(QueryError::NotFound)?;
        self.login_links.borrow_mut().remove(index);
        Ok(())
    }

    fn get_password_reset_with_hash(
        &self,
        password_reset_token_hash: &str,
    ) -> QueryResult<PasswordReset> {
        let password_resets = self.password_resets.borrow();
        let password_reset = password_resets
            .iter()
            .find(|password_reset| password_reset.proof_token_hash == password_reset_token_hash)
            .unwrap()
            .clone();
        Ok(password_reset)
    }

    fn insert_password_reset(
        &self,
        password_reset: NewPasswordReset,
    ) -> QueryResult<PasswordReset> {
        let id = self.get_new_id();
        let password_reset = password_reset.with_id(id);
        self.password_resets
            .borrow_mut()
            .push(password_reset.clone());
        Ok(password_reset)
    }

    fn delete_password_reset(&self, id: i64) -> QueryResult<()> {
        let index = self
            .password_resets
            .borrow()
            .iter()
            .position(|password_reset| password_reset.id == id)
            .ok_or(QueryError::NotFound)?;
        self.password_resets.borrow_mut().remove(index);
        Ok(())
    }

    fn insert_invitation(&self, invitation: NewInvitation) -> QueryResult<Invitation> {
        let id = self.get_new_id();
        let invitation = invitation.with_id(id);
        self.invitations.borrow_mut().push(invitation.clone());
        Ok(invitation)
    }

    fn get_invitations_of_invitee(&self, invitee_id: i64) -> QueryResult<Vec<Invitation>> {
        let invitations = self.invitations.borrow();
        let invitations = invitations
            .iter()
            .filter(|invitation| invitation.invitee == invitee_id)
            .cloned()
            .collect();
        Ok(invitations)
    }

    fn get_invitation_with_external_id(&self, external_id: Uuid) -> QueryResult<Invitation> {
        let invitations = self.invitations.borrow();
        let invitation = invitations
            .iter()
            .find(|invitation| invitation.external_id == external_id)
            .ok_or(QueryError::NotFound)?;
        Ok(invitation.clone())
    }

    fn delete_invitation(&self, id: i64) -> QueryResult<()> {
        let index = self
            .invitations
            .borrow()
            .iter()
            .position(|invitation| invitation.id == id)
            .ok_or(QueryError::NotFound)?;
        self.invitations.borrow_mut().remove(index);
        Ok(())
    }

    fn get_invite_link_with_hash(&self, invite_link_token_hash: &str) -> QueryResult<InviteLink> {
        let invite_links = self.invite_links.borrow();
        let invite_link = invite_links
            .iter()
            .find(|invite_link| invite_link.proof_token_hash == invite_link_token_hash)
            .unwrap()
            .clone();
        Ok(invite_link)
    }

    fn insert_invite_link(&self, invite_link: NewInviteLink) -> QueryResult<InviteLink> {
        let id = self.get_new_id();
        let invite_link = invite_link.with_id(id);
        self.invite_links.borrow_mut().push(invite_link.clone());
        Ok(invite_link)
    }

    fn delete_invite_link(&self, id: i64) -> QueryResult<()> {
        let index = self
            .invite_links
            .borrow()
            .iter()
            .position(|invite_link| invite_link.id == id)
            .ok_or(QueryError::NotFound)?;
        self.invite_links.borrow_mut().remove(index);
        Ok(())
    }

    fn get_debate_with_public_invite_link_token(
        &self,
        public_invite_link_token: &str,
    ) -> QueryResult<Debate> {
        let debates = self.debates.borrow();
        let debate = debates
            .iter()
            .find(|debate| {
                debate.public_invite_link_token == Some(public_invite_link_token.to_owned())
            })
            .unwrap()
            .clone();
        Ok(debate)
    }

    fn insert_debate_summary(&self, debate_summary: DebateSummary) -> QueryResult<DebateSummary> {
        self.debate_summaries
            .borrow_mut()
            .push(debate_summary.clone());
        Ok(debate_summary)
    }

    fn get_debate_summary_of_debate(&self, debate_id: i64) -> QueryResult<DebateSummary> {
        let debate_summaries = self.debate_summaries.borrow();
        let debate_summary = debate_summaries
            .iter()
            .find(|debate_summary| debate_summary.debate == debate_id)
            .unwrap()
            .clone();
        Ok(debate_summary)
    }

    fn delete_debate_summary(&self, debate_id: i64) -> QueryResult<()> {
        let index = self
            .debate_summaries
            .borrow()
            .iter()
            .position(|debate_summary| debate_summary.debate == debate_id)
            .ok_or(QueryError::NotFound)?;
        self.debate_summaries.borrow_mut().remove(index);
        Ok(())
    }

    fn get_debates_missing_summary(&self) -> QueryResult<Vec<Debate>> {
        unimplemented!()
    }

    fn delete_old_auth_tokens(&self, _age: Duration) -> QueryResult<()> {
        unimplemented!()
    }

    fn delete_old_login_links(&self, _age: Duration) -> QueryResult<()> {
        unimplemented!()
    }

    fn delete_old_password_resets(&self, _age: Duration) -> QueryResult<()> {
        unimplemented!()
    }

    fn delete_old_verify_email_links(&self, _age: Duration) -> QueryResult<()> {
        unimplemented!()
    }

    fn mark_account_as_deleted(&self, _account_id: i64) -> QueryResult<()> {
        unimplemented!()
    }

    fn delete_accounts_ready_for_deletion(&self) -> QueryResult<()> {
        unimplemented!()
    }

    fn delete_old_temporary_accounts(&self, _age: Duration) -> QueryResult<()> {
        Err(QueryError::Other("Not implemented".to_owned()))
    }
}

impl From<TryFromIntError> for QueryError {
    fn from(_: TryFromIntError) -> QueryError {
        QueryError::Other("TryFromIntError".to_owned())
    }
}
