pub mod connection;
pub mod mock;

use chrono::Duration;
use uuid::Uuid;

use crate::models::accounts::*;
use crate::models::answers::*;
use crate::models::argument_weights::*;
use crate::models::arguments::*;
use crate::models::auth_tokens::*;
use crate::models::ballots::*;
use crate::models::debate_summaries::*;
use crate::models::debates::*;
use crate::models::invitations::*;
use crate::models::invite_links::*;
use crate::models::login_links::*;
use crate::models::partial_ballots::*;
use crate::models::participations::*;
use crate::models::password_resets::*;
use crate::models::supports::*;
use crate::models::verify_email_links::*;
use crate::models::votes::*;

pub trait Db {
    fn insert_account(&self, account: NewAccount) -> QueryResult<Account>;
    fn update_account(&self, account: Account) -> QueryResult<Account>;
    fn get_account_with_email(&self, email: &str) -> QueryResult<Account>;
    fn get_account_with_external_id(
        &self,
        external_id: &Uuid,
    ) -> QueryResult<Result<Account, OtherAccount>>;
    fn get_account_with_id(&self, id: i64) -> QueryResult<Result<Account, OtherAccount>>;
    fn get_accounts_of_all_debates(&self, account_id: i64) -> QueryResult<Vec<OtherAccount>>;
    fn get_answers_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Answer>>;
    fn get_supporting_arguments_of_answer(&self, answer_id: i64) -> QueryResult<Vec<Argument>>;
    fn get_answer_with_external_id(&self, external_id: Uuid) -> QueryResult<Answer>;
    fn get_answer_with_id(&self, answer_id: i64) -> QueryResult<Answer>;
    fn insert_answers(&self, answers: &[NewAnswer]) -> QueryResult<Vec<Answer>>;
    fn get_argument_with_id(&self, id: i64) -> QueryResult<Argument>;
    fn get_arguments_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Argument>>;
    fn get_supported_answers_of_argument(&self, argument_id: i64) -> QueryResult<Vec<Answer>>;
    fn insert_supports(&self, supports: &[NewSupport]) -> QueryResult<Vec<Support>>;
    fn get_debate_of_argument(&self, argument_id: i64) -> QueryResult<Debate>;
    fn get_argument_with_external_id(&self, external_id: Uuid) -> QueryResult<Argument>;
    fn insert_argument(&self, argument: &NewArgument) -> QueryResult<Argument>;
    fn insert_auth_token(&self, auth_token: &NewAuthToken) -> QueryResult<AuthToken>;
    fn get_auth_token_with_hash(&self, auth_token_hash: &str) -> QueryResult<AuthToken>;
    fn delete_auth_tokens_of_account(&self, account_id: i64) -> QueryResult<()>;
    fn get_ballots_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Ballot>>;
    fn get_ballot_of_voter_of_debate(&self, debate_id: i64, voter_id: i64) -> QueryResult<Ballot>;
    fn insert_ballot(&self, ballot: NewBallot) -> QueryResult<Ballot>;
    fn update_ballot(&self, ballot: Ballot) -> QueryResult<Ballot>;
    fn get_debate_with_id(&self, id: i64) -> QueryResult<Debate>;
    fn get_debate_with_external_id(&self, external_id: Uuid) -> QueryResult<Debate>;
    fn get_top_level_debates_of_participant(&self, participant_id: i64)
        -> QueryResult<Vec<Debate>>;
    fn get_debates_about_argument(&self, argument_id: i64) -> QueryResult<Vec<Debate>>;
    fn get_sub_debates_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Debate>>;
    fn insert_debate(&self, debate: &NewDebate) -> QueryResult<Debate>;
    fn get_old_finished_debates(&self, finished_since: Duration) -> QueryResult<Vec<Debate>>;
    fn delete_debate(&self, debate_id: i64) -> QueryResult<()>;
    fn get_partial_ballots_of_ballot(&self, ballot_id: i64) -> QueryResult<Vec<PartialBallot>>;
    fn insert_partial_ballot(&self, partial_ballot: NewPartialBallot)
        -> QueryResult<PartialBallot>;
    fn delete_partial_ballots_of_ballot(&self, ballot_id: i64) -> QueryResult<()>;
    fn get_participation(
        &self,
        participant_account_id: i64,
        debate_id: i64,
    ) -> QueryResult<Participation>;
    fn get_participants_of_debate(
        &self,
        debate_id: i64,
    ) -> QueryResult<Vec<Result<Account, OtherAccount>>>;
    fn insert_participations(
        &self,
        participations: &[NewParticipation],
    ) -> QueryResult<Vec<Participation>>;
    fn get_votes_of_debate(&self, debate_id: i64) -> QueryResult<Vec<Vote>>;
    fn get_vote_of_voter_in_debate(&self, voter_id: i64, debate_id: i64) -> QueryResult<Vote>;
    fn insert_vote(&self, vote: NewVote) -> QueryResult<Vote>;
    fn insert_verify_email_link(
        &self,
        verify_email_link: &NewVerifyEmailLink,
    ) -> QueryResult<VerifyEmailLink>;
    fn get_verify_email_links_of_account(
        &self,
        account_id: i64,
    ) -> QueryResult<Vec<VerifyEmailLink>>;
    fn get_verify_email_link_with_hash(
        &self,
        verify_email_link_hash: &str,
    ) -> QueryResult<VerifyEmailLink>;
    fn delete_verify_email_link(&self, verify_email_link_id: i64) -> QueryResult<()>;
    fn get_argument_weight(&self, account_id: i64, argument_id: i64)
        -> QueryResult<ArgumentWeight>;
    fn insert_argument_weight(
        &self,
        argument_weight: NewArgumentWeight,
    ) -> QueryResult<ArgumentWeight>;
    fn update_argument_weight(
        &self,
        argument_weight: &ArgumentWeight,
    ) -> QueryResult<ArgumentWeight>;
    fn get_login_link_with_hash(&self, login_token_hash: &str) -> QueryResult<LoginLink>;
    fn insert_login_link(&self, login_link: NewLoginLink) -> QueryResult<LoginLink>;
    fn delete_login_link(&self, id: i64) -> QueryResult<()>;
    fn get_password_reset_with_hash(
        &self,
        password_reset_token_hash: &str,
    ) -> QueryResult<PasswordReset>;
    fn insert_password_reset(&self, password_reset: NewPasswordReset)
        -> QueryResult<PasswordReset>;
    fn delete_password_reset(&self, id: i64) -> QueryResult<()>;
    fn insert_invitation(&self, invitation: NewInvitation) -> QueryResult<Invitation>;
    fn get_invitations_of_invitee(&self, invitee_id: i64) -> QueryResult<Vec<Invitation>>;
    fn get_invitation_with_external_id(&self, external_id: Uuid) -> QueryResult<Invitation>;
    fn delete_invitation(&self, id: i64) -> QueryResult<()>;
    fn get_invite_link_with_hash(&self, invite_link_token_hash: &str) -> QueryResult<InviteLink>;
    fn insert_invite_link(&self, invite_link: NewInviteLink) -> QueryResult<InviteLink>;
    fn delete_invite_link(&self, id: i64) -> QueryResult<()>;
    fn get_debate_with_public_invite_link_token(
        &self,
        public_invite_link_token: &str,
    ) -> QueryResult<Debate>;
    fn insert_debate_summary(&self, debate_summary: DebateSummary) -> QueryResult<DebateSummary>;
    fn get_debate_summary_of_debate(&self, debate_id: i64) -> QueryResult<DebateSummary>;
    fn delete_debate_summary(&self, debate_id: i64) -> QueryResult<()>;
    fn get_debates_missing_summary(&self) -> QueryResult<Vec<Debate>>;
    fn delete_old_auth_tokens(&self, age: Duration) -> QueryResult<()>;
    fn delete_old_login_links(&self, age: Duration) -> QueryResult<()>;
    fn delete_old_password_resets(&self, age: Duration) -> QueryResult<()>;
    fn delete_old_verify_email_links(&self, age: Duration) -> QueryResult<()>;
    fn mark_account_as_deleted(&self, account_id: i64) -> QueryResult<()>;
    fn delete_accounts_ready_for_deletion(&self) -> QueryResult<()>;
    fn delete_old_temporary_accounts(&self, age: Duration) -> QueryResult<()>;
}

pub type QueryResult<T> = Result<T, QueryError>;

#[derive(Debug, Fail, Clone)]
pub enum QueryError {
    #[fail(display = "query has returned no items")]
    NotFound,
    #[fail(display = "insertion not possible due to uniqueness constraint")]
    UniquenessViolation,
    #[fail(display = "{}", _0)]
    Other(String),
}

impl From<diesel::result::Error> for QueryError {
    fn from(diesel_error: diesel::result::Error) -> QueryError {
        match diesel_error {
            diesel::result::Error::NotFound => QueryError::NotFound,
            e => QueryError::Other(format!("{:?}", e)),
        }
    }
}
