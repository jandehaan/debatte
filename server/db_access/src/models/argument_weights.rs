use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, AsChangeset)]
pub struct ArgumentWeight {
    pub id: i64,
    pub account: i64,
    pub argument: i64,
    pub weight: i32,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "argument_weights"]
pub struct NewArgumentWeight {
    pub account: i64,
    pub argument: i64,
    pub weight: i32,
}

impl NewArgumentWeight {
    pub fn with_id(self, id: i64) -> ArgumentWeight {
        ArgumentWeight {
            id,
            account: self.account,
            argument: self.argument,
            weight: self.weight,
        }
    }
}
