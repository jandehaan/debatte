use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, AsChangeset)]
pub struct Ballot {
    pub id: i64,
    pub external_id: Uuid,
    pub debate: i64,
    pub voter: i64,
    pub prepared_time: Option<DateTime<Utc>>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "ballots"]
pub struct NewBallot {
    pub external_id: Uuid,
    pub debate: i64,
    pub voter: i64,
    pub prepared_time: Option<DateTime<Utc>>,
}

impl NewBallot {
    pub fn with_id(self, id: i64) -> Ballot {
        Ballot {
            id,
            external_id: self.external_id,
            debate: self.debate,
            voter: self.voter,
            prepared_time: self.prepared_time,
        }
    }
}
