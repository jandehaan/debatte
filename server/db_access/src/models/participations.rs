use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct Participation {
    pub id: i64,
    pub debate: i64,
    pub participant: i64,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "participations"]
pub struct NewParticipation {
    pub debate: i64,
    pub participant: i64,
}

impl NewParticipation {
    pub fn with_id(self, id: i64) -> Participation {
        Participation {
            id,
            debate: self.debate,
            participant: self.participant,
        }
    }
}
