use chrono::{DateTime, Utc};

use crate::models::accounts::*;
use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, Associations)]
#[belongs_to(Account)]
pub struct LoginLink {
    pub id: i64,
    pub proof_token_hash: String,
    pub account_id: i64,
    pub issued_time: DateTime<Utc>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "login_links"]
pub struct NewLoginLink {
    pub proof_token_hash: String,
    pub account_id: i64,
    pub issued_time: DateTime<Utc>,
}

impl NewLoginLink {
    pub fn with_id(self, id: i64) -> LoginLink {
        LoginLink {
            id,
            proof_token_hash: self.proof_token_hash,
            account_id: self.account_id,
            issued_time: self.issued_time,
        }
    }
}
