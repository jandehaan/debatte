use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct Support {
    pub id: i64,
    pub argument: i64,
    pub answer: i64,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "supports"]
pub struct NewSupport {
    pub argument: i64,
    pub answer: i64,
}

impl NewSupport {
    pub fn with_id(self, id: i64) -> Support {
        Support {
            id,
            argument: self.argument,
            answer: self.answer,
        }
    }
}
