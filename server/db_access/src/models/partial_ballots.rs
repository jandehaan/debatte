use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct PartialBallot {
    pub id: i64,
    pub answer: i64,
    pub rank: i32,
    pub ballot: i64,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "partial_ballots"]
pub struct NewPartialBallot {
    pub answer: i64,
    pub rank: i32,
    pub ballot: i64,
}

impl NewPartialBallot {
    pub fn with_id(self, id: i64) -> PartialBallot {
        PartialBallot {
            id,
            answer: self.answer,
            rank: self.rank,
            ballot: self.ballot,
        }
    }
}
