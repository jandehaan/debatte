use chrono::{DateTime, Utc};

use crate::models::accounts::*;
use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, Associations)]
#[belongs_to(Account)]
pub struct AuthToken {
    pub id: i64,
    pub auth_token_hash: String,
    pub account_id: i64,
    pub issued_time: DateTime<Utc>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "auth_tokens"]
pub struct NewAuthToken {
    pub auth_token_hash: String,
    pub account_id: i64,
    pub issued_time: DateTime<Utc>,
}

impl NewAuthToken {
    pub fn with_id(self, id: i64) -> AuthToken {
        AuthToken {
            id,
            auth_token_hash: self.auth_token_hash,
            account_id: self.account_id,
            issued_time: self.issued_time,
        }
    }
}
