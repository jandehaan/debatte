use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, Insertable)]
#[primary_key("debate")]
#[table_name = "debate_summaries"]
pub struct DebateSummary {
    pub debate: i64,
    pub pdf: Vec<u8>,
    pub json: String,
}
