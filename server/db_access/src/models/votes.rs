use uuid::Uuid;

use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct Vote {
    pub id: i64,
    pub external_id: Uuid,
    pub voter: i64,
    pub debate: i64,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "votes"]
pub struct NewVote {
    pub external_id: Uuid,
    pub voter: i64,
    pub debate: i64,
}

impl NewVote {
    pub fn with_id(self, id: i64) -> Vote {
        Vote {
            id,
            external_id: self.external_id,
            voter: self.voter,
            debate: self.debate,
        }
    }
}
