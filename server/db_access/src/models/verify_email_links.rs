use chrono::{DateTime, Utc};

use crate::models::accounts::*;
use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, Associations)]
#[belongs_to(Account)]
pub struct VerifyEmailLink {
    pub id: i64,
    pub proof_token_hash: String,
    pub account_id: i64,
    pub issued_time: DateTime<Utc>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "verify_email_links"]
pub struct NewVerifyEmailLink {
    pub proof_token_hash: String,
    pub account_id: i64,
    pub issued_time: DateTime<Utc>,
}

impl NewVerifyEmailLink {
    pub fn with_id(self, id: i64) -> VerifyEmailLink {
        VerifyEmailLink {
            id,
            proof_token_hash: self.proof_token_hash,
            account_id: self.account_id,
            issued_time: self.issued_time,
        }
    }
}
