use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::schema::*;

pub use crate::models::types::AnswerSemantics;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct Answer {
    pub id: i64,
    pub external_id: Uuid,
    pub answer: String,
    pub debate: i64,
    pub author: i64,
    pub creation_time: DateTime<Utc>,
    pub semantics: AnswerSemantics,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "answers"]
pub struct NewAnswer {
    pub external_id: Uuid,
    pub answer: String,
    pub debate: i64,
    pub author: i64,
    pub creation_time: DateTime<Utc>,
    pub semantics: AnswerSemantics,
}

impl NewAnswer {
    pub fn with_id(self, id: i64) -> Answer {
        Answer {
            id,
            external_id: self.external_id,
            answer: self.answer,
            debate: self.debate,
            author: self.author,
            creation_time: self.creation_time,
            semantics: self.semantics,
        }
    }
}
