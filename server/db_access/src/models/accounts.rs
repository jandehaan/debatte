use chrono::{DateTime, Utc};
use std::convert::TryFrom;
use uuid::Uuid;

use crate::models::types::Locale;
use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, QueryableByName, AsChangeset)]
#[table_name = "accounts"]
pub struct RawAccount {
    pub id: i64,
    pub external_id: Uuid,
    pub email: Option<String>,
    pub salt: Option<String>,
    pub password_hash: Option<String>,
    pub name: String,
    pub email_verified: bool,
    pub locale: Locale,
    pub created: DateTime<Utc>,
    pub temporary: bool,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "accounts"]
pub struct NewRawAccount {
    pub external_id: Uuid,
    pub email: Option<String>,
    pub salt: Option<String>,
    pub password_hash: Option<String>,
    pub name: String,
    pub email_verified: bool,
    pub locale: Locale,
    pub created: DateTime<Utc>,
    pub temporary: bool,
}

impl NewRawAccount {
    pub fn with_id(self, id: i64) -> RawAccount {
        RawAccount {
            id,
            external_id: self.external_id,
            email: self.email,
            salt: self.salt,
            password_hash: self.password_hash,
            name: self.name,
            email_verified: self.email_verified,
            locale: self.locale,
            created: self.created,
            temporary: self.temporary,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Account {
    pub id: i64,
    pub external_id: Uuid,
    pub email: String,
    pub salt: Option<String>,
    pub password_hash: Option<String>,
    pub name: String,
    pub email_verified: bool,
    pub locale: Locale,
    pub created: DateTime<Utc>,
    pub temporary: bool,
}

impl From<Account> for RawAccount {
    fn from(account: Account) -> RawAccount {
        RawAccount {
            id: account.id,
            external_id: account.external_id,
            email: Some(account.email),
            salt: account.salt,
            password_hash: account.password_hash,
            name: account.name,
            email_verified: account.email_verified,
            locale: account.locale,
            created: account.created,
            temporary: account.temporary,
        }
    }
}

impl TryFrom<RawAccount> for Account {
    type Error = RawAccount;

    fn try_from(raw_account: RawAccount) -> Result<Account, Self::Error> {
        match raw_account.email {
            Some(email) => Ok(Account {
                id: raw_account.id,
                external_id: raw_account.external_id,
                email,
                salt: raw_account.salt,
                password_hash: raw_account.password_hash,
                name: raw_account.name,
                email_verified: raw_account.email_verified,
                locale: raw_account.locale,
                created: raw_account.created,
                temporary: raw_account.temporary,
            }),
            None => Err(raw_account),
        }
    }
}

#[derive(Debug, Clone)]
pub struct NewAccount {
    pub external_id: Uuid,
    pub email: String,
    pub salt: Option<String>,
    pub password_hash: Option<String>,
    pub name: String,
    pub email_verified: bool,
    pub locale: Locale,
    pub created: DateTime<Utc>,
    pub temporary: bool,
}

impl From<NewAccount> for NewRawAccount {
    fn from(new_account: NewAccount) -> NewRawAccount {
        NewRawAccount {
            external_id: new_account.external_id,
            email: Some(new_account.email),
            salt: new_account.salt,
            password_hash: new_account.password_hash,
            name: new_account.name,
            email_verified: new_account.email_verified,
            locale: new_account.locale,
            created: new_account.created,
            temporary: new_account.temporary,
        }
    }
}

#[derive(Debug, Clone)]
pub struct OtherAccount {
    pub external_id: Uuid,
    pub name: String,
}

impl From<Account> for OtherAccount {
    fn from(account: Account) -> OtherAccount {
        OtherAccount {
            external_id: account.external_id,
            name: account.name,
        }
    }
}

impl From<RawAccount> for OtherAccount {
    fn from(raw_account: RawAccount) -> OtherAccount {
        OtherAccount {
            external_id: raw_account.external_id,
            name: raw_account.name,
        }
    }
}
