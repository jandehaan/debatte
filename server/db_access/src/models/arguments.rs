use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, Associations)]
pub struct Argument {
    pub id: i64,
    pub external_id: Uuid,
    pub debate: i64,
    pub author: i64,
    pub creation_time: DateTime<Utc>,
    pub heading: String,
    pub body: String,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "arguments"]
pub struct NewArgument {
    pub external_id: Uuid,
    pub debate: i64,
    pub author: i64,
    pub creation_time: DateTime<Utc>,
    pub heading: String,
    pub body: String,
}

impl NewArgument {
    pub fn with_id(self, id: i64) -> Argument {
        Argument {
            id,
            external_id: self.external_id,
            heading: self.heading,
            body: self.body,
            debate: self.debate,
            author: self.author,
            creation_time: self.creation_time,
        }
    }
}
