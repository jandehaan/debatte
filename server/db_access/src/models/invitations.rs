use uuid::Uuid;

use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct Invitation {
    pub id: i64,
    pub external_id: Uuid,
    pub invitee: i64,
    pub debate: i64,
    pub invited_by: i64,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "invitations"]
pub struct NewInvitation {
    pub external_id: Uuid,
    pub invitee: i64,
    pub debate: i64,
    pub invited_by: i64,
}

impl NewInvitation {
    pub fn with_id(self, id: i64) -> Invitation {
        Invitation {
            id,
            external_id: self.external_id,
            invitee: self.invitee,
            debate: self.debate,
            invited_by: self.invited_by,
        }
    }
}
