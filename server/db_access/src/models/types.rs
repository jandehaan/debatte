use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DebateState {
    CollectingAnswers,
    Debating,
    Voting,
    Finished,
    Combined,
    Waiting,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, DbEnum, Serialize, Deserialize, Hash)]
pub enum DebateKind {
    MeritOfArgument,
    RelevanceOfArgument,
    Other,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, DbEnum)]
pub enum AnswerSemantics {
    Yes,
    No,
    None,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, DbEnum, Serialize, Deserialize)]
pub enum Locale {
    #[db_rename = "en_US"]
    EnUS,
    #[db_rename = "de_DE"]
    DeDE,
}
