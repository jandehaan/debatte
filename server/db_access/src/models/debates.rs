use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::models::types::*;
use crate::schema::*;

pub use std::convert::TryFrom;

#[derive(Debug, Clone, Identifiable, Queryable, Associations)]
pub struct Debate {
    pub id: i64,
    pub external_id: Uuid,
    pub question: String,
    pub allow_adding_more_answers: bool,
    pub collecting_answers_start: Option<DateTime<Utc>>,
    pub debating_start: Option<DateTime<Utc>>,
    pub voting_start: Option<DateTime<Utc>>,
    pub voting_end: Option<DateTime<Utc>>,
    pub concerned_argument: Option<i64>,
    pub kind: DebateKind,
    pub details: Option<String>,
    pub creation_time: DateTime<Utc>,
    pub locale: Locale,
    pub time_zone: String,
    pub public_invite_link_token: Option<String>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "debates"]
pub struct NewDebate {
    pub external_id: Uuid,
    pub question: String,
    pub allow_adding_more_answers: bool,
    pub collecting_answers_start: Option<DateTime<Utc>>,
    pub debating_start: Option<DateTime<Utc>>,
    pub voting_start: Option<DateTime<Utc>>,
    pub voting_end: Option<DateTime<Utc>>,
    pub concerned_argument: Option<i64>,
    pub kind: DebateKind,
    pub details: Option<String>,
    pub creation_time: DateTime<Utc>,
    pub locale: Locale,
    pub time_zone: String,
    pub public_invite_link_token: Option<String>,
}

impl NewDebate {
    pub fn with_id(self, id: i64) -> Debate {
        Debate {
            id,
            external_id: self.external_id,
            question: self.question,
            details: self.details,
            allow_adding_more_answers: self.allow_adding_more_answers,
            collecting_answers_start: self.collecting_answers_start,
            debating_start: self.debating_start,
            voting_start: self.voting_start,
            voting_end: self.voting_end,
            concerned_argument: self.concerned_argument,
            kind: self.kind,
            creation_time: self.creation_time,
            locale: self.locale,
            time_zone: self.time_zone,
            public_invite_link_token: self.public_invite_link_token,
        }
    }
}

#[derive(Debug)]
pub struct TopLevelDebate {
    pub id: i64,
    pub external_id: Uuid,
    pub question: String,
    pub allow_adding_more_answers: bool,
    pub collecting_answers_start: DateTime<Utc>,
    pub debating_start: DateTime<Utc>,
    pub voting_start: DateTime<Utc>,
    pub voting_end: DateTime<Utc>,
    pub kind: DebateKind,
    pub details: String,
    pub creation_time: DateTime<Utc>,
    pub locale: Locale,
    pub time_zone: String,
    pub public_invite_link_token: Option<String>,
}

impl From<TopLevelDebate> for Debate {
    fn from(top_level_debate: TopLevelDebate) -> Debate {
        Debate {
            id: top_level_debate.id,
            external_id: top_level_debate.external_id,
            question: top_level_debate.question,
            details: Some(top_level_debate.details),
            allow_adding_more_answers: top_level_debate.allow_adding_more_answers,
            collecting_answers_start: Some(top_level_debate.collecting_answers_start),
            debating_start: Some(top_level_debate.debating_start),
            voting_start: Some(top_level_debate.voting_start),
            voting_end: Some(top_level_debate.voting_end),
            concerned_argument: None,
            kind: top_level_debate.kind,
            creation_time: top_level_debate.creation_time,
            locale: top_level_debate.locale,
            time_zone: top_level_debate.time_zone,
            public_invite_link_token: top_level_debate.public_invite_link_token,
        }
    }
}

impl TryFrom<Debate> for TopLevelDebate {
    type Error = ();

    fn try_from(debate: Debate) -> Result<TopLevelDebate, Self::Error> {
        let details = debate.details.ok_or(())?;
        let collecting_answers_start = debate.collecting_answers_start.ok_or(())?;
        let debating_start = debate.debating_start.ok_or(())?;
        let voting_start = debate.voting_start.ok_or(())?;
        let voting_end = debate.voting_end.ok_or(())?;
        Ok(TopLevelDebate {
            id: debate.id,
            external_id: debate.external_id,
            question: debate.question,
            details,
            allow_adding_more_answers: debate.allow_adding_more_answers,
            collecting_answers_start,
            debating_start,
            voting_start,
            voting_end,
            kind: debate.kind,
            creation_time: debate.creation_time,
            locale: debate.locale,
            time_zone: debate.time_zone,
            public_invite_link_token: debate.public_invite_link_token,
        })
    }
}
