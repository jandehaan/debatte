use chrono::{DateTime, Utc};

use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct InviteLink {
    pub id: i64,
    pub debate: i64,
    pub email: String,
    pub name: String,
    pub proof_token_hash: String,
    pub invited_time: DateTime<Utc>,
    pub invited_by: Option<i64>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "invite_links"]
pub struct NewInviteLink {
    pub debate: i64,
    pub email: String,
    pub name: String,
    pub proof_token_hash: String,
    pub invited_time: DateTime<Utc>,
    pub invited_by: Option<i64>,
}

impl NewInviteLink {
    pub fn with_id(self, id: i64) -> InviteLink {
        InviteLink {
            id,
            debate: self.debate,
            email: self.email,
            name: self.name,
            proof_token_hash: self.proof_token_hash,
            invited_time: self.invited_time,
            invited_by: self.invited_by,
        }
    }
}
