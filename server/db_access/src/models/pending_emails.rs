use chrono::{DateTime, Utc};

use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable)]
pub struct PendingEmail {
    pub id: i64,
    pub to: String,
    pub from: String,
    pub subject: String,
    pub body: String,
    pub pdf_attachment_name: Option<String>,
    pub pdf_attachment: Option<Vec<u8>>,
    pub created: DateTime<Utc>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "pending_emails"]
pub struct NewPendingEmail {
    pub to: String,
    pub from: String,
    pub subject: String,
    pub body: String,
    pub pdf_attachment_name: Option<String>,
    pub pdf_attachment: Option<Vec<u8>>,
    pub created: DateTime<Utc>,
}

impl NewPendingEmail {
    pub fn with_id(self, id: i64) -> PendingEmail {
        PendingEmail {
            id,
            to: self.to,
            from: self.from,
            subject: self.subject,
            body: self.body,
            pdf_attachment_name: self.pdf_attachment_name,
            pdf_attachment: self.pdf_attachment,
            created: self.created,
        }
    }
}
