use chrono::{DateTime, Utc};

use crate::models::accounts::*;
use crate::schema::*;

#[derive(Debug, Clone, Identifiable, Queryable, Associations)]
#[belongs_to(Account)]
pub struct PasswordReset {
    pub id: i64,
    pub proof_token_hash: String,
    pub account_id: i64,
    pub started_time: DateTime<Utc>,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "password_resets"]
pub struct NewPasswordReset {
    pub proof_token_hash: String,
    pub account_id: i64,
    pub started_time: DateTime<Utc>,
}

impl NewPasswordReset {
    pub fn with_id(self, id: i64) -> PasswordReset {
        PasswordReset {
            id,
            proof_token_hash: self.proof_token_hash,
            account_id: self.account_id,
            started_time: self.started_time,
        }
    }
}
