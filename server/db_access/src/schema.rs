use crate::models::types::*;

table! {
    use diesel::sql_types::{Int8, Uuid, Text, Bool, Nullable, Timestamptz};
    use super::LocaleMapping;

    accounts (id) {
        id -> Int8,
        external_id -> Uuid,
        email -> Nullable<Text>,
        salt -> Nullable<Text>,
        password_hash -> Nullable<Text>,
        name -> Text,
        email_verified -> Bool,
        locale -> LocaleMapping,
        created -> Timestamptz,
        temporary -> Bool,
    }
}

table! {
    use diesel::sql_types::{Int8, Uuid, Text, Timestamptz};
    use super::AnswerSemanticsMapping;

    answers (id) {
        id -> Int8,
        external_id -> Uuid,
        answer -> Text,
        debate -> Int8,
        author -> Int8,
        creation_time -> Timestamptz,
        semantics -> AnswerSemanticsMapping,
    }
}

table! {
    arguments (id) {
        id -> Int8,
        external_id -> Uuid,
        debate -> Int8,
        author -> Int8,
        creation_time -> Timestamptz,
        heading -> Text,
        body -> Text,
    }
}

table! {
    argument_weights (id) {
        id -> Int8,
        account -> Int8,
        argument -> Int8,
        weight -> Int4,
    }
}

table! {
    auth_tokens (id) {
        id -> Int8,
        auth_token_hash -> Text,
        account_id -> Int8,
        issued_time -> Timestamptz,
    }
}

table! {
    ballots (id) {
        id -> Int8,
        external_id -> Uuid,
        debate -> Int8,
        voter -> Int8,
        prepared_time -> Nullable<Timestamptz>,
    }
}

table! {
    use diesel::sql_types::{Int8, Uuid, Text, Bool, Timestamptz, Nullable};
    use super::{DebateKindMapping, LocaleMapping};

    debates (id) {
        id -> Int8,
        external_id -> Uuid,
        question -> Text,
        allow_adding_more_answers -> Bool,
        collecting_answers_start -> Nullable<Timestamptz>,
        debating_start -> Nullable<Timestamptz>,
        voting_start -> Nullable<Timestamptz>,
        voting_end -> Nullable<Timestamptz>,
        concerned_argument -> Nullable<Int8>,
        kind -> DebateKindMapping,
        details -> Nullable<Text>,
        creation_time -> Timestamptz,
        locale -> LocaleMapping,
        time_zone -> Text,
        public_invite_link_token -> Nullable<Text>,
    }
}

table! {
    debate_summaries (debate) {
        debate -> Int8,
        pdf -> Bytea,
        json -> Text,
    }
}

table! {
    login_links (id) {
        id -> Int8,
        proof_token_hash -> Text,
        account_id -> Int8,
        issued_time -> Timestamptz,
    }
}

table! {
    invitations (id) {
        id -> Int8,
        external_id -> Uuid,
        invitee -> Int8,
        debate -> Int8,
        invited_by -> Int8,
    }
}

table! {
    invite_links (id) {
        id -> Int8,
        debate -> Int8,
        email -> Text,
        name -> Text,
        proof_token_hash -> Text,
        invited_time -> Timestamptz,
        invited_by -> Nullable<Int8>,
    }
}

table! {
    partial_ballots (id) {
        id -> Int8,
        answer -> Int8,
        rank -> Int4,
        ballot -> Int8,
    }
}

table! {
    participations (id) {
        id -> Int8,
        debate -> Int8,
        participant -> Int8,
    }
}

table! {
    password_resets (id) {
        id -> Int8,
        proof_token_hash -> Text,
        account_id -> Int8,
        started_time -> Timestamptz,
    }
}

table! {
    pending_emails (id) {
        id -> Int8,
        to -> Text,
        from -> Text,
        subject -> Text,
        body -> Text,
        pdf_attachment_name -> Nullable<Text>,
        pdf_attachment -> Nullable<Bytea>,
        created -> Timestamptz,
    }
}

table! {
    supports (id) {
        id -> Int8,
        argument -> Int8,
        answer -> Int8,
    }
}

table! {
    verify_email_links (id) {
        id -> Int8,
        proof_token_hash -> Text,
        account_id -> Int8,
        issued_time -> Timestamptz,
    }
}

table! {
    votes (id) {
        id -> Int8,
        external_id -> Uuid,
        voter -> Int8,
        debate -> Int8,
    }
}

joinable!(answers -> accounts (author));
joinable!(answers -> debates (debate));
joinable!(argument_weights -> accounts (account));
joinable!(argument_weights -> arguments (argument));
joinable!(arguments -> accounts (author));
joinable!(arguments -> debates (debate));
joinable!(auth_tokens -> accounts (account_id));
joinable!(ballots -> accounts (voter));
joinable!(ballots -> debates (debate));
joinable!(debate_summaries -> debates (debate));
joinable!(invitations -> debates (debate));
joinable!(login_links -> accounts (account_id));
joinable!(partial_ballots -> answers (answer));
joinable!(partial_ballots -> ballots (ballot));
joinable!(participations -> accounts (participant));
joinable!(participations -> debates (debate));
joinable!(password_resets -> accounts (account_id));
joinable!(supports -> answers (answer));
joinable!(supports -> arguments (argument));
joinable!(verify_email_links -> accounts (account_id));
joinable!(votes -> accounts (voter));
joinable!(votes -> debates (debate));

allow_tables_to_appear_in_same_query!(
    accounts,
    answers,
    arguments,
    argument_weights,
    auth_tokens,
    ballots,
    debates,
    debate_summaries,
    invitations,
    invite_links,
    login_links,
    partial_ballots,
    participations,
    password_resets,
    supports,
    verify_email_links,
    votes,
);
