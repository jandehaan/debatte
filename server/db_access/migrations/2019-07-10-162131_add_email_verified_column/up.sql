alter table "accounts" add column "email_verified" boolean;
update "accounts" set "email_verified" = false;
alter table "accounts" alter column "email_verified" set not null;
