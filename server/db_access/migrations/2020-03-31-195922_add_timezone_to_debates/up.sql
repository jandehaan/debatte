alter table "debates" add column "time_zone" text;
update "debates" set "time_zone" = 'Europe/Berlin';
alter table "debates" alter column "time_zone" set not null;
