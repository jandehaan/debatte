create type "internal_debate_state" as enum (
    'implicit',
    'combined'
);
alter table "debates" add column "state" "internal_debate_state";
update "debates" set "state" = 'implicit' where "concerned_argument" is null;
update "debates" set "state" = 'combined' where "concerned_argument" is not null;
alter table "debates" alter column "state" set not null;
