create type "answer_semantics" as enum (
    'none',
    'yes',
    'no'
);

alter table "answers" add column "semantics" "answer_semantics";
update "answers" set "semantics" = 'yes' where "answer" = 'Yes';
update "answers" set "semantics" = 'no' where "answer" = 'No';
update "answers" set "semantics" = 'none' where not ("answer" = 'Yes' or "answer" = 'No');
alter table "answers" alter column "semantics" set not null;
