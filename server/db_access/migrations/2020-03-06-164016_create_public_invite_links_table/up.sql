create table "public_invite_links" (
    "id" bigserial primary key,
    "debate" bigserial not null,
    "proof_token_hash" text not null,
    "created" timestamp with time zone not null,
    "created_by" bigserial not null
);
