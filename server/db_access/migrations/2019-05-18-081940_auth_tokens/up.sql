create table "auth_tokens" (
    "id" bigserial primary key,
    "auth_token_hash" text not null,
    "account_id" bigserial references "accounts"("id"),
    "issued_time" timestamp with time zone not null
);
