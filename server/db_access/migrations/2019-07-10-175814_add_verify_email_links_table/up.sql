create table "verify_email_links" (
    "id" bigserial primary key,
    "proof_token_hash" text not null,
    "account_id" bigserial not null references "accounts"("id"),
    "issued_time" timestamp with time zone not null
);
