alter table "debates" add column "creation_time" timestamp with time zone;
update "debates" set "creation_time" = "collecting_answers_start";
alter table "debates" alter column "creation_time" set not null;

alter table "answers" add column "creation_time" timestamp with time zone;
update "answers" set "creation_time" = 'now';
alter table "answers" alter column "creation_time" set not null;

alter table "arguments" add column "creation_time" timestamp with time zone;
update "arguments" set "creation_time" = 'now';
alter table "arguments" alter column "creation_time" set not null;
