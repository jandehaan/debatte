create type "locale" as enum (
    'en_US',
    'de_DE'
);

alter table "accounts" add column "locale" "locale";
update "accounts" set "locale" = 'en_US';
alter table "accounts" alter column "locale" set not null;
