create table "argument_weights" (
    "id" bigserial primary key,
    "account" bigint not null references "accounts"("id"),
    "argument" bigint not null references "arguments"("id"),
    "weight" int not null
);
