alter table "debates" add column "locale" "locale";
update "debates" set "locale" = 'en_US';
alter table "debates" alter column "locale" set not null;
