create table "debate_summaries" (
    "debate" bigserial primary key references "debates"("id"),
    "pdf" bytea not null,
    "json" text not null
);
