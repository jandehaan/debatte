create table "invitations" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "invitee" bigserial not null references "accounts"("id"),
    "debate" bigserial not null references "debates"("id"),
    "invited_by" bigserial not null references "accounts"("id")
);
