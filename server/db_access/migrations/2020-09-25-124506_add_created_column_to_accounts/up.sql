alter table "accounts" add column "created" timestamp with time zone;
update "accounts" set "created" = (select current_timestamp);
alter table "accounts" alter column "created" set not null;
