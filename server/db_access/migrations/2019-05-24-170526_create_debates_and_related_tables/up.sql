create type "internal_debate_state" as enum (
    'implicit',
    'combined'
);

create type "debate_kind" as enum (
    'merit_of_argument',
    'relevance_of_argument',
    'other'
);

create table "debates" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "question" text not null,
    "state" "internal_debate_state" not null,
    "allow_adding_more_answers" boolean not null,
    "anonymous_voting" boolean not null,
    "collecting_answers_start" timestamp with time zone not null,
    "debating_start" timestamp with time zone not null,
    "voting_start" timestamp with time zone not null,
    "voting_end" timestamp with time zone not null,
    "concerned_argument" bigint,
    "kind" "debate_kind" not null
);

create table "arguments" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "heading" text not null,
    "body" text not null,
    "debate" bigint not null references "debates"("id"),
    "author" bigint not null references "accounts"("id")
);

alter table "debates" add constraint "debates_concerned_argument_fkey" foreign key ("concerned_argument") references "arguments"("id");

create table "participations" (
    "id" bigserial primary key,
    "debate" bigint not null references "debates"("id"),
    "participant" bigint not null references "accounts"("id")
);

create table "answers" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "answer" text not null,
    "debate" bigint not null references "debates"("id"),
    "author" bigint not null references "accounts"("id")
);

create table "supports" (
    "id" bigserial primary key,
    "argument" bigint not null references "arguments"("id"),
    "answer" bigint not null references "answers"("id")
);

create table "ballots" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "debate" bigint not null references "debates"("id"),
    "voter" bigint references "accounts"("id")
);

create table "partial_ballots" (
    "id" bigserial primary key,
    "answer" bigint not null references "answers"("id"),
    "rank" int not null,
    "ballot" bigint not null references "ballots"("id")
);

create table "votes" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "voter" bigint not null references "accounts"("id"),
    "debate" bigint not null references "debates"("id")
);
