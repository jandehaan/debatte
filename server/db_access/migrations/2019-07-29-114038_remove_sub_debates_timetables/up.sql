alter table "debates" alter column "collecting_answers_start" drop not null;
alter table "debates" alter column "debating_start" drop not null;
alter table "debates" alter column "voting_start" drop not null;
alter table "debates" alter column "voting_end" drop not null;
alter table "debates" alter column "anonymous_voting" drop not null;

update "debates"
    set "collecting_answers_start" = null,
    "debating_start" = null,
    "voting_start" = null,
    "voting_end" = null,
    "anonymous_voting" = null
    where "concerned_argument" is not null;
