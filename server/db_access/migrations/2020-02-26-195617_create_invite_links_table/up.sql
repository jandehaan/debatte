create table "invite_links" (
    "id" bigserial primary key,
    "debate" bigserial not null,
    "email" text not null,
    "name" text not null,
    "proof_token_hash" text not null,
    "invited_time" timestamp with time zone not null,
    "invited_by" bigserial not null
);
