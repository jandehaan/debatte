insert into "accounts" ("id", "external_id", "email", "salt", "password_hash", "name", "email_verified", "locale") values (-1, '6141059e-fc26-11ea-8dfb-5b24ffbdc358', 'eltrot@jandehaan.name', null, null, 'Unknown', true, 'en_US');
update "ballots" set "voter" = -1 where "voter" is null;
alter table "ballots" alter "voter" set not null;