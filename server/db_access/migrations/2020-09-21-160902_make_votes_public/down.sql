alter table "ballots" alter "voter" drop not null;
update "ballots" set "voter" = null where "voter" = -1;
delete from "accounts" where "id" = -1;
