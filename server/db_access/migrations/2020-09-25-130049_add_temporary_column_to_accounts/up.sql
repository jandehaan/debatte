alter table "accounts" add column "temporary" boolean;
update "accounts" set "temporary" = false;
alter table "accounts" alter column "temporary" set not null;
