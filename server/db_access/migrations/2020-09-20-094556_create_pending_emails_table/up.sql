create table "pending_emails" (
    "id" bigserial primary key,
    "to" text not null,
    "from" text not null,
    "subject" text not null,
    "body" text not null,
    "pdf_attachment_name" text,
    "pdf_attachment" bytea,
    "created" timestamp with time zone not null
);