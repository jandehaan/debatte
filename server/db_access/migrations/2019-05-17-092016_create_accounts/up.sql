create table "accounts" (
    "id" bigserial primary key,
    "external_id" uuid not null,
    "email" text not null unique,
    "salt" text,
    "password_hash" text,
    "name" text not null
);
