alter table "debates" add column "anonymous_voting" boolean;
update "debates" set "anonymous_voting" = true;
alter table "debates" alter column "anonymous_voting" set not null;
