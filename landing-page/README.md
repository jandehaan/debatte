# Eltrot: Landing page

## Build

Run `npm run build`.

## Develop

Use `npm run serve` to rebuild files when they are changed and serve them on a local web server.