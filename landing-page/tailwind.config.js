module.exports = {
  prefix: '',
  important: false,
  separator: ':',
  theme: {
    colors: {
      transparent: 'transparent',
      black: '#102020',
      realblack: '#000000',
      white: '#fff',
      main: {
        100: '#00c85d80',
        300: '#00c85d',
        500: '#008940',
        700: '#005a2a',
      },
      accent: {
        300: '#bee3e6',
        500: '#9fd5d9',
        700: '#7ec6cc',
      },
      gray: {
        400: '#cbd5e0',
        500: '#a0aec0',
        600: '#718096',
      },
      red: {
        500: '#f56565',
      },

    },
    fontFamily: {
      sans: [
        '"Source Sans Pro"',
        'sans-serif'
      ],
    },
    corePlugins: {},
    plugins: [],
  }
}
