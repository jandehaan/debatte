module.exports = {
    defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
    content: ['**/*.html'],
    css: ["dist/style.raw.css"],
}