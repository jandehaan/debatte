#!/bin/sh

mkdir dist
postcss src/style.css -o dist/style.raw.css 
purgecss --config purgecss.config.js --out dist
mv dist/style.raw.css dist/style.css
mkdir dist/de
mkdir dist/en
cp src/de/*.html dist/de
cp src/en/*.html dist/en
cp -r src/assets dist/assets
cp -r src/favicon.ico dist/favicon.ico