# Design

## Goals

The design should not draw any attention to it while using the app.
It may stand out on the splash screen or in some decorative elements,
but there should only be very few of those.

## Elements

![Overview of design elements](./6-design.svg)

### Name

The name of the application is »Eltrot« (the name of a plant also known as common hogweed).

### Text

All text should be at a readable size of at least 12pt.
If some text is not important, its size should nonetheless not be reduced below 12pt.
Instead, use less saturated colors to convey this.

Font: Source Sans Pro

### Colors

Only a few different colors are used:

- Very dark gray for most text (slightly green-blue): `#102020`
- White as a background: `#000000`
- A main color for e.g. buttons: `#008940` (greenish).
Additionally, a lighter (`#00c85d`) and a darker (`#005a2a`) shade of this color may be used.
- An accent color: `#9fd5d9` (bluish)
Additionally, a lighter (`#bee3e6`) and a darker (`#7ec6cc`) shade of this color may be used.

### Visual hierarchy

There may be some exceptions to these rules if that leads to a clearly superior design.

- Everything that can be clicked uses the primary color.
- The accent color is only used for emphasis of non-clickable things.
- Content (i.e. anything that has been entered by a user) is never bold.
- Emphasis of non-content may be expressed by using boldness.
- Emphasis of content should mainly be expressed through font size.

## UI Elements

### Buttons

There are two kinds of buttons:

- Buttons that have enough space around them and are the main focus of the general area they occupy (»regular buttons«).
- Buttons that mark actions that are closely tied to the parent UI element and would stand out too much if they looked the same as regular buttons,
distracting from the content of the parent (»auxiliary buttons«).

All buttons should

- respond when hovering above them with the pointer
- look clickable – there should be some elements of the design that distinguish buttons from other UI elements

This is achieved in two different ways for the two different kinds of buttons:

- Regular buttons
    - rectangular with slightly rounded corners
    - filled with the main color
    - white text in caps, regular weight
- Auxiliary buttons: 
    - only text
    - text color is the accent color, in caps, bold

### Decorative elements

There should be no elements that have no purpose but creating a certain look.
That should instead be accomplished by changing elements that would be needed for other reasons to serve this purpose as well.
