# Implementation

## Client

The client is a web page that uses Typescript and Vue.

## Server

The server is written in Rust using Rocket;
data is stored in a Postgres database.
Diesel is used as an ORM.

### API

The API uses GraphQL.
On the server side, the Rust library [Juniper](https://github.com/graphql-rust/juniper) is used.
The GraphQL-API is used from the client side by creating Typescript objects from the answers.

### Sending Emails

Emails are sent using [Postmark](postmarkapp.com) and their API.

## Infrastructure

Both the client and the server can be built in three configurations:
development, staging and production.
The development configuration is meant to run locally when developing.
The staging configuration is meant to run on a set of servers that is separate from the production servers.
It is used to check whether deploying a new version to the production servers would cause any problems.
The production configuration runs on the production servers, which are exposed to users.

The git repository contains two long-lived branches:
The `master` branch is the version of the app that is currently in production
or that is being tested in the staging environment.
A tag is added to commits that have been successfully tested in the staging environment,
which are then deployed to production.
The `development` branch contains the version that is currently in development,
but it is not always ready to be deployed to a »real«, i.e. staging or production, server.
It should, however, always be able to be built and pass all tests.
Every commit to this branch is automatically built and tested.
