# Create an Account

## Regular account creation

To create a regular account, the user has to enter:

- their name to display in debates,
- an email to identify them,
- a password (twice).

After pressing the button labelled »Create an Account«,
the app will display a pop-up telling the user that their account is being created.
If the email address is already in use,
an error message »This email is already in use« is displayed.
There is only one constraint on the password:
It has to be at least 12 characters long.
If this requirement is not met after the user proceeds to another field of the form,
the password field will turn red and a small error message
(»The password has to be at least 12 characters long«)
is displayed below the password field.
If the second password does not match the first one,
the second password field turns red and a small error message
(»The two passwords do not match«)
is displayed below the second password field.

When the server has created the account,
the user is sent an email with a link and a popup is displayed telling them that they need to confirm their email address:

> An email has been sent to your email address.
>
> Please click the link in the email to continue.

After clicking the link, the user will be logged in and sent to the Overview of Debates screen.

![Regular account creation mock-up](./create-an-account-password.svg)

## Quick account creation

If a user clicks an invitation link to a debate and they are not logged in yet,
they will have to create an account before being able to participate in the debate.
They are not sent to the regular account creation page,
but to a quicker version,
which only requires a name and an email.

After entering their name and email and pressing the »Get Started« button,
the user is sent an email containing a link.
The app displays a pop-up:

> An email has been sent to your email address.
>
> It might take a moment for it to arrive.
>
> When it does, click the link in the email to get started.

When the user clicks the link, they are logged in and sent to their destination.

If the provided email is already in use,
a popup will be displayed
(»This email is already in use«).
Then, the user is returned to the form.

![Quick account creation mock-up](./create-an-account-quick.svg)

## Technical note

The links to verify emails have the following format:

```
https://eltrot.jandehaan.name/verify-email/<token>
```

The links to log in have this format:

```
https://eltrot.jandehaan.name/login/<token>
```

In both cases,
`<token>` is some string that the server can use to identify the link and prove that it was created by the server.
