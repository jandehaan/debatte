# Invite Participants

There are two kinds of participants:
Those that the user has already debated with and others.
The form to invite participants consists of:

- a search box
- a list of known participants, each consisting of
    - their profile picture
    - their name
    - a button to invite them
- a text box for an email address
- a button to invite a participant via email
- a button to create an invitation link

The search box shows users whose names match the input.
The email addresses of these users are not used in any way.

After entering an email address and pressing the button to invite the person it belongs to,
an email will be sent to the address if there is no account associated with it.
If an account exists, the user will get a notification as if they had been invited regularly.

The regularly invited users get a notification in their profile section that they can click to join the debate.

When the button to create an invitation link is clicked,
the user is asked (popup) whether every participant’s participation must be confirmed by them.
Then, a link is displayed in a popup with buttons to copy and share it.

![Invite Participants mock-up](./invite-participants.svg)
