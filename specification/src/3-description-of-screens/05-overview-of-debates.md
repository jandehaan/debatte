# Overview of Debates

The user can choose whether they want to view »Top-Level« or »All« debates with a segmented control.
They are then shown a list of debates that they are participating in.
Each debate is displayed as a card that contains the title of the debate, emphasized.
Debates are ordered by when the user last participated in them.

When only top-level debates are being displayed,
the card contains the detailed explanation and the possible conclusions.
At the bottom right of these cards there is a link »view«.
Clicking anywhere on these card sends the user to the View a Debate screen.

![Overview of top-level debates mock-up](./overview-of-debates-top-level.svg)

When all debates are shown,
the card contains the question and detailed explanation as well as a tree of sub-debates, to a depth of 2.
If there are more deeply nested debates,
a button to display those as well (»Show more debates«) is shown.
If there are more sub-debates in total than fit on one and a half phone screens,
a button to show more sub-debates will be displayed at the bottom.
Hierarchies are expanded more deeply than two levels if there is an event concerning a sub-debate.

![Overview of all debates mock-up](./overview-of-debates-all.svg)

There is a search box at the very top of the screen.
When it is used, the view switches to »All« automatically.
Debates whose question, details or arguments contain the search term are shown and highlighted.

If there are no debates, the text »No debates to show« and a button »start a debate«,
which sends the user to the Start a Debate screen, are shown.

![Overview of debates (empty) mock-up](./overview-of-debates-empty.svg)

## Details: Last participation

Events that are considered to be a participation
(and therefore influence the order in which debates are displayed),
and the debate they are considered to belong to,
are the following:

- Create a debate
- Add an answer
- Add an argument
