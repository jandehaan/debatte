# Login

There are two ways for users to log in:

1. A regular combination of email and password
2. Just an email address which has been used before:
An email containing a link to log in is sent to this address.

Therefore, the login form consists of the following elements:

- A field for the email address (placeholder: »email«).
- A field for the password (placeholder: »password«).
- A button to log in using email and password (text: »Log in with password«).
- A link to reset the password (text: »reset password«).
- A button to log in using only the email address (text: »Send an email to log in without a password«).

First, all fields are empty.

As soon as the user starts entering a password, the button to log in using only the email address is grayed out.

When the user clicks the button to log in using email and password,
the app displays a popup showing that the process of logging in is going on.
If the login is successful,
the user will be sent to their destination page.
Usually, this is the overview of debates,
but it may also be single debate if the user has clicked a special link.
If the login fails,
an error message will be displayed (»Username unknown or password incorrect«),
the contents of the fields will be kept and
the user will be allowed to attempt to log in again.

When the user clicks the button to log in by being sent an email,
the app displays a popup saying that the email has been sent (only after it has actually been sent) and
displays instructions for the user on what to do next:

> An email has been sent to your email address.
>
> It might take a moment for it to arrive.
>
> When it does, click the link in the email to log in.

If the email that is entered is associated with an account that is protected using a password,
a message will be displayed
(»This account is protected with a password. Please use it to log in.«).

The link that is sent to the user’s email sends them to their destination page.

![Login mock-up](./login.svg)
