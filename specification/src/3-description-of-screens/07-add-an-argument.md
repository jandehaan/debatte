# Add an Argument

The form for adding an argument to a debate consists of the following elements, from top to bottom:

1. The main question of the debate and a subheading »Your argument«
2. Label »Heading«
3. Text field (max. 140 characters)
4. Label »Body«
5. Large text field (unlimited, expands to display its entire content)
6. List of answers of the debate, each with a check box.
The column with the check boxes is labelled »Supports«
7. A button »Add«

![Add an Argument mock-up](./add-an-argument-1.svg)
![Add an Argument mock-up (scrolled)](./add-an-argument-2.svg)
