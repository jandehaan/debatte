# My Profile

The screen consists of the following elements:

- a button to change the profile picture
- the user’s name
- a button to change the user’s name
- the user’s email
- a button to log out
- a list of notifications

![My Profile mock-up](./my-profile.svg)
