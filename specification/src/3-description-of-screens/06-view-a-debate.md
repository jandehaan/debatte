# View a Debate

A top-level debate can be in one of four states:

1. Collection of answers:
Participants can add answers to the main question.
2. Debate (collection of arguments):
Participants can add arguments and start new debates about arguments that have already been added.
No new answers can be added.
3. Voting:
No new arguments can be added.
Participants can vote.
4. Finished:
The results of the vote are final.

Debates that concern other arguments do not have these four distinct states.
No answers can be added, voting and adding arguments are simultaneous.
Intermediate voting results are always shown.

## Top-Level Debate

At the top of the screen, a progress bar with four points is shown.
The number of reached points from the left is equal to the number of the state of the debate.
Above this line, the current state of the debate is displayed
(»Collecting answers«, »Debating«, »Voting«, »Finished«).
This bar is only shown once here, but should be displayed at all times on screens displaying a top-level debate.

![View a debate progress bar](./view-a-debate-progress-bar.svg)

### Collection of answers

The main question of the debate is shown at the top,
followed by a more detailed description of the question
(this is not shown on all mock-ups, but should always be displayed).
The possible answers that have been added so far are shown below.
A button to add an answer is shown at the bottom.

![View a debate (collection of answers) mock-up](./view-a-debate-collection-of-answers.svg)

### Debate

Instead of the button to add an answer,
there is a heading »arguments« and a list of arguments that have been added.
Every argument consists of a headline and a body.
Below the body, a list of all answer that are supported by the argument is shown.
Debates concerning an argument are displayed below the argument they concern.
Only the main questions of sub-debates are shown.
When clicked, the user is sent to a View a Debate screen showing that debate.
Below the list of debates about the argument, there is a button to start a debate about that argument.
Clicking it sends the user to the Start a Debate screen for debates about an argument.

There is a button to add an argument *at the very bottom* of the list of arguments.
This position encourages reading all other arguments before adding your own,
leading to fewer duplicates.

![View a debate (debate) mock-up](./view-a-debate-debate-1.svg)
![View a debate (debate) mock-up](./view-a-debate-debate-2.svg)

### Voting

At the top, a short explanation of how to vote is shown.
Below each argument, a slider with the title »weight of this argument« is shown.
It allows the user to select values between 0 and 5, as well as infinity.
The answers are displayed at the bottom to encourage considering the arguments and ordered according to the weights assigned by the user.
The heading above the answers says »Ballot« instead of »X Answers«.
The sum of the weights of the arguments supporting an answer is displayed to the right of the answer.
At the right, affordances for dragging are displayed.
After the order of answers has been changed manually,
changing the weight of arguments does not influence the ordering anymore.
Instead, a button »Order by argument weights« is shown.
Below the answers, there is a button »vote«.
The order of the answers can also be changed by dragging them to another spot in the list.

![View a debate (vote) mock-up](./view-a-debate-vote.svg)

#### Details of voting: Ranked-Pairs

A ballot specifies a complete ordering of all \\(n\\) answers.
The voting result is determined as follows (from [this paper](/Tideman-1987-Independence-of-Clones.pdf)):

1. Determine the ranking of all pairs, i.e. for every pair \\((x, y)\\) count the number of ballots where \\(x\\) is ranked ahead of \\(y\\) and order them by decreasing majorities.
2. Take a directed graph whose nodes are all possible answers. For each pair \\((x, y)\\), starting from the one with the greatest majority, add the edge \\((x, y)\\) if that does not lead to any cycles in the graph. Otherwise, do nothing.
3. The winner is the element \\(x\\) such that there is a path from \\(x\\) to every other answer \\(y\\) (the source of the graph).

### Finished

The answer that was selected is displayed with emphasis,
the other answers are displayed in the resulting order.
The arguments are also displayed.

## Debate Concerning an Argument

Because all four states are merged there is no progress bar and all buttons described in the last section are visible at the same time.
Participants can always vote, but their votes will not be put into the urn until the end of the Debating phase,
to ensure that they can change their vote when new arguments are added.

The lack of separation between the four states will cause answers, arguments and votes to become incoherent:
The person who added an argument may not have stated that it supports a new answer,
and votes may not have taken new arguments into consideration.

That is why a red dot with a number is shown to each argument, debate title and vote.
The number is the number of things that have to be reviewed to make everything consistent again.
Red dots are displayed when an argument has been added, for everyone who has voted next to the »Vote« button.

![View a debate concerning an argument mock-up](./view-a-debate-concerning-an-argument.svg)
