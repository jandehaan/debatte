# Splash Screen

The splash screen consists of the logo, the name of the application and something symbolizing that the app is loading.

![splash screen mock-up](./splash-screen.svg)
