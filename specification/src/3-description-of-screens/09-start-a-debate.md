# Start a Debate

## Starting a standalone debate

The form for starting a standalone debate is divided into three parts,
with buttons to continue to the next part and go back to previous parts:

1. »Question«
    - textbox for the question
    - textbox for a detailed explanation of the question
    - heading »answers«
    - button »add answer«
    - list of answers
    - checkbox: »participants may add more answers«

![Starting a standalone debate mock-up (1)](./start-a-debate-1.svg)

2. »Schedule«
    - table headings »phase«, »start/end«
    - table entries »add answers«, »debate« and »vote«
    - four date entry controls *between* the other table entries
    - checkbox: »anonymous voting«

![Starting a standalone debate mock-up (2)](./start-a-debate-2.svg)

3. »Invitations«
    - a segmented control to switch between:
        - »Contacts«: a list of known people,
        i.e. people who have participated in at least one debate the logged in user also participates in.
        There is a search box above that filters the list.
        - »Email«: an editable list of email addresses and names of people to invite.
        - »Links«: an editable list of links that allow people to join the debate.
        When creating a link, the user can choose whether every person joining the debate has to be approved individually.
        The link or whether approval is necessary cannot be changed after its creation.
    - button »add participant«
    - list of participants, each with their profile picture and name
    - button: »create debate«

![Starting a standalone debate mock-up (3)](./start-a-debate-3.svg)
![Starting a standalone debate mock-up (4)](./start-a-debate-4.svg)
![Starting a standalone debate mock-up (5)](./start-a-debate-5.svg)

## Starting a debate about an argument

If the debate is about an argument,
there are some changes to the form:

- The argument the debate is about is shown at the very top.
- there is an option picker for instead of an input box for the question and details:
»Does this argument have merit?«, »Is this argument relevant?«, »Does this argument support a certain answer?« for all answers
- The debate’s details consist of the argument the debate concerns.
- The two answers »Yes« and »No« are displayed,
the button for adding answers is disabled and
the checkbox for deciding whether participants may add their own answers is unchecked and not editable.
- Everything regarding the schedule and the participants is removed.
- The option whether voting is anonymous is inherited from the parent debate and cannot be changed.

![Starting a debate about an argument mock-up](./start-a-debate-about-an-argument.svg)
