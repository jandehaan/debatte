# Summaries of Debates

Each debate is kept in the database for 30 days after finishing voting. Immediately after finishing voting, all participants are sent an email containing a summary of the debate. Until the debate is deleted, a link to download this summary is displayed on the website as well.

The summary is produced using LaTeX and looks like this [example summary PDF](./4-example-summary/example.pdf).

The LaTeX source for this document is:

``` tex
{{#include 4-example-summary/example.tex}}
```
