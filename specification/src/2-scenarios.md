# Scenarios

## A large group of students deciding on a motto for their graduation

As is customary, a large group of (about 100) students wants a motto for their graduation that can be printed on t-shirts.
There are many different proposals and the issue is very important to many of the students,
who want to see their motto make it.
Two of the students are chosen to organize the collection of mottos and the final voting.
They start a debate using the application, posing the question: »What should be the motto for the graduation?«.
They add their first ideas for mottos as answers,
and decide that everyone will be allowed to add their own answers, but only for the first three days of the debate.
After that, they plan a week for discussion and then three days for voting.
They also decide that voting should be anonymous.

They invite everyone to participate by sending them a link to the debate.
The other students click the link and are asked for their email and name to identify them as the same person in the future,
and may choose whether they would like to be reminded when the voting phase starts.
They are sent an email with a link to the debate that stays valid for the duration of the debate.
After clicking it, they can participate in the debate.

One of the other students has an idea for a motto and adds it as an answer.
After a few days, they see that lots of mottos have been added.
To them, it is important that the motto incorporates the name of their school,
so they add an argument »This motto incorporates the name of our school«,
marking it as supporting all the mottos that do.
Soon, the student gets an email that it is now possible to vote on the mottos.
They click the link and assign weights to arguments that are important to them.
The order they are shown does not quite reflect how funny the student thinks some of the mottos are,
so they modify it to reflect that.
Then, they click vote.

After the voting period is over,
everyone gets an email containing the results of the vote.
