# Description of Screens

There is a navigation bar at the bottom of the following screens:

- Overview of Debates
- View a Debate
- Add an argument
- View an Argument
- Start a Debate
- Invite participants
- My Profile

The navigation bar contains three buttons,
each containing some text and an icon representing its destination.

1. A button that leads to the Overview of Debates screen (»participate«).
2. A button that leads to the Start a Debate screen (»start a debate«).
3. A button that leads to the My Profile screen (»my profile«).

![Navigation bar mock-up](./navigation-bar.svg)

> ### Note
>
> The navigation bar is not drawn in the mock-ups of any of these screens.
> It should be there regardless, this is only done to make changing the mock-ups easier.
