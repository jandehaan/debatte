# Invitations and Login

While using the website, a user is either logged in or not logged in.
Debates can only be viewed by users who are logged in and are participants of the debate.
Invitation emails contain an invitation link, no direct link to a debate.

## Process of being invited

Alice creates a debate and creates invitations. There are three ways to do that:
- Known accounts: Alice invites Bob, a known user.
Bob is shown a notification on the website that allows him to become a participant of the debate.
He is also sent an email.
- Email: Alice wants to send an invitation to Bob’s email address `bob@example.com`. There are two possible cases:
    - There is already an account for `bob@example.com`. In this case, Bob is treated as if his account had been selected directly.
    - There is no account for `bob@example.com`. In this case, an email is sent to the address containing an invitation link that leads to the quick introduction screen.
- Shared link: Alice creates a link and shares it with Bob via a medium of her choice.
Bob visits the link and is sent to the create an account screen. The confirmation email’s contents are changed so that Bob is lead directly to the debate.
