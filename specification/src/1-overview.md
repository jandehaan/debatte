# Overview

This application makes taking informed decisions easier.
It enforces a minimum of structure in debates and facilitates actions by the participants to make the debate more constructive.

The central element that users deal with are debates.
A debate consists of a question and multiple exclusive possible answers.
The objective of a debate is to choose one answer.
This is done by weighing arguments.
Every participant weighs arguments and thereby decides how to vote.
The voting method that is used is the ranked-pairs method (see [this paper](./Tideman-1987-Independence-of-Clones.pdf)).
Participants state arguments.
Each argument supports one ore more of the answers.
For every argument, there may be sub-debates,
which concern only this one argument.

There are two kinds of sub-debates:

- Debates about the relevance of an argument with regards to an answer.
- Debates about the argument’s merit.

If these debates lead to the conclusion that an argument is not relevant or that it does not have merit,
it will not be considered when voting.
A two-thirds majority is required in these debates.

Each participant votes by weighing the arguments.
Each argument is assigned a weight,
which is added to the answer it supports.
The answers are then ordered by the total weight of their supporting arguments.
A vote does not only consist of a single choice,
but of an ordering of all possible choices.
The ordering arising from the weights of the arguments may be overridden by the participant.

## Structure of the application

The application is a website that is (for now) designed for smartphones.
It consists of the following screens
(the arrows represent which other screens can be reached from a screen)

![screen flow](./1-screens-flow.svg)

Additionally, there is a navigation bar that can be reached from the screens listed in the section [Description of Screens](./description-of-screens.md)
(»most screens« in the diagram).

![screen flow (navigation bar)](./1-screens-flow-nav-bar.svg)
