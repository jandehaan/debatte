# Eltrot: Specification

This specification document is built using [`mdbook`](https://github.com/rust-lang-nursery/mdBook),
which can be installed by running

```sh
cargo install mdbook
```

> Make sure to update Rust before trying to install `mdbook`
> (`rustup update`)

- Run `mdbook serve` to automatically rebuild after changing something. The book will be available at `localhost:3000`.
- Use `mdbook build` to build the book. You can deploy the `book` directory using any web server.
