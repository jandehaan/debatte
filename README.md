# Eltrot

<img alt="Eltrot logo: a flower with five petals" src="./assets/logo-name.dist.svg" width="200">

Eltrot is a web app for hosting discussions and voting.

Unique features:
- Very structured debates (this argument supports that answer, this debate concerns that argument, etc.)
- Encourages making a decision by forcing you to choose a schedule for proposing answers, discussion, and voting.
- Frictionless participation: New users do not need to choose a password just to participate in a discussion.
- A preferential voting system (ranked pairs, Tideman) discourages tactical voting.
- Makes it easy to vote rationally by allowing people to assign weights to arguments.
- Privacy-friendly: All data is only kept around for 30 days after a debate is over, you can get a PDF-summary to save.
- Aware of what it cannot do: Votes are visible for all participants – if there is so much at stake that a real election is necessary, use paper ballots.

The web app in this repository consists of a server, a client, and a landing page, as well as a functional specification and Ansible scripts to automatically deploy everything.
These components can be found in the different top level directories, each with instructions for building, using, etc.
The most common operations have been automated, you can use the actions provided in `.gitlab-ci.yml`.
